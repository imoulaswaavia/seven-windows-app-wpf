﻿using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace partigiano_windows_app_wpf
{
    /// <summary>
    /// Interaction logic for IncomingCallWindow.xaml
    /// </summary>
    public partial class IncomingCallWindow : Window
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user answers a video call")]
        public event EventHandler<object> AnswerVideo;


        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user answers an audo only call")]
        public event EventHandler<object> AnswerMicrophone;


        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user declines the call")]
        public event EventHandler<object> DeclineCall;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user didn't answer the call")]
        public event EventHandler<object> NoAnswerCall;

        private readonly Msg _msg;

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        private int mintCountRinging; 

        public IncomingCallWindow(Msg Message)
        {
            InitializeComponent();

            _msg = Message;

            Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {

                txtCaller.Text = _msg.FromNiceAccountName;

                CallHeaderModel chm = JsonSerializer.Deserialize<CallHeaderModel>(_msg.Body);

                txtCallDetails.Text = String.Format("{0}x{1} {2}fps", chm.Width, chm.Height, chm.FPS);

                var workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
                var transform = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice;
                var corner = transform.Transform(new Point(workingArea.Right, workingArea.Bottom));

                this.Left = corner.X - this.ActualWidth - 150;
                this.Top = corner.Y - this.ActualHeight - 20;


                if (Properties.Settings.Default.enableaudio)
                {
                    mintCountRinging = 0;
                    dispatcherTimer.Tick += Ringing_Tick;
                    dispatcherTimer.Interval = new TimeSpan(0, 0, 3);
                    dispatcherTimer.Start();
                    Ringing_Tick(dispatcherTimer, new EventArgs());
                }
            }));

        }

        private void Ringing_Tick(object sender, EventArgs e)
        {
            string audiofile = System.IO.Directory.GetCurrentDirectory() + "\\audio\\office-telephone-ringing.wav";
            System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
            player.Play();
            mintCountRinging++;
            if(mintCountRinging > 5)
            {
                dispatcherTimer.Tick -= Ringing_Tick;
                EventHandler<object> handler = NoAnswerCall;
                handler?.Invoke(null, _msg);
                this.Close();
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            dispatcherTimer.Tick -= Ringing_Tick;
            EventHandler<object> handler = DeclineCall;
            handler?.Invoke(null, _msg);
            this.Close();
        }

        private void BtnAnswerMicrophone_Click(object sender, EventArgs e)
        {
            dispatcherTimer.Tick -= Ringing_Tick;
            EventHandler<object> handler = AnswerMicrophone;
            handler?.Invoke(null, _msg);
            this.Close();
        }

        private void BtnAnswerVideo_Click(object sender, EventArgs e)
        {
            dispatcherTimer.Tick -= Ringing_Tick;
            EventHandler<object> handler = AnswerVideo;
            handler?.Invoke(null, _msg);
            this.Close();
        }

    }
}
