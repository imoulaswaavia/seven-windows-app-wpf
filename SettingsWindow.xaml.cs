﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace partigiano_windows_app_wpf
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : MetroWindow
    {
        public SettingsWindow()
        {
            InitializeComponent();
            tglAutoStart.IsOn = Properties.Settings.Default.autostartup;
            tglDarkMode.IsOn = Properties.Settings.Default.darkmode;
            tglNotifications.IsOn = Properties.Settings.Default.enablenotifications;
            tglAudio.IsOn = Properties.Settings.Default.enableaudio;
            tglOnTop.IsOn = Properties.Settings.Default.ontopnewmsg;

            txtVersion.Text = String.Format("Version {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());

        }

        void SettingsWindow_Closing(object sender, CancelEventArgs e)
        {
        }

        private void BtnUpgrade_Click(object sender, RoutedEventArgs e)
        {
            Globals.ApplicationUpgrade();
        }
        

        private void OnTop_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    Properties.Settings.Default.ontopnewmsg = true;
                }
                else
                {
                    Properties.Settings.Default.ontopnewmsg = false;
                }
            }

            Properties.Settings.Default.Save();
        }

        private void Notifications_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    Properties.Settings.Default.enablenotifications = true;
                }
                else
                {
                    Properties.Settings.Default.enablenotifications = false;
                }
            }

            Properties.Settings.Default.Save();
        }

        private void DarkMode_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    Properties.Settings.Default.darkmode = true;
                }
                else
                {
                    Properties.Settings.Default.darkmode = false;
                }
            }

            Properties.Settings.Default.Save();
        }

        private void AutoStart_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    Properties.Settings.Default.autostartup = true;
                    Helpers.CreateStartupLink();
                }
                else
                {
                    Properties.Settings.Default.autostartup = false;
                    Helpers.DeleteStartupLink();
                }
            }

            Properties.Settings.Default.Save();
        }

        private void Audio_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    Properties.Settings.Default.enableaudio = true;
                }
                else
                {
                    Properties.Settings.Default.enableaudio = false;
                }
            }

            Properties.Settings.Default.Save();
        }
    }
}
