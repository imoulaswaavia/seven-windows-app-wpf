﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// Receive the response of that server get the file
    /// Action message: [is]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class IS_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("f")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("g")]
        public int ToAccountGroupID { get; set; }

        [JsonPropertyName("t")]
        public int TotalSegments { get; set; }

        [JsonPropertyName("s")]
        public int Segment { get; set; }

        [JsonPropertyName("h")]
        public string Hash { get; set; }

        [JsonPropertyName("k")]
        public string Filename { get; set; }

        [JsonPropertyName("z")]
        public long FileSize { get; set; }

        [JsonPropertyName("e")]
        public string FileDate { get; set; }


    }

}
