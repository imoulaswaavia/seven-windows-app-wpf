﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The receive a read update model. Message from SERVER to client.
    /// Action message: [uu]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class UU_ServerModel
    {
        [JsonPropertyName("a")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("d")]
        public int ToDeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("q")]
        public int FromAccountID { get; set; }

        [JsonPropertyName("g")]
        public int AccountGroupID { get; set; }

        [JsonPropertyName("w")]
        public int FromDeviceID { get; set; }

        [JsonPropertyName("j")]
        public string ReadDate { get; set; }
    }


}
