﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class SG_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("t")]
        public sg_site_model[] Sites { get; set; }

        [JsonPropertyName("g")]
        public sg_group_model[] Groups { get; set; }

    }

    class sg_site_model
    {
        [JsonPropertyName("s")]
        public int SiteID { get; set; }

        [JsonPropertyName("d")]
        public string Description { get; set; }

        [JsonPropertyName("y")]
        public int CompanyLevel{ get; set; }
    }

    class sg_group_model
    {
        [JsonPropertyName("i")]
        public int AccountGroupID { get; set; }

        [JsonPropertyName("s")]
        public int SiteID { get; set; }

        [JsonPropertyName("d")]
        public string Description { get; set; }

        [JsonPropertyName("p")]
        public int Priority { get; set; }


        [JsonPropertyName("c")]
        public string Color { get; set; }

        [JsonPropertyName("e")]
        public string Email { get; set; }
    }
}
