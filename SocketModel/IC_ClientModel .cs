﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{

    class IC_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("ic")]
        public ic_model Ic { get; set; }

    }

    class ic_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("f")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("g")]
        public int ToAccountGroupID { get; set; }

        [JsonPropertyName("h")]
        public string Hash { get; set; }

        [JsonPropertyName("k")]
        public string Filename { get; set; }

        [JsonPropertyName("z")]
        public long FileSize { get; set; }

        [JsonPropertyName("e")]
        public string FileDate { get; set; }
        [JsonPropertyName("t")]
        public int TotalSegments { get; set; }
    }


}
