﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The video call header included in body messages
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Dec 2020
    /// </remarks>
    class CallHeaderModel
    {
        [JsonPropertyName("o")]
        public string RoomName { get; set; }

        [JsonPropertyName("f")]
        public int FPS { get; set; }

        [JsonPropertyName("h")]
        public int Height { get; set; }

        [JsonPropertyName("w")]
        public int Width { get; set; }

        [JsonPropertyName("u")]
        public int AudioEnabled { get; set; }

        [JsonPropertyName("v")]
        public int VideoEnabled { get; set; }
    }


}
