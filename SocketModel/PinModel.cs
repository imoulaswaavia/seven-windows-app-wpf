﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{

    class PinModel
    {
        [JsonPropertyName("au")]
        public auth_Model Auth { get; set; }

        [JsonPropertyName("pn")]
        public pn_Model Pin { get; set; }
    }

    class auth_Model
    {
        [JsonPropertyName("p")]
        public string Password { get; set; }
    }

    class pn_Model
    {
        [JsonPropertyName("i")]
        public string Pin { get; set; }

        [JsonPropertyName("c")]
        public string Cguid { get; set; }

        [JsonPropertyName("o")]
        public string Os { get; set; }

        [JsonPropertyName("h")]
        public string Hw { get; set; }

        [JsonPropertyName("s")]
        public string Serial { get; set; }

        [JsonPropertyName("b")]
        public string Version { get; set; }
        
    }
}
