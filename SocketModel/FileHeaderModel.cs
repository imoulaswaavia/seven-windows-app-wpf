﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The file header included in body messages
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class FileHeaderModel
    {
        [JsonPropertyName("k")]
        public String Filename { get; set; }

        [JsonPropertyName("z")]
        public long FileSize { get; set; }

        [JsonPropertyName("e")]
        public string FileDate { get; set; }

        [JsonPropertyName("t")]
        public int TotalSegments { get; set; }

        [JsonPropertyName("s")]
        public int LastSegment { get; set; }

        [JsonPropertyName("h")]
        public string Hash { get; set; }

    }


}
