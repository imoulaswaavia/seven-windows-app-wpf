﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{

    class SM_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("sm")]
        public sm_Model Sm { get; set; }
    }

    class sm_Model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }


        [JsonPropertyName("b")]
        public string Body { get; set; }

        [JsonPropertyName("f")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("g")]
        public int ToAccountGroupID { get; set; }

        [JsonPropertyName("s")]
        public string ScheduledDate { get; set; }

        [JsonPropertyName("e")]
        public int Alert { get; set; }

        [JsonPropertyName("y")]
        public int Destroy { get; set; }

        [JsonPropertyName("t")]
        public int MessageType { get; set; }

        [JsonPropertyName("q")]
        public string QuoteMsgUUID { get; set; }
    }


}
