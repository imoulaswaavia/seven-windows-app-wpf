﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class ResponseModel
    {
        [JsonPropertyName("ac")]
        public String Action { get; set; }

        [JsonPropertyName("rsp")]
        public object Response { get; set; }

    }
}
