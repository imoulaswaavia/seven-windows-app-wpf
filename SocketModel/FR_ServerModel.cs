﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class FR_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("frl")]
        public frl_model[] Friends { get; set; }
    }

    class frl_model
    {
        [JsonPropertyName("q")]
        public int AccountID { get; set; }

        [JsonPropertyName("l")]
        public String Lastname { get; set; }

        [JsonPropertyName("f")]
        public String Firstname { get; set; }

        [JsonPropertyName("e")]
        public String Email { get; set; }

        [JsonPropertyName("i")]
        public int SiteRoleID { get; set; }

        [JsonPropertyName("t")]
        public int Active { get; set; }

        [JsonPropertyName("g")]
        public String Language { get; set; }

        [JsonPropertyName("s")]
        public int SiteLevel { get; set; }
    }
}
