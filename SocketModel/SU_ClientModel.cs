﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{

    class SU_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("su")]
        public su_model Su { get; set; }

    }

    class su_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("j")]
        public string ReadDate { get; set; }
        
    }


}
