﻿using System.Text.Json.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The auth model to update firstname/lastname/password
    /// Action message: [du]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    
    class AU_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("p")]
        public string Password { get; set; }

        [JsonPropertyName("f")]
        public string Firstname { get; set; }

        [JsonPropertyName("l")]
        public string Lastname { get; set; }

        [JsonPropertyName("e")]
        public string Email { get; set; }
    }

}
