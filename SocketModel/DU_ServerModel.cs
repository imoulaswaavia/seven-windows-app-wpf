﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The receive read update) delivery acknowledgement to the server. Message from SERVER to client.
    /// Action message: [du]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class DU_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("q")]
        public int FromAccountID { get; set; }
        [JsonPropertyName("w")]
        public int FromDeviceID { get; set; }

    }

}
