﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The friend header included in body messages
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class FriendHeaderModel
    {
        [JsonPropertyName("f")]
        public int FromAccountID { get; set; }

        [JsonPropertyName("t")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("i")]
        public int ActionID { get; set; }


    }


}
