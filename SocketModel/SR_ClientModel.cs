﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{

    class SR_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("sr")]
        public sr_Model Sr { get; set; }
    }

    class sr_Model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

    }


}
