﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{

    class FS_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("fs")]
        public fs_model Fs { get; set; }
    }

    class fs_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }


        [JsonPropertyName("x")]
        public string SearchName { get; set; }

        [JsonPropertyName("q")]
        public int SearchAccountID { get; set; }

    }

}
