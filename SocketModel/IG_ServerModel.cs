﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// Receive a part of a file from server
    /// Action message: [ig]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class IG_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("h")]
        public string Hash { get; set; }

        [JsonPropertyName("t")]
        public int TotalSegments { get; set; }

        [JsonPropertyName("s")]
        public int Segment { get; set; }


        [JsonPropertyName("p")]
        public string PartOfFile { get; set; }

    }

}
