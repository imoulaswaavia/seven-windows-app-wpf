﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{

    class FR_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("fr")]
        public fr_model Fr { get; set; }
    }

    class fr_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

    }

}
