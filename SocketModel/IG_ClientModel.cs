﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{

    class IG_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("ig")]
        public ig_model Ig { get; set; }

    }

    class ig_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("h")]
        public string Hash { get; set; }

        [JsonPropertyName("t")]
        public int TotalSegments { get; set; }

        [JsonPropertyName("s")]
        public int Segment { get; set; }
    }


}
