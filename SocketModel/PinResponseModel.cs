﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class PinResponseModel
    {
     
        [JsonPropertyName("u")]
        public string Username { get; set; }

        [JsonPropertyName("p")]
        public string Password { get; set; }

        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("s")]
        public int FileSegment { get; set; }

        [JsonPropertyName("y")]
        public string CompanyDescription { get; set; }

        [JsonPropertyName("i")]
        public int SiteRoleID { get; set; }

        [JsonPropertyName("f")]
        public string FirstName { get; set; }

        [JsonPropertyName("l")]
        public string Lastname { get; set; }

        [JsonPropertyName("e")]
        public string Email { get; set; }

        [JsonPropertyName("v")]
        public int SiteLevel { get; set; }

        [JsonPropertyName("n")]
        public int NameLocked { get; set; }

        [JsonPropertyName("g")]
        public string Language { get; set; }
    }
}
