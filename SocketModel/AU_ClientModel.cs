﻿using System.Text.Json.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{

    class AU_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("au")]
        public au_model Au { get; set; }

    }

    class au_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("p")]
        public string Password { get; set; }

        [JsonPropertyName("f")]
        public string Firstname { get; set; }

        [JsonPropertyName("l")]
        public string Lastname { get; set; }

        [JsonPropertyName("e")]
        public string Email { get; set; }
    }


}
