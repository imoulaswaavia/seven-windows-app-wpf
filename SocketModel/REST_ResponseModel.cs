﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class REST_ResponseModel
    {
        [JsonPropertyName("StatusCode")]
        public int statusCode { get; set; }

        [JsonPropertyName("Body")]
        public string body { get; set; }

    }
}
