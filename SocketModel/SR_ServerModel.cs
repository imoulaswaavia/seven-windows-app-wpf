﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    // [SR] SITEROLES response from SERVER TO CLIENT 

    class SR_ServerModel
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("s")]
        public sr_model[] SiteRoles { get; set; }

    }

    class sr_model
    {
        [JsonPropertyName("i")]
        public int SiteRoleID { get; set; }

        [JsonPropertyName("d")]
        public string Description { get; set; }
    }

}
