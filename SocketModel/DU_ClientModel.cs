﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace partigiano_windows_app_wpf.SocketModel
{

    class DU_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("du")]
        public du_model Du { get; set; }

    }

    class du_model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }

        [JsonPropertyName("q")]
        public int FromAccountID { get; set; }

        [JsonPropertyName("w")]
        public int FromDeviceID { get; set; }
    }


}
