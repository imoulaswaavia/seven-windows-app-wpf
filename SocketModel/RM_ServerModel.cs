﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    /// <summary>
    /// The receive mesasge model. Message from SERVER to client.
    /// Action message: [rm]
    /// </summary>
    /// <remarks>
    /// Isidoros Moulas Nov 2020
    /// </remarks>
    class RM_ServerModel
    {
        [JsonPropertyName("a")]
        public int ToAccountID { get; set; }

        [JsonPropertyName("d")]
        public int ToDeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("m")]
        public string MsgUUID { get; set; }


        [JsonPropertyName("b")]
        public string Body { get; set; }

        [JsonPropertyName("f")]
        public int FromAccountID { get; set; }

        [JsonPropertyName("g")]
        public int AccountGroupID { get; set; }

        [JsonPropertyName("x")]
        public int FromDeviceID { get; set; }

        [JsonPropertyName("s")]
        public string ScheduledDate { get; set; }

        [JsonPropertyName("j")]
        public string MsgDate { get; set; }

        [JsonPropertyName("e")]
        public int Alert { get; set; }

        [JsonPropertyName("y")]
        public int Destroy { get; set; }

        [JsonPropertyName("t")]
        public int MessageType { get; set; }

        [JsonPropertyName("q")]
        public string QuoteMsgUUID { get; set; }
    }


}
