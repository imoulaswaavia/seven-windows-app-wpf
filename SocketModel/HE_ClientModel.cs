﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.SocketModel
{
    class HE_ClientModel
    {
        [JsonPropertyName("ac")]
        public string Action { get; set; }

        [JsonPropertyName("he")]
        public he_Model Hello { get; set; }
    }

    class he_Model
    {
        [JsonPropertyName("a")]
        public int AccountID { get; set; }

        [JsonPropertyName("d")]
        public int DeviceID { get; set; }

        [JsonPropertyName("r")]
        public int CompanyID { get; set; }

        [JsonPropertyName("c")]
        public string Cguid { get; set; }

        [JsonPropertyName("o")]
        public string Os { get; set; }

        [JsonPropertyName("h")]
        public string Hw { get; set; }

        [JsonPropertyName("s")]
        public string Serial { get; set; }

        [JsonPropertyName("b")]
        public string Version { get; set; }
    }
}
