﻿using partigiano_windows_app_wpf.Classes;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Websocket.Client;
using System.Net.WebSockets;

using System.Windows.Forms.VisualStyles;
using System.Text.Json;
using partigiano_windows_app_wpf.SocketModel;
using partigiano_windows_app_wpf.ViewModel;
using partigiano_windows_app_wpf.Model;
using System.Net.NetworkInformation;
using static partigiano_windows_app_wpf.Model.Msg;
using System.IO;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Drawing;
using System.Drawing.Imaging;
using Brushes = System.Windows.Media.Brushes;
using System.Diagnostics;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Windows.Threading;

namespace partigiano_windows_app_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        int mintDestroy = 0;
        string mstrQuoteMsgUUID = "";
        bool mblnSearchInProgress = false;

        public MainWindow()
        {
            InitializeComponent();

            //default
            popMessageOptions.IsOpen = false;
            Globals.CheckFolders();
            Globals.CheckGuid();

            Database.Check();

            if (!Database.IsConfigured())
            {
                this.Hide();
                ServersWindow winServers = new ServersWindow();
                winServers.ShowPinRegisterPanel = true;
                winServers.ShowDialog();
                if (!Database.IsConfigured())
                {
                    System.Windows.Application.Current.Shutdown();
                    return;
                }
                else
                {
                    this.Show();
                }
            }


            Globals.ServerAccountIDs = DatabaseCommon.GetServerAccountIDs();
            Server _server;
            //get the default server id
            if (Globals.ServerAccountIDs.Count > 0)
            {
                _server = DatabaseCommon.GetServer(AccountID: Globals.ServerAccountIDs[0]);
                Globals.CurrentServerID = _server.ServerID;
            } else
            {
                _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            }


            txtCurrentUserInitials.Text = _server.NiceInitials;

            this.Show();

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                Console.WriteLine("Start socket client");

                //var c = new AWSSocket();
                Globals.AwsSocketClient.ImageReceived += ImageReceived;
                Globals.AwsSocketClient.MessageReceived += MessageReceived;
                Globals.AwsSocketClient.MessageReceivedDuplicate += MessageReceivedDuplicate;
                Globals.AwsSocketClient.MessageDelivered += MessageDelivered;
                Globals.AwsSocketClient.ReadUpdateReceived += ReadUpdateReceived;
                Globals.AwsSocketClient.Connected += SocketConnected;
                Globals.AwsSocketClient.Disconnected += SocketDisconnected;
                Globals.AwsSocketClient.FriendsReceived += FriendsReceived;
                Globals.AwsSocketClient.FriendsSearchReceived += FriendsSearchReceived;
                Globals.AwsSocketClient.FriendshipReceived += FriendshipReceived;
                Globals.AwsSocketClient.GroupsReceived += GroupsReceived;

                Globals.AwsSocketClient.Start();

            }).Start();

            Console.WriteLine("Ready");

            SitesViewControl.SiteSelected += new EventHandler(SitesViewControl_SiteSelected);
            GroupsViewControl.GroupSelected += new EventHandler(GroupsViewControl_GroupSelected);
            AccountsViewControl.AccountSelected += new EventHandler(AccountsViewControl_AccountSelected);

            if (Globals.CurrentAccountID == 0 && Globals.CurrentGroupID == 0)
            {
                gridMessagePanel.Visibility = Visibility.Collapsed;
            }

            FillHourAndTimeControls();
            SetMessageTextDefaults();

            //start read updates timer
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += ProcessTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 10);
            dispatcherTimer.Start();

            SelectImageQualityCntrol.SendFileHeaderMessage += SendFileHeaderMessage;
            AccountsViewControl.SearchFriend += SendSearchFriend;
            AccountsViewControl.AccountsRefreshUI += AccountsRefreshUI;
            AccountsViewControl.SendFriendAction += SendFriendAction;
            AccountsViewControl.RemoveFriendAction += RemoveFriendAction;

            RecentViewControl.RecentChatSelected += RecentViewControl_RecentChatSelected;

            MsgsViewControl.ReplyToMsg += ReplyToMsg;
            MsgsViewControl.LoadMoreMessages += LoadMoreMessages;

            CallControlControl.SendCallRequest += SendCallRequest;
            SearchMessageControl.SearchMessage += SearchMessage;
            SearchMessageControl.ResetSearch += ResetSearch;

            ShowUnreadBadges(_server.AccountID);

            ShowRecent(); //default view
            FixStartUI();

            if (Properties.Settings.Default.autostartup)
            {
                Helpers.CreateStartupLink();
            } else
            {
                Helpers.DeleteStartupLink();
            }
            
            DataObject.AddPastingHandler(txtMessage, txtMessage_OnPaste);
        }

        private void ApplicationBadge()
        {
            string BadgeNumber="";
            int t = 0;

            try
            {
                if (badgeSites.Badge != null)
                {
                    if (badgeSites.IsBadgeSet)
                    {
                        t += (int)badgeSites.Badge;
                    }
                        
                }
                if (badgeContacts.Badge != null)
                {
                    if(badgeContacts.IsBadgeSet)
                    {
                        t += (int)badgeContacts.Badge;
                    }
                    
                }
            } catch
            {

            }
            if(t>0)
            {
                BadgeNumber = Convert.ToString(t);
            }

            int x;
            int FontSize = 20;
            int intWidth = 40;

            if (BadgeNumber.Length==1)
            {
                x = intWidth / 2 - 10;
            } else if (BadgeNumber.Length==2) {
                x = 0;
            } else if (BadgeNumber.Length > 2)
            {
                BadgeNumber = "99+";
                x = 0;
            } else
            {
                this.taskBarItemInfo1.Overlay = null;
                return;
            }

            RectangleF rectF = new RectangleF(0, 0, intWidth, intWidth);
            Bitmap bitmap = new Bitmap(intWidth, intWidth, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bitmap.MakeTransparent();


            Graphics g = Graphics.FromImage(bitmap);

            g.FillEllipse(System.Drawing.Brushes.Red, rectF);
            g.DrawString(BadgeNumber, new Font("Segoe UI", FontSize,System.Drawing.FontStyle.Bold), System.Drawing.Brushes.White, new PointF(x, 0));

            IntPtr hBitmap = bitmap.GetHbitmap();

            ImageSource wpfBitmap =
                Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap, IntPtr.Zero, Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());

            this.taskBarItemInfo1.Overlay = wpfBitmap;
        }


        private void FixStartUI()
        {
            CallControlControl.Visibility = Visibility.Hidden;
            txtChatroomTitle.Text = "";

            txtPendingMessagesToReceive.Text = "";
            proProgress.Visibility = Visibility.Collapsed;
            imgDownloadMail.Visibility = Visibility.Collapsed;
            MsgsViewControl.BtnLoadMessages.Visibility = Visibility.Collapsed;
            SearchMessageControl.Visibility=Visibility.Collapsed;
        }

        private void ShowUnreadBadges(int AccountID)
        {
            int unreadMsgsFromAccounts = DatabaseMessages.GetUnreadMsgsFromAllAccounts(AccountID);
            if (unreadMsgsFromAccounts > 0)
            {
                badgeContacts.Badge = unreadMsgsFromAccounts;
            }

            int unreadMsgsFromSites = DatabaseMessages.GetUnreadMsgsFromAllSites(AccountID);
            if (unreadMsgsFromSites > 0)
            {
                badgeSites.Badge = unreadMsgsFromSites;
            }

            this.Dispatcher.Invoke(() =>
            {
                ApplicationBadge();
            });


        }


        private void RefreshDownloadMessagesProgress()
        {
            Globals.PendingMessagesToReceive--;
            if (Globals.PendingMessagesToReceive < 0) Globals.PendingMessagesToReceive = 0;

            this.Dispatcher.Invoke(() =>
            {
                if (Globals.PendingMessagesToReceive > 5)
                {
                    txtPendingMessagesToReceive.Text = Globals.PendingMessagesToReceive.ToString();
                    proProgress.Visibility = Visibility.Visible;
                    imgDownloadMail.Visibility = Visibility.Visible;
                }
                else
                {
                    txtPendingMessagesToReceive.Text = "";
                    proProgress.Visibility = Visibility.Collapsed;
                    imgDownloadMail.Visibility = Visibility.Collapsed;
                }
            });
        }
        private void MessageReceivedDuplicate(object sender, EventArgs e)
        {
            RefreshDownloadMessagesProgress();
        }


        private void MessageReceived(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ontopnewmsg)
            {
                this.Dispatcher.Invoke(() =>
                {
                    this.Activate();
                    this.Topmost = true;  // important
                    this.Topmost = false; // important
                    this.Focus();         // important
                });
            }

            RefreshDownloadMessagesProgress();

            Console.WriteLine("message received.");
            RU_ClientModel ru = (RU_ClientModel)sender;

            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: ru.ClientMsgID);
            if (_msg == null) return;

            this.Dispatcher.Invoke(() =>
            {
                if (Globals.PendingMessagesToReceive <= 5)
                {
                    if (_msg.Header.FromAccountID != Globals.CurrentAccountID && Globals.CurrentGroupID == 0 || _msg.Header.AccountGroupID != Globals.CurrentGroupID)
                    {
                        if (_msg.MsgType == 0 || _msg.MsgType == 1 || _msg.MsgType == 2)
                        {
                            NotificationWindow winNotification = new NotificationWindow(_msg);
                            winNotification.Show();
                            winNotification.NotificationClick += NotificationClick;
                        }
                    }
                    if (_msg.MsgType == 11)
                    {
                        if (_msg.MsgDate.AddMinutes(1) >= DateTime.UtcNow)
                        {
                            IncomingCallWindow winIncomingCall = new IncomingCallWindow(_msg);
                            winIncomingCall.Show();
                            winIncomingCall.AnswerMicrophone += AnswerMicrophone;
                            winIncomingCall.AnswerVideo += AnswerVideo;
                            winIncomingCall.DeclineCall += DeclineCall;
                            winIncomingCall.NoAnswerCall += NoAnswerCall;
                        }
                    }


                }
            }
            );

            //UPDATE BADGES --------------------------------
            if (_msg.Header.AccountGroupID == 0)
            {
                UpdateAccountBadges(_msg.Header.FromAccountID, 1);
            } else
            {
                Group _group = DatabaseCommon.GetGroup(_msg.Header.AccountGroupID);
                if(_group!=null)
                {
                    UpdateSiteBadges(_group.SiteID, 1);
                }
                
                UpdateGroupBadges(_msg.Header.AccountGroupID, 1);
                this.Dispatcher.Invoke(() =>
                {
                    ApplicationBadge();
                });

            }


            bool Found = false;
                               
            if (Globals.CurrentAccountID > 0)
            {
                if (_msg.Header.FromAccountID == Globals.CurrentAccountID && _msg.Header.AccountGroupID == 0)
                {
                    Found = true;
                }
            }

            if (Globals.CurrentGroupID > 0)
            {
                if (_msg.Header.AccountGroupID == Globals.CurrentGroupID)
                {
                    Found = true;
                }
            }

            if (Found)
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (((MsgViewModel)MsgsViewControl.DataContext).Msgs != null)
                    {
                        //_msg.QStatus = 1; //hack to update UI
                        if (_msg.QuoteMsgUUID != "")
                        {
                            Msg _qmsg = DatabaseMessages.GetMsg(MsgUUID: _msg.QuoteMsgUUID);
                            if (_qmsg != null)
                            {
                                _msg.QuoteMsg = _qmsg;
                            } else
                            {
                                _msg.QuoteMsgUUID = "";
                            }

                        }

                        ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msg);
                        MsgsViewControl.MessageScrollViewer.ScrollToEnd();
                    }

                    //send at once the read update
                    SU_ClientModel su = Api.ReadUpdateMessage(_msg);
                    if (su != null)
                    {
                        Globals.AwsSocketClient.SendMessage(su);
                    }


                });
            } 

            if (Properties.Settings.Default.enableaudio && _msg.Alert == 0 && Globals.PendingMessagesToReceive <= 5)
            {
                string audiofile = Directory.GetCurrentDirectory() + "\\audio\\newmsg.wav";
                System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
                player.Play();
            }

            if (_msg.Alert== 1  && Globals.PendingMessagesToReceive <= 5)
            {
                this.Dispatcher.Invoke(() =>
                {
                    txtAlertFrom.Text = _msg.FromNiceAccountName;
                    txtAlertDate.Text = _msg.MsgDate.ToString("yyyy-M-d HH:mm");
                    txtAlertBody.Text = _msg.Body;
                    stackAlert.Visibility = Visibility.Visible;
 
                    if( this.WindowState==WindowState.Minimized )
                    {
                        this.WindowState = WindowState.Normal;
                    }
                    this.Activate();
                    this.Topmost = true;  // important
                    this.Topmost = false; // important
                    this.Focus();         // important

                });

                if (Properties.Settings.Default.enableaudio)
                {
                    string audiofile = Directory.GetCurrentDirectory() + "\\audio\\sms-alert-5-daniel_simon.wav";
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
                    player.Play();
                }


            }


            LoadRecent();

            this.Dispatcher.Invoke(() =>
            {
                if (Globals.CurrentAccountID > 0 || Globals.CurrentGroupID > 0)
                {
                    txtMessage.Focus();
                }

            });

        }

        private void AnswerMicrophone(object sender, object m)
        {
            Msg _msg = (Msg)m;
            CallHeaderModel chm = JsonSerializer.Deserialize<CallHeaderModel>(_msg.Body);
            chm.AudioEnabled = 1;
            chm.VideoEnabled = 0;
            StartVideoCall(chm);
        }

        private void AnswerVideo(object sender, object m)
        {
            Msg _msg = (Msg)m;
            CallHeaderModel chm = JsonSerializer.Deserialize<CallHeaderModel>(_msg.Body);
            chm.AudioEnabled = 1;
            chm.VideoEnabled = 1;
            StartVideoCall(chm);
        }


        private void IncomingCallResponse(Msg _msg,string message)
        {
            bool ShowMessageInChatRoom = false;
            int GroupID = 0;
            int AccountID = 0;
            if (_msg.Header.AccountGroupID == 0)
            {
                AccountID = _msg.Header.ToAccountID;
            }
            else
            {
                GroupID = _msg.Header.AccountGroupID;
            }

            //show "not available msg" if user is at callers window
            if (Globals.CurrentGroupID > 0 && Globals.CurrentGroupID == _msg.Header.AccountGroupID)
            {
                ShowMessageInChatRoom = true;
            }
            else if (Globals.CurrentGroupID == 0 && Globals.CurrentAccountID == _msg.Header.ToAccountID)
            {
                ShowMessageInChatRoom = true;
            }

            //String.Format("@{0}, Not available for call.",_msg.FromNiceAccountName)
            SendMessage(message, 0, 1, _msg.ServerID, _msg.CompanyID, AccountID, GroupID, ShowMessageInChatRoom);

        }
        private void NoAnswerCall(object sender, object m)
        {
            Msg _msg = (Msg)m;
            IncomingCallResponse(_msg, "No answer.");
        }

        private void DeclineCall(object sender, object m)
        {
            Msg _msg = (Msg)m;
            IncomingCallResponse(_msg, "Not available.");
        }


        private void NotificationClick(object sender, object m)
        {
            Msg _msg = (Msg)m;

            Server _server = DatabaseCommon.GetServer(CompanyID: _msg.CompanyID);

            if (_msg.Header.AccountGroupID > 0)
            {
                Group _group = DatabaseCommon.GetGroup(_msg.Header.AccountGroupID);
                Globals.CurrentGroupID = _msg.Header.AccountGroupID;
                Globals.CurrentSiteID = _group.SiteID;
                Globals.CurrentAccountID = 0;
                LoadGroups();

            }
            else
            {
                Globals.CurrentGroupID = 0;
                Globals.CurrentSiteID = 0;
                if (_server.AccountID == _msg.Header.ToAccountID)
                {
                    Globals.CurrentAccountID = _msg.Header.FromAccountID;
                }
                else
                {
                    Globals.CurrentAccountID = _msg.Header.ToAccountID;
                }
            }

            HighlightSelectedAccount();
            HighlightSelectedSite();
            HighlightSelectedGroup();

            //handle the event 
            LoadMessages();

            gridMessagePanel.Visibility = Visibility.Visible;
        }

        private void UpdateAccountBadges(int FromAccountID, int value)
        {
            //UPDATE BADGES --------------------------------
            this.Dispatcher.Invoke(() =>
            {
                if (((AccountViewModel)AccountsViewControl.DataContext).Accounts != null)
                {
                    foreach (Account _account in ((AccountViewModel)AccountsViewControl.DataContext).Accounts)
                    {
                        if (_account.AccountID == FromAccountID)
                        {
                            _account.UnreadMessages+=value;
                            if (_account.UnreadMessages < 0) _account.UnreadMessages = 0; //safety first
                            break;
                        }
                    }
                }
             
                if (badgeContacts.IsBadgeSet)
                {
                    if ((int)badgeContacts.Badge + value <= 0) //safety first
                    {
                        badgeContacts.Badge = null;
                    }
                    else
                    {
                        badgeContacts.Badge = (int)badgeContacts.Badge + value;
                    }

                }
                else
                {
                    if( value > 0 )
                    {
                        badgeContacts.Badge = value;
                    }
                }


            });

            this.Dispatcher.Invoke(() =>
            {
                ApplicationBadge();
            });

        }

        private void UpdateSiteBadges(int SiteID, int value)
        {
            //UPDATE BADGES --------------------------------
            this.Dispatcher.Invoke(() =>
            {
                if (((SiteViewModel)SitesViewControl.DataContext).Sites != null)
                {
                    foreach (Site _site in ((SiteViewModel)SitesViewControl.DataContext).Sites)
                    {
                        if (_site.SiteID == SiteID)
                        {
                            _site.UnreadMessages += value;
                            if (_site.UnreadMessages < 0) _site.UnreadMessages = 0; //safety first
                            break;
                        }
                    }
                }

                if (badgeSites.IsBadgeSet)
                {
                    if ((int)badgeSites.Badge + value <= 0)
                    {
                        badgeSites.Badge = null;
                    }
                    else
                    {
                        badgeSites.Badge = (int)badgeSites.Badge + value;
                    }

                }
                else
                {
                    if ( value > 0 )
                    {
                        badgeSites.Badge = value;
                    }
                    
                }


            });

            this.Dispatcher.Invoke(() =>
            {
                ApplicationBadge();
            });

        }

        private void UpdateGroupBadges(int AccountGroupID, int value)
        {
            //UPDATE BADGES --------------------------------
            this.Dispatcher.Invoke(() =>
            {
                if (((GroupViewModel)GroupsViewControl.DataContext).Groups != null)
                {
                    foreach (Group _group in ((GroupViewModel)GroupsViewControl.DataContext).Groups)
                    {
                        if (_group.AccountGroupID == AccountGroupID)
                        {
                            _group.UnreadMessages += value;
                            if (_group.UnreadMessages < 0) _group.UnreadMessages = 0; //safety first
                            break;
                        }
                    }
                }

            });

            this.Dispatcher.Invoke(() =>
            {
                ApplicationBadge();
            });
        }

        private void ImageReceived(object sender, EventArgs e)
        {
            Console.WriteLine("image downloaded/received.");
            IG_ClientModel igm = (IG_ClientModel)sender;

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: igm.Ig.MsgUUID);
            if (_msg != null)
            {
                this.Dispatcher.Invoke(() =>
                {
                    if (((MsgViewModel)MsgsViewControl.DataContext).Msgs == null)
                    {
                        return;
                    }

                    //ugly but it works.
                    //if you just update the queue doesnt update the ui
                    //_msg.RefreshModel(); //not refreshing UI

                    foreach (Msg _msgui in ((MsgViewModel)MsgsViewControl.DataContext).Msgs)
                    {
                        if (_msgui.MsgUUID == igm.Ig.MsgUUID)
                        {
                            //ugly but it works.
                            //if you just update the queue doesnt update the ui

                            ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Remove(_msgui);
                            ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msgui);
                            break;

                        }
                    }

                    //((MsgViewModel)MsgsViewControl.DataContext).Msgs.Remove(_msg);
                    //((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msg);

                });
                

                //if (Properties.Settings.Default.enableaudio)
                //{
                //    string audiofile = Directory.GetCurrentDirectory() + "\\audio\\newmsg.wav";
                //    System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
                //    player.Play();
                //}

            }

            


        }

        private void SocketConnected(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                txtStatus.Text = "Connected";
            }
            );

        }
        private void SocketDisconnected(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                txtStatus.Text = "Disconnected";
            }
            );
        }

        private void FriendsReceived(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                LoadAccounts(false,"");
                HighlightSelectedAccount();
            }
            );
        }

        private void FriendshipReceived(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                LoadAccounts(true, "");
                HighlightSelectedAccount();
            }
            );
        }

        private void FriendsSearchReceived(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                String SearchText = AccountsViewControl.SearchText;
                LoadAccounts(true,SearchText);
                //HighlightSelectedAccount();
            }
            );
        }

        private void GroupsReceived(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                LoadSites();
                LoadGroups();
                HighlightSelectedSite();
                HighlightSelectedGroup();
            }
            );
        }

        private void MessageDelivered(object sender, EventArgs e)
        {
            Console.WriteLine("message delivered.");
            SM_ServerModel sm = (SM_ServerModel)sender;

            this.Dispatcher.Invoke(() =>
            {
                try
                {
                    if (((MsgViewModel)MsgsViewControl.DataContext).Msgs == null) {
                        return;
                    }
                    foreach (Msg _msg in ((MsgViewModel)MsgsViewControl.DataContext).Msgs)
                    {
                        if (_msg.MsgUUID == sm.MsgUUID)
                        {
                            
                            ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Remove(_msg);
                            _msg.Header.Status = 3;
                            ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msg);

                            if (Properties.Settings.Default.enableaudio)
                            {
                                //string audiofile = Directory.GetCurrentDirectory() + "\\audio\\sendmsgok.wav";
                                //System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
                                //player.Play();
                            }

                            //foreach (Msgq _q in _msg.MessageQueue)
                            //{
                            //    if (_q.IsHeader == 1)
                            //    {
                            //        //ugly but it works.
                            //        //if you just update the queue doesnt update the ui

                            //        ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Remove(_msg);
                            //        _q.Status = 3;
                            //        ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msg);

                            //        if (Properties.Settings.Default.enableaudio)
                            //        {
                            //            //string audiofile = Directory.GetCurrentDirectory() + "\\audio\\sendmsgok.wav";
                            //            //System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
                            //            //player.Play();
                            //        }
                            //        return;
                            //    }
                            //}
                        }
                    }
                } catch
                {

                }


            });


        }


        private void ReadUpdateReceived(object sender, EventArgs e)
        {
            Console.WriteLine("MessageReadUpdateReceived");
            UU_ServerModel uu = (UU_ServerModel)sender;
            
            this.Dispatcher.Invoke(() =>
            {
          
                if (((MsgViewModel)MsgsViewControl.DataContext).Msgs == null)
                {
                    return;
                }
                foreach (Msg _msg in ((MsgViewModel)MsgsViewControl.DataContext).Msgs)
                {
                    if (_msg.MsgUUID == uu.MsgUUID)
                    {
                        _msg.QStatus = 1;

                        return;
                    }
                }
                
       


            });


        }
        #region Top toolbar______________________________________________________

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
            System.Windows.Application.Current.Shutdown();
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow winSettings;

            if (Helpers.IsWindowOpen<Window>("SettingsWindowName"))
            {
                winSettings = (SettingsWindow)Helpers.GetWindow("SettingsWindowName");
                winSettings.Activate();
            } else
            {
                winSettings = new SettingsWindow();
            }
            winSettings.Show();


        }

      

        private void BtnAccounts_Click(object sender, RoutedEventArgs e)
        {
            ServersWindow winServers;
            if (Helpers.IsWindowOpen<Window>("ServersWindowName"))
            {
                winServers = (ServersWindow)Helpers.GetWindow("ServersWindowName");
                winServers.Activate();
            }
            else
            {
                winServers = new ServersWindow();
            }
            winServers.Show();
            winServers.ForceRefreshUI += ForceRefreshUI;
            winServers.SendMessage += WinServerSendMessage;
        }

        private void BtnCloseAlert_Click(object sender, RoutedEventArgs e)
        {
            stackAlert.Visibility = Visibility.Hidden;
        }


        #endregion

        private void BtnMessageOptions_Click(object sender, RoutedEventArgs e)
        {
            HidePopupWindows();
            popMessageOptions.IsOpen = true; ;
        }

        private void BtnMessageOptionsExit_Click(object sender, RoutedEventArgs e)
        {
            HidePopupWindows();
            popMessageOptions.IsOpen = false;
            txtMessage.Focus();
        }


        private void GroupsViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadGroups();
        }

        private void AccountsViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadAccounts(false,"");
        }
        private void RecentViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadRecent();
        }

        private void ServersSelectViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadServers();
        }

        private void SitesViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadSites();
        }

        private void LoadSites()
        {
            partigiano_windows_app_wpf.ViewModel.SiteViewModel siteViewModelObject =
               new partigiano_windows_app_wpf.ViewModel.SiteViewModel();
            siteViewModelObject.LoadSites();

            SitesViewControl.DataContext = siteViewModelObject;
        }
        private void LoadGroups()
        {
            partigiano_windows_app_wpf.ViewModel.GroupViewModel groupViewModelObject =
             new partigiano_windows_app_wpf.ViewModel.GroupViewModel();
            groupViewModelObject.LoadGroups();

            GroupsViewControl.DataContext = groupViewModelObject;
        }

        private void LoadServers()
        {
            partigiano_windows_app_wpf.ViewModel.ServerViewModel selectViewModelObject =
             new partigiano_windows_app_wpf.ViewModel.ServerViewModel();
            selectViewModelObject.LoadServers();

            ServersSelectViewControl.DataContext = selectViewModelObject;
        }

        private void LoadAccounts(bool WithFriends, string SearchText)
        {
            partigiano_windows_app_wpf.ViewModel.AccountViewModel accountViewModelObject =
             new partigiano_windows_app_wpf.ViewModel.AccountViewModel();
            accountViewModelObject.LoadAccounts(WithFriends, SearchText);

            AccountsViewControl.DataContext = accountViewModelObject;
        }


        protected void SitesViewControl_SiteSelected(object sender, EventArgs e)
        {
            gridMessagePanel.Visibility = Visibility.Collapsed;
            HighlightSelectedSite();
            HighlightSelectedGroup();
            HighlightSelectedAccount();

            //handle the event 
            LoadGroups();

            //auto select if only one group
            if (((GroupViewModel)GroupsViewControl.DataContext).Groups.Count() == 1)
            {
                Globals.CurrentGroupID = ((GroupViewModel)GroupsViewControl.DataContext).Groups[0].AccountGroupID;
                HighlightSelectedGroup();
                gridMessagePanel.Visibility = Visibility.Visible;
            }

            //handle the event 
            mblnSearchInProgress = false;
            LoadMessages();
            gridMessagePanel.Visibility = Visibility.Visible;
            MsgsViewControl.BtnLoadMessages.Visibility = Visibility.Visible;
            SearchMessageControl.Visibility = Visibility.Visible;

        }

        private void HighlightSelectedSite()
        {
            //highlight selected site
            for (int i = 0; i < ((SiteViewModel)SitesViewControl.DataContext).Sites.Count(); i++)
            {
                Site _site = ((SiteViewModel)SitesViewControl.DataContext).Sites[i];
                _site.IsCurrent = _site.SiteID == Globals.CurrentSiteID;
                //_site.UnreadMessages = 2325;

                if (_site.IsCurrent)
                {
                    Globals.CurrentCompanyID = _site.CompanyID;
                    GroupsViewControl.Visibility = Visibility.Visible;
                    txtChatroomTitle.Text = _site.Description;
                    CallControlControl.Visibility = Visibility.Hidden;
                }
            }

            
        }

        private void HighlightSelectedGroup()
        {
            for (int i = 0; i < ((GroupViewModel)GroupsViewControl.DataContext).Groups.Count(); i++)
            {
                Group _group = ((GroupViewModel)GroupsViewControl.DataContext).Groups[i];
                _group.IsCurrent = _group.AccountGroupID == Globals.CurrentGroupID;
                if(_group.IsCurrent)
                {
                    Site _site = DatabaseCommon.GetSite(_group.SiteID);
                    if (_site == null) return;
                    txtChatroomTitle.Text = String.Format("{0} - {1}", _site.Description, _group.Description);
                    CallControlControl.Visibility = Visibility.Visible;
                };
            }

        }

        private void HighlightSelectedAccount()
        {
            for (int i = 0; i < ((AccountViewModel)AccountsViewControl.DataContext).Accounts.Count(); i++)
            {
                Account _account = ((AccountViewModel)AccountsViewControl.DataContext).Accounts[i];
                _account.IsCurrent = _account.AccountID == Globals.CurrentAccountID;
                //_account.UnreadMessages = 4444;

                if (_account.IsCurrent)
                {
                    Globals.CurrentCompanyID = _account.CompanyID;
                    GroupsViewControl.Visibility = Visibility.Collapsed;
                    txtChatroomTitle.Text = _account.NiceName;
                    CallControlControl.Visibility = Visibility.Visible;
                }
            }
        }

        protected void GroupsViewControl_GroupSelected(object sender, EventArgs e)
        {
            HidePopupWindows();
            HighlightSelectedGroup();

            //handle the event 
            mblnSearchInProgress = false;
            LoadMessages();
            gridMessagePanel.Visibility = Visibility.Visible;
            MsgsViewControl.BtnLoadMessages.Visibility = Visibility.Visible;
            SearchMessageControl.Visibility = Visibility.Visible;


        }

        protected void AccountsViewControl_AccountSelected(object sender, EventArgs e)
        {
            HidePopupWindows();
            HighlightSelectedAccount();
            HighlightSelectedSite();
            HighlightSelectedGroup();

            //handle the event 
            mblnSearchInProgress = false;
            LoadMessages();

            gridMessagePanel.Visibility = Visibility.Visible;
            MsgsViewControl.BtnLoadMessages.Visibility = Visibility.Visible;
            SearchMessageControl.Visibility = Visibility.Visible;
        }

        protected void RecentViewControl_RecentChatSelected(object sender, long ClientMsgID)
        {
            HidePopupWindows();
            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: ClientMsgID);
            if (_msg == null) return;

            Server _server = DatabaseCommon.GetServer(CompanyID: _msg.CompanyID);
            if (_server == null) return;
            if(_server.AccountID==_msg.Header.ToAccountID)
            {
                Globals.CurrentAccountID = _msg.Header.FromAccountID;
            } else
            {
                Globals.CurrentAccountID = _msg.Header.ToAccountID;
            }
            if (_msg.Header.AccountGroupID>0)
            {
                Group _group = DatabaseCommon.GetGroup(_msg.Header.AccountGroupID);
                if (_group == null) return; //no group found
                Globals.CurrentSiteID = _group.SiteID;
                Globals.CurrentAccountID = 0;
                LoadGroups();

            } else
            {
                Globals.CurrentSiteID = 0;
            }

            Globals.CurrentGroupID = _msg.Header.AccountGroupID;
            Globals.CurrentCompanyID = _server.CompanyID;
            Globals.CurrentServerID = _server.ServerID;

            HighlightSelectedAccount();
            HighlightSelectedSite();
            HighlightSelectedGroup();

            //handle the event 
            mblnSearchInProgress = false;
            LoadMessages();

            gridMessagePanel.Visibility = Visibility.Visible;
            MsgsViewControl.BtnLoadMessages.Visibility = Visibility.Visible;
            SearchMessageControl.Visibility = Visibility.Visible;


        }

        private void MsgsViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadMessages();
        }

        private void LoadMessages(int MessageLimit=15,bool ScrollToEnd=true,String SearchText="")
        {

            partigiano_windows_app_wpf.ViewModel.MsgViewModel msgViewModelObject =
                    new partigiano_windows_app_wpf.ViewModel.MsgViewModel();
            msgViewModelObject.LoadMessages(MessageLimit: MessageLimit, SearchText: SearchText);

            //mblnSearchInProgress=true means that user started search, but clicked somewhere else while searching
            if (!mblnSearchInProgress && SearchText!="") 
            {
                return;
            }
            this.Dispatcher.Invoke(() =>
            {
                SearchMessageControl.proProgress.Visibility = Visibility.Collapsed;
                if (SearchText == "")
                {
                    SearchMessageControl.txtSearch.Document.Blocks.Clear();
                }

                MsgsViewControl.DataContext = msgViewModelObject;

                if (ScrollToEnd) MsgsViewControl.MessageScrollViewer.ScrollToEnd();

                ProcessChatRoomUI();
            });

        }

        private void LoadRecent()
        {

            this.Dispatcher.Invoke(() =>
            {
                partigiano_windows_app_wpf.ViewModel.RecentViewModel recentViewModelObject =
                    new partigiano_windows_app_wpf.ViewModel.RecentViewModel();
                recentViewModelObject.LoadMessages();

                RecentViewControl.DataContext = recentViewModelObject;

                RecentViewControl.MessageScrollViewer.ScrollToTop();

            });

        }


        private void ProcessTimer_Tick(object sender, EventArgs e)
        {
            ProcessChatRoomUI();
            ProcessUnsentMessages();

        }

        private void ProcessUnsentMessages()
        {
            List<int> _ids;

            _ids = DatabaseMessages.GetUnsentMsgs();

            foreach (int ClientMsgID in _ids)
            {
                SM_ClientModel sm = Api.GetSendMessageModel(ClientMsgID);
                Globals.AwsSocketClient.SendMessage(sm);
            }
            if (_ids.Count > 0)
            {
                PlaySendSound();
            }

        }

        public bool IsActiveWindow(IntPtr handle)
        {
            IntPtr activeHandle = GetForegroundWindow();
            return (activeHandle == handle);
        }
    

        private void ProcessChatRoomUI()
        {
            //if(!IsActiveWindow(Process.GetCurrentProcess().MainWindowHandle))
            //{
            //    return;
            //}

            if (Globals.CurrentAccountID == 0 && Globals.CurrentGroupID == 0)
            {
                return;
            }

            if ((MsgViewModel)MsgsViewControl.DataContext == null)
            {
                return;
            }

            if (((MsgViewModel)MsgsViewControl.DataContext).Msgs == null)
            {
                return;
            }

            //Console.WriteLine("ProcessChatRoomUI");

            foreach (Msg _msg in ((MsgViewModel)MsgsViewControl.DataContext).Msgs)
            {
                if (_msg.IsMyMsg == false && _msg.QStatus == 0)
                {
                    _msg.QStatus = 1; //hack to update UI

                    //ugly every X seconds to try to update the database
                    SU_ClientModel su = Api.ReadUpdateMessage(_msg);
                    if (su != null)
                    {
                        //send the read update at once
                        Globals.AwsSocketClient.SendMessage(su);
                    }
                    if (_msg.Header.AccountGroupID == 0)
                    {
                        UpdateAccountBadges(_msg.Header.FromAccountID, -1);
                    }
                    else
                    {
                        UpdateSiteBadges(Globals.CurrentSiteID, -1);
                        UpdateGroupBadges(Globals.CurrentGroupID, -1);
                    }
                }

                if(_msg.IsMyMsg == false && _msg.Destroy > 0 )
                {
                    Msgq _msgq = DatabaseMessages.GetMsgq(
                                    _msg.ClientMsgID,
                                    _msg.Header.ToAccountID,
                                    _msg.Header.ToDevID,
                                    _msg.Header.FromAccountID,
                                    _msg.Header.FromDevID);
                       
                    if( _msgq.ReadDate.Value.AddMinutes(_msg.Destroy) < DateTime.UtcNow )
                    {
                        Console.WriteLine(_msgq.ReadDate.ToString());
                        Console.WriteLine("destroy me");
                        ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Remove(_msg);
                        DatabaseMessages.DeleteMsg(_msg.ClientMsgID);
                        break;

                    }
                }

                

            }
        }


        private void txtMessage_GotFocus(object sender, RoutedEventArgs e)
        {
            HidePopupWindows();
        }

        private void txtMessage_OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                txtMessage.AppendText("\r");
                txtMessage.CaretPosition = txtMessage.CaretPosition.DocumentEnd;
                e.Handled = true;
            }
            else if (e.Key == Key.Enter)
            {
                BtnSendMessage_Click(sender, e);
            }

        }

        private void txtMessage_OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            //var isText = e.SourceDataObject.GetDataPresent(DataFormats.UnicodeText, true);
            //if (!isText) return;

            //var isBitmap = e.SourceDataObject.GetDataPresent(DataFormats.Bitmap, true);
          
            BitmapSource returnImage = null;
            if (Clipboard.ContainsImage())
            {
                returnImage = Clipboard.GetImage();
                string strTempFileName = String.Format("{0}{1}", System.IO.Path.GetTempFileName(), ".jpg");

                using (var fileStream = new FileStream(strTempFileName, FileMode.Create))
                {
                    BitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(returnImage));
                    encoder.Save(fileStream);
                }
                SelectImageQualityCntrol.ImageFilename = strTempFileName;
                popCompressImage.IsOpen = true;
                e.CancelCommand();
            }

        }

        private void BtnSendMessage_Click(object sender, RoutedEventArgs e)
        {
            if (txtMessage.Text.Trim() == "")
            {
                return;
            }

            SendMessage(txtMessage.Text.Trim(), 0, 1,Globals.CurrentServerID,Globals.CurrentCompanyID,Globals.CurrentAccountID, Globals.CurrentGroupID,true);
            LoadRecent();

        }


      

        private void SendFileHeaderMessage(object sender, object FileHeaderObject)
        {
            FileHeaderModel fh = (FileHeaderModel)FileHeaderObject;

            Server _server = DatabaseCommon.GetServer(CompanyID: Globals.CurrentCompanyID);
            long lngClientMsgID = SendMessage(JsonSerializer.Serialize(fh), 1, -2, Globals.CurrentServerID, Globals.CurrentCompanyID, Globals.CurrentAccountID, Globals.CurrentGroupID, true);

            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: lngClientMsgID);

            IC_ClientModel ic = new IC_ClientModel
            {
                Action = "ic",
                Ic = new ic_model
                {
                    AccountID = _server.AccountID,
                    DeviceID = _server.DeviceID,
                    CompanyID = _server.CompanyID,
                    MsgUUID = _msg.MsgUUID,
                    ToAccountID = Globals.CurrentAccountID,
                    ToAccountGroupID = Globals.CurrentGroupID,
                    Hash = fh.Hash,
                    Filename = fh.Filename,
                    FileSize = fh.FileSize,
                    FileDate = fh.FileDate,
                    TotalSegments = fh.TotalSegments
                }
            };
            Globals.AwsSocketClient.SendMessage(ic);
        }

        private void SendSearchFriend(object sender, object obj)
        {
            //show something before getting new values
            LoadAccounts(true, AccountsViewControl.SearchText);

            Globals.AwsSocketClient.SendMessage(obj);
        }


        private void AccountsRefreshUI(object sender, object obj)
        {
            LoadAccounts(false,"");
        }

        private void SendFriendAction(object sender, object obj)
        {
            FriendHeaderModel frh = (FriendHeaderModel)obj;
            Server _server = DatabaseCommon.GetServer(AccountID: frh.FromAccountID);
            Guid g = Guid.NewGuid();
            long lngClientMsgID = DatabaseMessages.SaveMsg(
                               _server.ServerID,
                               g.ToString(),
                               _server.CompanyID,
                               _server.AccountID,
                               _server.DeviceID,
                               frh.ToAccountID,
                               0,
                               0,
                               JsonSerializer.Serialize(frh),
                               0, 0,
                               null, DateTime.UtcNow, DateTime.UtcNow,
                               3,
                               "",
                               1);

            //get send message model from api
            SM_ClientModel sm = Api.GetSendMessageModel(lngClientMsgID);
            Globals.AwsSocketClient.SendMessage(sm);
            
        }

        private void RemoveFriendAction(object sender, object obj)
        {
            Console.WriteLine("you removed a friend");
            FriendHeaderModel frh = (FriendHeaderModel)obj;
            if(Globals.CurrentAccountID==frh.ToAccountID)
            {
                Globals.CurrentAccountID = 0;
                gridMessagePanel.Visibility = Visibility.Collapsed;
                LoadAccounts(false, "");
                LoadMessages(); //dummy to reset msgview
                txtChatroomTitle.Text = "";

            }
            LoadAccounts(false, "");
        }

        private void ReplyToMsg(object sender, long ClientMsgID)
        {
            Console.WriteLine("reply to msg");
            long lngClientMsgID = ClientMsgID;

            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: lngClientMsgID);
            
            mstrQuoteMsgUUID = _msg.MsgUUID;
            txtQuoteMessage.Text = _msg.Body;
            txtQuoteSender.Text = _msg.FromNiceAccountName;
            txtQuoteDate.Text = _msg.MsgDate.ToString("yyyy-M-d HH:mm");
            brdQuotePanel.Visibility = Visibility.Visible;

            txtMessage.Focus();

        }
        private void LoadMoreMessages(object sender, EventArgs e)
        {
            Console.WriteLine("LoadMoreMessages");

            string strSearchText = "";// SearchMessageControl.txtSearch.Text.Trim();
            
            if (((MsgViewModel)MsgsViewControl.DataContext).Msgs == null) return;
            if (((MsgViewModel)MsgsViewControl.DataContext).Msgs.Count == 0) return;

            //get first ClientMsgID from the list
            Msg _msg = ((MsgViewModel)MsgsViewControl.DataContext).Msgs[0];
            long lngClientMsgID = _msg.ClientMsgID;

            partigiano_windows_app_wpf.ViewModel.MsgViewModel msgViewModelObject =
                 new partigiano_windows_app_wpf.ViewModel.MsgViewModel();
            msgViewModelObject.LoadMessages(30, lngClientMsgID, strSearchText);

            int intPos = 0;
            foreach (Msg _m in msgViewModelObject.Msgs)
            {
                ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Insert(intPos, _m);
                intPos++;
            }

        

        }

        private void ForceRefreshUI(object sender, EventArgs e)
        {
            Globals.CurrentAccountID = 0;
            Globals.CurrentCompanyID = 0;
            Globals.CurrentGroupID = 0;
            Globals.CurrentServerID = 0;
            txtChatroomTitle.Text = "";

            gridMessagePanel.Visibility = Visibility.Hidden;
            CallControlControl.Visibility = Visibility.Hidden;
            ShowRecent();
            LoadMessages();

            if (!Database.IsConfigured())
            {
                System.Windows.Application.Current.Shutdown();
            }
        }
        private void WinServerSendMessage(object sender, Object auobject)
        {
            AU_ClientModel aum = (AU_ClientModel)auobject;
            Globals.AwsSocketClient.SendMessage(aum);
        }
        

        private long SendMessage(String body,int msgtype, int Status, 
            int ServerID, int CompanyID,  int AccountID, int GroupID, bool ShowMessageInChatRoom)
        {
            Server _server = DatabaseCommon.GetServer(ServerID: ServerID);
            int intAccountID = _server.AccountID;
            int intDeviceID = _server.DeviceID;

            int intAlert = tglAlert.IsOn == true ? 1 : 0;

            DateTime? _Scheduleddate = null;
            if (tglSchedule.IsOn && datScheduledDate.SelectedDate != null)
            {
                int hours = Int16.Parse(cmbHours.SelectedValue.ToString());
                int mins = Int16.Parse(cmbMinutes.SelectedValue.ToString());

                _Scheduleddate = datScheduledDate.SelectedDate.Value.AddHours(hours).AddMinutes(mins);

                //.ToString() + " " + cmbHours.SelectedValue.ToString() +":"+cmbMinutes.SelectedValue.ToString();

            }
            int _destroy = 0;
            if(tglSelfDestroy.IsOn)
            {
                _destroy = mintDestroy;
            }

            Guid g = Guid.NewGuid();

            long lngClientMsgID = DatabaseMessages.SaveMsg(
                                _server.ServerID,
                                g.ToString(),
                                CompanyID,
                                intAccountID,
                                intDeviceID,
                                AccountID,
                                0,
                                GroupID,
                                body.Trim(),
                                intAlert, _destroy,
                                _Scheduleddate, DateTime.UtcNow, DateTime.UtcNow,
                                msgtype,
                                mstrQuoteMsgUUID,
                                Status);

            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: lngClientMsgID);
            if (_msg != null)
            {
                if (ShowMessageInChatRoom)
                {
                    if (((MsgViewModel)MsgsViewControl.DataContext).Msgs != null)
                    {
                        if (mstrQuoteMsgUUID != "")
                        {
                            Msg _qmsg = DatabaseMessages.GetMsg(MsgUUID: mstrQuoteMsgUUID);
                            if (_qmsg != null)
                            {
                                _msg.QuoteMsg = _qmsg;
                            }
                           
                        }
                        ((MsgViewModel)MsgsViewControl.DataContext).Msgs.Add(_msg);
                        MsgsViewControl.MessageScrollViewer.ScrollToEnd();
                    }
                }

                if (Status != -2) //hold message do not send immediately ----why is that?
                {
                    //get send message model from api
                    SM_ClientModel sm = Api.GetSendMessageModel(lngClientMsgID);
                    Globals.AwsSocketClient.SendMessage(sm);
                }
            }

            //clear richtextbox
            TextRange txt = new TextRange(txtMessage.Document.ContentStart, txtMessage.Document.ContentEnd)
            {
                Text = ""
            };

            if (Properties.Settings.Default.enableaudio)
            {
                PlaySendSound();
            }

            SetMessageTextDefaults();
            return lngClientMsgID;
        }

     



        private void BtnTakePicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                Title = "Select file to send",
                Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if ((bool)dlg.ShowDialog())
            {
                SelectImageQualityCntrol.ImageFilename = dlg.FileName;
                popCompressImage.IsOpen = true;
            }
        }

        private void PlaySendSound()
        {
            string audiofile = Directory.GetCurrentDirectory() + "\\audio\\sendmsg.wav";
            System.Media.SoundPlayer player = new System.Media.SoundPlayer(@audiofile);
            player.Play();
        }
        private void FillHourAndTimeControls()
        {
            for(int i=0; i<24; i++)
            {
                cmbHours.Items.Add(i.ToString("00"));
            }
            for (int i = 0; i < 59; i+=15)
            {
                cmbMinutes.Items.Add(i.ToString("00"));
            }
        }

        private void SetMessageTextDefaults()
        {
            popMessageOptions.IsOpen = false;

            btnAlertCancel.Visibility = Visibility.Collapsed;
            tglAlert.IsOn = false;

            btnScheduleCancel.Visibility = Visibility.Collapsed;
            tglSchedule.IsOn = false;
            panSelectScheduledDate.Visibility = Visibility.Collapsed;

            btnDestroyCancel.Visibility = Visibility.Collapsed;
            tglSelfDestroy.IsOn = false;

            gridDestroyButtons.Visibility = Visibility.Collapsed;

            //default destroy 3 minutes
            ClearDestroyButtons();
            eliDestroy3.Fill = eliDestroy5.Stroke;
            txtDestroy3.Foreground = new SolidColorBrush(Colors.White);
            mintDestroy = 3;

            cmbHours.SelectedIndex = 0;
            cmbMinutes.SelectedIndex = 0;

            popCompressImage.IsOpen = false;
 
            brdQuotePanel.Visibility = Visibility.Collapsed;
            mstrQuoteMsgUUID = "";
            txtQuoteMessage.Text = "";
        }

        private void ClearDestroyButtons()
        {
            eliDestroy1.Fill = new SolidColorBrush(Colors.White);
            eliDestroy2.Fill = new SolidColorBrush(Colors.White);
            eliDestroy3.Fill = new SolidColorBrush(Colors.White);
            eliDestroy4.Fill = new SolidColorBrush(Colors.White);
            eliDestroy5.Fill = new SolidColorBrush(Colors.White);

            txtDestroy1.Foreground = new SolidColorBrush(Colors.DarkGray);
            txtDestroy2.Foreground = new SolidColorBrush(Colors.DarkGray);
            txtDestroy3.Foreground = new SolidColorBrush(Colors.DarkGray);
            txtDestroy4.Foreground = new SolidColorBrush(Colors.DarkGray);
            txtDestroy5.Foreground = new SolidColorBrush(Colors.DarkGray);
            mintDestroy = 0;
        }
       
        private void Alert_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    btnAlertCancel.Visibility = Visibility.Visible;
                }
                else
                {
                    btnAlertCancel.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void BtnAlertCancel_Click(object sender, RoutedEventArgs e)
        {
            btnAlertCancel.Visibility = Visibility.Collapsed;
            tglAlert.IsOn = false;
        }

        private void Destroy_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    btnDestroyCancel.Visibility = Visibility.Visible;
                    gridDestroyButtons.Visibility = Visibility.Visible;
                }
                else
                {
                    btnDestroyCancel.Visibility = Visibility.Collapsed;
                    gridDestroyButtons.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void BtnDestroyCancel_Click(object sender, RoutedEventArgs e)
        {
            btnDestroyCancel.Visibility = Visibility.Collapsed;
            gridDestroyButtons.Visibility = Visibility.Collapsed;
            tglSelfDestroy.IsOn = false;
        }

        private void Schedule_Toggled(object sender, RoutedEventArgs e)
        {
            ToggleSwitch toggleSwitch = sender as ToggleSwitch;
            if (toggleSwitch != null)
            {
                if (toggleSwitch.IsOn == true)
                {
                    btnScheduleCancel.Visibility = Visibility.Visible;
                    panSelectScheduledDate.Visibility = Visibility.Visible;
                }
                else
                {
                    btnScheduleCancel.Visibility = Visibility.Collapsed;
                    panSelectScheduledDate.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void BtnScheduleCancel_Click(object sender, RoutedEventArgs e)
        {
            btnScheduleCancel.Visibility = Visibility.Collapsed;
            tglSchedule.IsOn = false;
        }


        private void BtnSelectServer_Click(object sender, RoutedEventArgs e)
        {
            popSelectServer.IsOpen = !popSelectServer.IsOpen;
        }

        private void SearchMessage(object sender, string SearchText)
        {
            //proProgress.Visibility = Visibility.Visible;
            SearchMessageControl.proProgress.Visibility = Visibility.Visible;
            Thread thr = new Thread(() => SearchMessagesJob(SearchText));
            thr.Start();
        }
        private void SearchMessagesJob(string SearchText)
        {
            mblnSearchInProgress = true;
            LoadMessages(MessageLimit: 1000, SearchText: SearchText);
            this.Dispatcher.Invoke(() =>
            {
                //proProgress.Visibility = Visibility.Collapsed;
                SearchMessageControl.proProgress.Visibility = Visibility.Collapsed;
                mblnSearchInProgress = false;
            });
        }

        private void ResetSearch(object sender, EventArgs e)
        {
            LoadMessages();
            txtMessage.Focus();
        }

        private void SendCallRequest(object sender, object obj)
        {
            CallHeaderModel ch = (CallHeaderModel)obj;
            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            Guid g = Guid.NewGuid();
            long lngClientMsgID = DatabaseMessages.SaveMsg(
                               _server.ServerID,
                               g.ToString(),
                               _server.CompanyID,
                               _server.AccountID,
                               _server.DeviceID,
                               Globals.CurrentAccountID,
                               0,
                               Globals.CurrentGroupID,
                               JsonSerializer.Serialize(ch),
                               0, 0,
                               null, DateTime.UtcNow, DateTime.UtcNow,
                               11,
                               "",
                               1);

            //get send message model from api
            SM_ClientModel sm = Api.GetSendMessageModel(lngClientMsgID);
            Globals.AwsSocketClient.SendMessage(sm);

            StartVideoCall(ch);
 
        }

       private void StartVideoCall(CallHeaderModel chm)
       {
            string strOvideoDatFile = string.Format(@"{0}\ovideo\ovideo.dat", AppDomain.CurrentDomain.BaseDirectory);
            if (System.IO.File.Exists(strOvideoDatFile))
            {
                System.IO.File.Delete(strOvideoDatFile);
            }

            System.IO.File.AppendAllText(strOvideoDatFile, JsonSerializer.Serialize(chm));

            var startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = string.Format(@"{0}\ovideo", AppDomain.CurrentDomain.BaseDirectory);
            startInfo.FileName = string.Format(@"{0}\ovideo\openvidu-electron.exe", AppDomain.CurrentDomain.BaseDirectory);
            Process proc = Process.Start(startInfo);
        }



        private void BtnShowRecent_Click(object sender, RoutedEventArgs e)
        {
            ShowRecent();
        }

        private void ShowRecent()
        {
            AccountsViewControl.SearchText = "";
            LoadRecent();
            BtnShowContacts.BorderThickness = new Thickness(0);
            BtnShowSites.BorderThickness = new Thickness(0);
            BtnShowRecent.BorderThickness = new Thickness(1);
            AccountsViewControl.Visibility = Visibility.Hidden;
            SitesViewControl.Visibility = Visibility.Hidden;
            RecentViewControl.Visibility = Visibility.Visible;

            HidePopupWindows();
        }

        private void BtnShowContacts_Click(object sender, RoutedEventArgs e)
        {
            AccountsViewControl.SearchText = "";
            LoadAccounts(false,"");
            BtnShowContacts.BorderThickness = new Thickness(1);
            BtnShowSites.BorderThickness = new Thickness(0);
            BtnShowRecent.BorderThickness = new Thickness(0);
            AccountsViewControl.Visibility = Visibility.Visible;
            SitesViewControl.Visibility = Visibility.Hidden;
            RecentViewControl.Visibility = Visibility.Hidden;

            HidePopupWindows();

        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            HidePopupWindows();
        }


        private void Window_LocationChanged(object sender, EventArgs e)
        {
            HidePopupWindows();
        }

        private void BtnShowSites_Click(object sender, RoutedEventArgs e)
        {
            BtnShowContacts.BorderThickness = new Thickness(0);
            BtnShowSites.BorderThickness = new Thickness(1);
            BtnShowRecent.BorderThickness = new Thickness(0);
            AccountsViewControl.Visibility = Visibility.Hidden;
            SitesViewControl.Visibility = Visibility.Visible;
            RecentViewControl.Visibility = Visibility.Hidden;

            HidePopupWindows();
        }


        private void HidePopupWindows()
        {
            //popCompressImage.IsOpen = false;
            //popMessageOptions.IsOpen = false;
            //popSelectServer.IsOpen = false;
            //AccountsViewControl.popFriendAction.IsOpen = false;
            //MsgsViewControl.popMessageActions.IsOpen = false;
            //MsgsViewControl.popMessageInfo.IsOpen = false;
        }

        private void BtnCancelQuote_Click(object sender, RoutedEventArgs e)
        {
            brdQuotePanel.Visibility = Visibility.Collapsed;
            mstrQuoteMsgUUID = "";
            txtQuoteMessage.Text = "";

        }
        private void BtnDestroy_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            ClearDestroyButtons();
            if ( (string)btn.Tag == "5" )
            {
                eliDestroy5.Fill = eliDestroy5.Stroke;
                txtDestroy5.Foreground = new SolidColorBrush(Colors.White);
                mintDestroy = 5;
            }
            if ((string)btn.Tag == "4")
            {
                eliDestroy4.Fill = eliDestroy5.Stroke;
                txtDestroy4.Foreground = new SolidColorBrush(Colors.White);
                mintDestroy = 4;
            }
            if ((string)btn.Tag == "3")
            {
                eliDestroy3.Fill = eliDestroy5.Stroke;
                txtDestroy3.Foreground = new SolidColorBrush(Colors.White);
                mintDestroy = 3;
            }
            if ((string)btn.Tag == "2")
            {
                eliDestroy2.Fill = eliDestroy5.Stroke;
                txtDestroy2.Foreground = new SolidColorBrush(Colors.White);
                mintDestroy = 2;
            }
            if ((string)btn.Tag == "1")
            {
                eliDestroy1.Fill = eliDestroy5.Stroke;
                txtDestroy1.Foreground = new SolidColorBrush(Colors.White);
                mintDestroy = 1;
            }

        }

        private void BtnTest_Click(object sender, RoutedEventArgs e)
        {

            var exitEvent = new ManualResetEvent(false);
            var url = new Uri("wss://m0rbnty9eg.execute-api.eu-central-1.amazonaws.com/production?c=%5B%7B%22u%22%3A%20%22abcde%22%2C%22p%22%3A%20%22ab56b4d92b40713acc5af89985d4b786%22%7D%2C%7B%22u%22%3A%20%22qwerty%22%2C%22p%22%3A%20%22d8578edf8458ce06fbc5bb76a58c5ca4%22%7D%5D");

            
            using (var client = new WebsocketClient(url))
            {
                client.ReconnectTimeout = TimeSpan.FromSeconds(30000);
                //client.ReconnectionHappened.Subscribe(info =>
                //    Log.Information($"Reconnection happened, type: {info.Type}"));
                //client.MessageReceived.Subscribe(msg => Log.Information($"Message received: {msg}"));
                client.ReconnectionHappened.Subscribe(type =>
                {
                   // Log.Information($"Reconnection happened, type: {type}, url: {client.Url}");
                });
                
                client.MessageReceived.Subscribe(msg =>
                {
                    //Log.Information($"Message received: {msg}");
                });

                
                client.Start();
                //Task.Run(() => StartSendingPing(client));
                //client.Send("ping");

                HE_ClientModel ach = new HE_ClientModel
                {
                    Action = "he",
                    Hello =  new he_Model
                    {
                        AccountID = 44,
                        DeviceID = 6,
                        CompanyID = 1,
                        Cguid = "dasdasda",
                        Os = "Windows",
                        Hw = "i3",
                        Serial = "1234567890",
                        Version = "1.0"
                    }
                };



                //Task.Run(() => client.Send("{\"ac\":\"he\",\"he\":{\"a\":44,\"d\":6,\"r\":1,\"c\":\"....\",\"o\":\"....\",\"h\":\"....\",\"s\":\"....\",\"b\":\"....\"}}"));

                string json = JsonSerializer.Serialize(ach);
                Task.Run(() => client.Send(json));

                exitEvent.WaitOne();
            }
        }

      

    


    private static async Task StartSendingPing(IWebsocketClient client)
        {
            while (true)
            {
                await Task.Delay(1000);

                if (!client.IsRunning)
                    continue;

                client.Send("ping");
            }
        }



    }


  


}
