﻿using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for MsgStatusView.xaml
    /// </summary>
    public partial class MsgStatusView : UserControl
    {
        public MsgStatusView()
        {
            InitializeComponent();
            Loaded += MsgStatusViewControl_Loaded;
        }

        public static readonly DependencyProperty MessageQueueStatusProperty =
          DependencyProperty.Register("MyMessageQueueStatus", typeof(List<Msgq>), typeof(UserControl));
        public List<Msgq> MyMessageQueueStatus
        {
            get { return (List<Msgq>)GetValue(MessageQueueStatusProperty); }
            set { SetValue(MessageQueueStatusProperty, value); }
        }

        

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks a recipient name")]
        public event EventHandler MsgStatusSelected;
        private void BtnMsgStatus_Click(object sender, RoutedEventArgs e)
        {
            //Globals.CurrentGroupID = Int32.Parse(((Button)sender).Tag.ToString());
            //Globals.CurrentAccountID = 0;

            //bubble the event up to the parent
            if (this.MsgStatusSelected != null)
                this.MsgStatusSelected(this, e);
        }


        private void MsgStatusViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("MsgStatusViewControl_Loaded ");

            partigiano_windows_app_wpf.ViewModel.MsgqViewModel msgqViewModelObject =
             new partigiano_windows_app_wpf.ViewModel.MsgqViewModel();

            // build here

            msgqViewModelObject.LoadMsgqs(MyMessageQueueStatus);
            this.DataContext = msgqViewModelObject;


        }

    }
}
