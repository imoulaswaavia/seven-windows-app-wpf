﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for RecentView.xaml
    /// </summary>
    public partial class RecentView : UserControl
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user selected a recent chat msg")]
        public event EventHandler<long> RecentChatSelected;


        public RecentView()
        {
            InitializeComponent();
        }


        private void BtnSelectMessage_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("BtnSelectMessage_Click ");
                                   
            long lngClientMsgID = Int32.Parse(((Button)sender).Tag.ToString());

            EventHandler<long> handler = RecentChatSelected;
            handler?.Invoke(null, lngClientMsgID);

          
        }


    }
}
