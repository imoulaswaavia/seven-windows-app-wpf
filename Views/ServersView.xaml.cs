﻿using partigiano_windows_app_wpf.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for ServersView.xaml
    /// </summary>
    public partial class ServersView : UserControl
    {
        int mintSelectedServerID;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user removed an account")]
        public event EventHandler ServerRemoved;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user wants to update an account")]
        public event EventHandler<int> ServerUpdate;

        public ServersView()
        {
            InitializeComponent();
        }

        private void BtnServer_Click(object sender, RoutedEventArgs e)
        {
            popServerActions.IsOpen = !popServerActions.IsOpen;
            if(popServerActions.IsOpen)
            {
                mintSelectedServerID = Int32.Parse(((Button)sender).Tag.ToString()); ;
            }
                
        }

        private void BtnDeleteServer_Click(object sender, RoutedEventArgs e)
        {
            popServerActions.IsOpen = false;
            MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to delete the account and all messages and files? This action is permanent.","Warning",MessageBoxButton.YesNo,MessageBoxImage.Question);
            if( dialogResult == MessageBoxResult.Yes )
            {
                this.Cursor = Cursors.Wait;
                DatabaseCommon.DeleteServer(mintSelectedServerID);

                EventHandler handler = ServerRemoved;
                handler?.Invoke(null, null);
                this.Cursor = Cursors.Arrow;
            }

        }

        private void BtnUpdateServer_Click(object sender, RoutedEventArgs e)
        {
            popServerActions.IsOpen = false;
            EventHandler<int> handler = ServerUpdate;
            handler?.Invoke(null, mintSelectedServerID);
        }

    }
}
