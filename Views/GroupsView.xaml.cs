﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for GroupsView.xaml
    /// </summary>
    public partial class GroupsView : UserControl
    {
        public GroupsView()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks a group")]
        public event EventHandler GroupSelected;
        private void BtnGroup_Click(object sender, RoutedEventArgs e)
        {
            Globals.CurrentGroupID = Int32.Parse(((Button)sender).Tag.ToString());
            Globals.CurrentAccountID = 0;

            //bubble the event up to the parent
            if (this.GroupSelected != null)
                this.GroupSelected(this, e);
        }

    }
}
