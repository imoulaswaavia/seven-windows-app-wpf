﻿using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for MsgqView.xaml
    /// </summary>
    public partial class MsgqView : UserControl
    {
        public MsgqView()
        {
            InitializeComponent();
            Loaded += Msgq_Loaded;
        }

        public static readonly DependencyProperty MessageQueueProperty =
          DependencyProperty.Register("MyMessageQueue", typeof(List<Msgq>), typeof(UserControl));
        public List<Msgq> MyMessageQueue
        {
            get { return (List<Msgq>)GetValue(MessageQueueProperty); }
            set { SetValue(MessageQueueProperty, value); }
        }

         public static readonly DependencyProperty MsgqClientMsgIDProperty =
            DependencyProperty.Register("MsgqClientMsgID", typeof(int), typeof(UserControl));
         public int MsgqClientMsgID
         {
            get { return (int)GetValue(MsgqClientMsgIDProperty); }
            set { SetValue(MsgqClientMsgIDProperty, value); }
         }


        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks a recipient name")]
        public event EventHandler MsgqSelected;
        private void BtnMsgq_Click(object sender, RoutedEventArgs e)
        {
            //Globals.CurrentGroupID = Int32.Parse(((Button)sender).Tag.ToString());
            //Globals.CurrentAccountID = 0;

            //bubble the event up to the parent
            if (this.MsgqSelected != null)
                this.MsgqSelected(this, e);
        }


        private void Msgq_Loaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Msgq_Loaded "+ MsgqClientMsgID);

            partigiano_windows_app_wpf.ViewModel.MsgqViewModel msgqViewModelObject =
             new partigiano_windows_app_wpf.ViewModel.MsgqViewModel();
            //msgqViewModelObject.LoadMsgqs(MsgqClientMsgID);
            //this.DataContext = msgqViewModelObject;

            // build here

            msgqViewModelObject.LoadMsgqs(MyMessageQueue);
            this.DataContext = msgqViewModelObject;

            //if (MyMessageQueue != null)
            //{
            //    foreach (Msgq q in MyMessageQueue)
            //    {
            //        Console.WriteLine("q " + q.FromAccountID);
            //    }
            //}
        }

    }
}
