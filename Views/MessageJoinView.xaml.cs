﻿using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for MsgJoinView.xaml
    /// </summary>
    public partial class MessageJoinView : UserControl
    {
        public MessageJoinView()
        {
            InitializeComponent();
        }

        private void BtnJoinCall_Click(object sender, RoutedEventArgs e)
        {
            long lngClientMsgID = Int32.Parse(((Button)sender).Tag.ToString());
            Msg _msg = DatabaseMessages.GetMsg(ClientMsgID: lngClientMsgID);

            string strOvideoDatFile = string.Format(@"{0}\ovideo\ovideo.dat", AppDomain.CurrentDomain.BaseDirectory);
            if (System.IO.File.Exists(strOvideoDatFile))
            {
                System.IO.File.Delete(strOvideoDatFile);
            }

            System.IO.File.AppendAllText(strOvideoDatFile, _msg.Body);

            var startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = string.Format(@"{0}\ovideo", AppDomain.CurrentDomain.BaseDirectory);
            startInfo.FileName = string.Format(@"{0}\ovideo\openvidu-electron.exe", AppDomain.CurrentDomain.BaseDirectory);
            Process proc = Process.Start(startInfo);
        }
    }
}
