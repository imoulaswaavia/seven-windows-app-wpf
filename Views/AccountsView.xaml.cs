﻿using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for AccountsView.xaml
    /// </summary>
    public partial class AccountsView : UserControl
    {

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user just needs to refresh UI")]
        public event EventHandler<object> AccountsRefreshUI;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user searches for a friend")]
        public event EventHandler<object> SearchFriend;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks an account")]
        public event EventHandler AccountSelected;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user wants to send a friend action")]
        public event EventHandler<object> SendFriendAction;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user removes a friend action")]
        public event EventHandler<object> RemoveFriendAction;

        int SelectedAccountID;

        public AccountsView()
        {
            InitializeComponent();
        }

        public string SearchText
        {
            get
            {
                return txtSearch.Text;
            }

            set
            {
                txtSearch.Document.Blocks.Clear();
                txtSearch.Document.Blocks.Add(new Paragraph(new Run(value)));
            }
        }

        private void BtnAccount_Click(object sender, RoutedEventArgs e)
        {
            int intAccountID = Int32.Parse(((Button)sender).Tag.ToString());
            Account _account = DatabaseCommon.GetAccount(intAccountID);
            if(_account==null)
            {
                return;
            }

            if(_account.Active!=0)
            {
                SelectedAccountID = intAccountID;
                ShowOptions(_account.Active);
                return;
            }

            txtSearch.Document.Blocks.Clear();
            txtSearch.Document.Blocks.Add(new Paragraph(new Run("")));

            popFriendAction.IsOpen = false;
            Globals.CurrentAccountID = intAccountID;
            Globals.CurrentSiteID = 0;
            Globals.CurrentGroupID = 0;
            //MessageBox.Show("account " + ((Button)sender).Tag);

            //bubble the event up to the parent
            if (this.AccountSelected != null)
                this.AccountSelected(this, e);
        }

        private void ShowOptions(int Status)
        {
            popFriendAction.IsOpen = !popFriendAction.IsOpen;
            //popFriendAction.IsOpen = false;
            //popFriendAction.IsOpen = true;
            switch (Status)
            {
                case 0: //friends
                    BtnAcceptFriend.Visibility = Visibility.Collapsed;
                    BtnDenyFriend.Visibility = Visibility.Collapsed;
                    BtnSendFriendRequest.Visibility = Visibility.Collapsed;
                    BtnRemoveFriend.Visibility = Visibility.Visible;
                    break;
                case -1: //you requested
                    BtnAcceptFriend.Visibility = Visibility.Collapsed;
                    BtnDenyFriend.Visibility = Visibility.Visible;
                    BtnSendFriendRequest.Visibility = Visibility.Collapsed;
                    BtnRemoveFriend.Visibility = Visibility.Collapsed;
                    break;
                case -2: //non friend
                    BtnAcceptFriend.Visibility = Visibility.Collapsed;
                    BtnDenyFriend.Visibility = Visibility.Collapsed;
                    BtnSendFriendRequest.Visibility = Visibility.Visible;
                    BtnRemoveFriend.Visibility = Visibility.Collapsed;
                    break;
                case -3: //friend requested
                    BtnAcceptFriend.Visibility = Visibility.Visible;
                    BtnDenyFriend.Visibility = Visibility.Visible;
                    BtnSendFriendRequest.Visibility = Visibility.Collapsed;
                    BtnRemoveFriend.Visibility = Visibility.Collapsed;
                    break;
                case -19: //you denied
                    BtnAcceptFriend.Visibility = Visibility.Visible;
                    BtnDenyFriend.Visibility = Visibility.Collapsed;
                    BtnSendFriendRequest.Visibility = Visibility.Collapsed;
                    BtnRemoveFriend.Visibility = Visibility.Collapsed;
                    break;
            }
        }
        private void BtnAccountOptions_Click(object sender, RoutedEventArgs e)
        {
            int intAccountID = Int32.Parse(((Button)sender).Tag.ToString());
            Account _account = DatabaseCommon.GetAccount(intAccountID);
            if (_account == null)
            {
                return;
            }
            SelectedAccountID = intAccountID;
            ShowOptions(_account.Active);
          
        }

        private void OnKeyDownHandler_txtSearch(object sender, KeyEventArgs e)
        {
           if (e.Key == Key.Enter)
            {
                if(txtSearch.Text.Trim()=="")
                {
                    EventHandler<object> handler = AccountsRefreshUI;
                    handler?.Invoke(null, null);
                    return;
                }
                popFriendAction.IsOpen = false;

                Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);

                FS_ClientModel fs = new FS_ClientModel
                {
                    Action="fs",
                    Fs = new fs_model
                    {
                        AccountID = _server.AccountID,
                        DeviceID = _server.DeviceID,
                        CompanyID = _server.CompanyID,
                        SearchName = txtSearch.Text.Trim(),
                        SearchAccountID = 0
                    }
                };
                EventHandler<object> handlerSearchFriend = SearchFriend;
                handlerSearchFriend?.Invoke(null, fs);
            }

        }

        private void BtnSendFriendRequest_Click(object sender, RoutedEventArgs e)
        {
            popFriendAction.IsOpen = false;
            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            FriendHeaderModel frh = new FriendHeaderModel()
            {
                FromAccountID = _server.AccountID,
                ToAccountID = SelectedAccountID,
                ActionID = 1
            };

            EventHandler<object> handler = SendFriendAction;
            handler?.Invoke(null, frh);
        }

        private void BtnRemoveFriend_Click(object sender, RoutedEventArgs e)
        {
            popFriendAction.IsOpen = false;
            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            FriendHeaderModel frh = new FriendHeaderModel()
            {
                FromAccountID = _server.AccountID,
                ToAccountID = SelectedAccountID,
                ActionID = 4
            };

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "update account set active=-2 where accountid=@accountid ";
            command.Parameters.AddWithValue("@accountid", SelectedAccountID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            EventHandler<object> handler1 = RemoveFriendAction;
            handler1?.Invoke(null, frh);

            EventHandler<object> handler2 = SendFriendAction;
            handler2?.Invoke(null, frh);
        }

        private void BtnAcceptFriend_Click(object sender, RoutedEventArgs e)
        {
            popFriendAction.IsOpen = false;
            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            FriendHeaderModel frh = new FriendHeaderModel()
            {
                FromAccountID = _server.AccountID,
                ToAccountID = SelectedAccountID,
                ActionID = 2
            };

            EventHandler<object> handler = SendFriendAction;
            handler?.Invoke(null, frh);
        }

        private void BtnDenyFriend_Click(object sender, RoutedEventArgs e)
        {
            popFriendAction.IsOpen = false;
            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            FriendHeaderModel frh = new FriendHeaderModel()
            {
                FromAccountID = _server.AccountID,
                ToAccountID = SelectedAccountID,
                ActionID = 3
            };

            EventHandler<object> handler = SendFriendAction;
            handler?.Invoke(null, frh);
        }


    }


}
