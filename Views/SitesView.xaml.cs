﻿using partigiano_windows_app_wpf.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for SitesView.xaml
    /// </summary>
    public partial class SitesView : UserControl
    {
        public SitesView()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks a site")]
        public event EventHandler SiteSelected;
        private void BtnSite_Click(object sender, RoutedEventArgs e)
        {
            Globals.CurrentSiteID = Int32.Parse(((Button)sender).Tag.ToString());
            Globals.CurrentGroupID = 0;
            Globals.CurrentAccountID = 0;
                        
            //bubble the event up to the parent
            if (this.SiteSelected != null)
                this.SiteSelected(this, e);


        }
    }
}
