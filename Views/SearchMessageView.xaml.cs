﻿using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for SearchMessageView.xaml
    /// </summary>
    public partial class SearchMessageView : UserControl
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user searches for a message")]
        public event EventHandler<string> SearchMessage;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user cancel search and need to show all messages again")]
        public event EventHandler ResetSearch;

        public SearchMessageView()
        {
            InitializeComponent();
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                EventHandler handler = ResetSearch;
                handler?.Invoke(null, null);
            }
            else
            {
                EventHandler<string> handler = SearchMessage;
                handler?.Invoke(null, txtSearch.Text.Trim());
            }


        }

        private void OnKeyDownHandler_txtSearchMessage(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtSearch.Text.Trim() == "")
                {
                    EventHandler handler = ResetSearch;
                    handler?.Invoke(null, null);
                }
                else
                {
                    EventHandler<string> handler = SearchMessage;
                    handler?.Invoke(null, txtSearch.Text.Trim());
                }
            }

        }





    }
}
