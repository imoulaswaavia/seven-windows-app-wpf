﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for AccountsView.xaml
    /// </summary>
    public partial class MsgsView : UserControl
    {
        private long SelectedClientMsgID = 0;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user replies to a msg")]
        public event EventHandler<long> ReplyToMsg;

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when need load more messages")]
        public event EventHandler LoadMoreMessages;

        Point mPointMessageOptions;

        public MsgsView()
        {
            InitializeComponent();
            MessageScrollViewer.ScrollChanged += OnScrollChanged;
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {


            var scrollViewer = (ScrollViewer)sender;

            //if (scrollViewer.ScrollableHeight==0)
            //{
            //    return;
            //}

            //if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
            //    Console.WriteLine("This is the end");

            //Console.WriteLine(scrollViewer.VerticalOffset);

            //if (scrollViewer.VerticalOffset < 20 && scrollViewer.ScrollableHeight != 0)
            //{
            //    //scrollViewer.ScrollToVerticalOffset(1);
            //    //scrollViewer.ScrollToVerticalOffset(200);
            //    //EventHandler handler = LoadMoreMessages;
            //    //handler?.Invoke(null, e);
            //    brdLoadMessages.Visibility = Visibility.Visible;
            //} else
            //{
            //    brdLoadMessages.Visibility = Visibility.Collapsed;
            //}
            brdLoadMessages.Visibility = Visibility.Visible;

            //GeneralTransform groupBoxTransform = MsgsViewControl.TransformToAncestor(MessageScrollViewer);
            //Rect rectangle = groupBoxTransform.TransformBounds(new Rect(new Point(0, 0), groupBox.RenderSize));
            //scrollViewer.ScrollToVerticalOffset(rectangle.Top + scrollViewer.VerticalOffset);
        }

        private void MsgsqViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("MsgsqViewControl_Loaded ");
            //partigiano_windows_app_wpf.ViewModel.SiteViewModel siteViewModelObject =
            //   new partigiano_windows_app_wpf.ViewModel.SiteViewModel();
            //siteViewModelObject.LoadSites();

            //SitesViewControl.DataContext = siteViewModelObject;
        }

        private void MsgStatusViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("MsgStatusViewControl_Loaded ");

        }

        private void BtnMessageOptions_Click(object sender, RoutedEventArgs e)
        {
            Button _sender = (Button)sender;
            SelectedClientMsgID = Int32.Parse(_sender.Tag.ToString());
            popMessageActions.IsOpen = !popMessageActions.IsOpen;
            popMessageInfo.IsOpen = false;
            MessageInfoControl.CloseMessageInfo += CloseMessageInfo;

            mPointMessageOptions = _sender.TransformToAncestor(this).Transform(new Point(0, 0));
        }

        private void CloseMessageInfo(object sender, EventArgs e)
        {
            popMessageInfo.IsOpen = false;
        }

        private void BtnReply_Click(object sender, RoutedEventArgs e)
        {
            popMessageActions.IsOpen = false;
            popMessageInfo.IsOpen = false;
            EventHandler<long> handler = ReplyToMsg;
            handler?.Invoke(null, SelectedClientMsgID);
        }

        private void BtnShowDetails_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(SelectedClientMsgID.ToString());

            partigiano_windows_app_wpf.ViewModel.MsgqViewModel obj =
              new partigiano_windows_app_wpf.ViewModel.MsgqViewModel();
            obj.LoadMsgqs(SelectedClientMsgID);

            MessageInfoControl.DataContext = obj;
            
            popMessageActions.IsOpen = false;

            popMessageInfo.CustomPopupPlacementCallback = new CustomPopupPlacementCallback(PopMessageInfo_Placement);
            popMessageInfo.IsOpen = true;
        }

        private void BtnLoadMessages_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("BtnLoadMessages_Click ");

            MessageScrollViewer.ScrollToVerticalOffset(100);

            EventHandler handler = LoadMoreMessages;
            handler?.Invoke(null, e);

         
        }

        public CustomPopupPlacement[] PopMessageInfo_Placement(Size popupSize,
                                           Size targetSize,
                                           Point offset)
        {
            //CustomPopupPlacement placement1 =
            //   new CustomPopupPlacement(new Point(-50, 100), PopupPrimaryAxis.Vertical);

            //CustomPopupPlacement placement2 =
            //    new CustomPopupPlacement(new Point(10, 20), PopupPrimaryAxis.Horizontal);

            //CustomPopupPlacement[] ttplaces =
            //        new CustomPopupPlacement[] { placement1, placement2 };
            Console.WriteLine(mPointMessageOptions.X + " " + mPointMessageOptions.Y);
            CustomPopupPlacement placement2 =
                new CustomPopupPlacement(new Point(mPointMessageOptions.X, mPointMessageOptions.Y), PopupPrimaryAxis.Horizontal);

            CustomPopupPlacement placement22 =
                new CustomPopupPlacement(new Point(0, 0), PopupPrimaryAxis.Horizontal);


            CustomPopupPlacement[] ttplaces =
                    new CustomPopupPlacement[] { placement2 };

            return ttplaces;
        }


    }
}
