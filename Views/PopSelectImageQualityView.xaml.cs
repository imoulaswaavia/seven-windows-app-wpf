﻿using Microsoft.Win32;
using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for PopSelectImageQualityView.xaml
    /// </summary>
    public partial class PopSelectImageQualityView : UserControl
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user selects an image to send")]
        public event EventHandler<object> SendFileHeaderMessage;

        public PopSelectImageQualityView()
        {
            InitializeComponent();
        }

        public string ImageFilename { get; set; }

        private void BtnCompressUltraLow_Click(object sender, RoutedEventArgs e)
        {
            CompressImageAndSend(1);
        }

        private void BtnCompressLow_Click(object sender, RoutedEventArgs e)
        {
            CompressImageAndSend(2);
        }
        private void BtnCompressMedium_Click(object sender, RoutedEventArgs e)
        {
            CompressImageAndSend(3);
        }
        private void BtnCompressHigh_Click(object sender, RoutedEventArgs e)
        {
            CompressImageAndSend(4);
        }

        private void CompressImageAndSend(int Compression)
        {

            var fileContent = string.Empty;
            var filePath = string.Empty;
            ImageCodecInfo jpgEncoder = ImageHandlers.GetEncoder(ImageFormat.Jpeg);
            // Create an Encoder object based on the GUID  
            // for the Quality parameter category.  
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            // Create an EncoderParameters object.  
            // An EncoderParameters object has an array of EncoderParameter  
            // objects. In this case, there is only one  
            // EncoderParameter object in the array.  
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L); //default

    
          
            int w = 0;
            int h;
                
            Bitmap originalimage = (Bitmap)System.Drawing.Image.FromFile(ImageFilename, true);
            originalimage.SetResolution(72, 72);

            if (Compression == 1 || Compression == 2)
            {
                originalimage = ImageHandlers.MakeGrayscale3(originalimage);
            }
          
            if (Compression == 1) //ULTRA COMPRESSION WIDTH 640 PIXELS
            {
                w = 640;
                myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            }
            if (Compression == 2 || Compression == 3) //HIGH AND MEDIUM COMPRESSION WIDTH 1024 PIXELS 
            {
                w = 1024;
                myEncoderParameter = new EncoderParameter(myEncoder, 75L);
            }
            if (Compression == 4) //LOW COMPRESSION WIDTH 2048 PIXELS
            {
                w = 2048;
                myEncoderParameter = new EncoderParameter(myEncoder, 80L);
            }

            myEncoderParameters.Param[0] = myEncoderParameter;

            if (originalimage.Width < w) {
                w = originalimage.Width;
            }
            h = originalimage.Height * w / originalimage.Width;

            string storageFolder = string.Format("{0}\\storage\\{1}", Globals.DataPath, Globals.CurrentServerID);
            if (!System.IO.Directory.Exists(storageFolder))
            {
                System.IO.Directory.CreateDirectory(storageFolder);
            }

            Bitmap compressedImage = ImageHandlers.ResizeImage(originalimage, w, h);

            //calculate hash of original file              
            string strTempFileName = System.IO.Path.GetTempFileName();
            compressedImage.Save(strTempFileName, jpgEncoder, myEncoderParameters);

            var sha256 = SHA256.Create();
            var hashstream = File.OpenRead(strTempFileName);
            var _sha256 = sha256.ComputeHash(hashstream);
            hashstream.Close();
            string _hash = BitConverter.ToString(_sha256).Replace("-", "").ToLowerInvariant();
              
            string previewFileName = _hash + ".p.jpg";
            string previewFullFilename = string.Format("{0}\\{1}", storageFolder, previewFileName);
            
            string compressedFilename = _hash + ".jpg";
                    
            string compressedFullFilename = string.Format("{0}\\{1}", storageFolder, compressedFilename);
            if (!System.IO.File.Exists(compressedFullFilename))
            {
                System.IO.File.Copy(strTempFileName, compressedFullFilename, true);
                System.IO.File.Delete(strTempFileName);
            }

            //create a preview image file
            ImageHandlers.CreatePreviewImage(compressedFullFilename, previewFullFilename);

            FileInfo originf = new FileInfo(ImageFilename);
            FileInfo inf = new FileInfo(compressedFullFilename);
            Server _server = DatabaseCommon.GetServer(CompanyID: Globals.CurrentCompanyID);

            int t = (int)Math.Ceiling( (double)inf.Length / 1024 / _server.FileSegment);

            string strFilename = System.IO.Path.GetFileNameWithoutExtension(originf.Name);

            //MessageBox.Show(_hash);
            FileHeaderModel fh = new FileHeaderModel
            {
                Filename = strFilename + ".jpg",
                FileSize = inf.Length,
                FileDate = (inf.CreationTime).ToString("yyyy-MM-dd HH:mm:ss"),
                TotalSegments = t,
                LastSegment = 0,
                Hash = _hash

            };

            EventHandler<object> handlerSendMessage = SendFileHeaderMessage;
            handlerSendMessage?.Invoke(null, fh);
           

            

        }


        public static string ComputeSHA256Hash(string text)
        {
            using (var sha256 = new SHA256Managed())
            {
                return BitConverter.ToString(sha256.ComputeHash(Encoding.UTF8.GetBytes(text))).Replace("-", "");
            }
        }

    }
}
