﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for MessageImageView.xaml
    /// </summary>
    public partial class MessageImageView : UserControl
    {
        public MessageImageView()
        {
            InitializeComponent();
        }

        private void MsgStatusViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Debug.WriteLine("MsgStatusViewControl_Loaded ");
        }

        private void MsgsqViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("MsgsqViewControl_Loaded ");
        }


        private void BtnShowImage_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (@btn.Tag.ToString() == "")
            {
                return;
            }
            if( System.IO.File.Exists(@btn.Tag.ToString()))
            {
                string strExtension = System.IO.Path.GetExtension(@btn.Tag.ToString()).ToLower();
                string strTempFileName = String.Format("{0}{1}",System.IO.Path.GetTempFileName(), strExtension);
                System.IO.File.Copy(@btn.Tag.ToString(), strTempFileName,true);

                if (strExtension == ".jpg" || strExtension == ".jpeg" || strExtension == ".png")
                {
                    Process.Start(strTempFileName);

                } else if (strExtension == ".mp3" || strExtension == ".wav")
                {
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    mediaPlayer.Open(new Uri(strTempFileName));
                    mediaPlayer.Play();
                } else
                {
                    Process.Start(strTempFileName);
                }
            }
            

        }
    }
}
