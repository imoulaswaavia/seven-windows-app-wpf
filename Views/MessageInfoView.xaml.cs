﻿using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using System;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for MessageInfoView.xaml
    /// </summary>
    public partial class MessageInfoView : UserControl
    {

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user closes message information window")]
        public event EventHandler CloseMessageInfo;


        public MessageInfoView()
        {
            InitializeComponent();
        }
        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            EventHandler handler = CloseMessageInfo;
            handler?.Invoke(null, null);
        }

    }

    

}
