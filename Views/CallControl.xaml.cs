﻿using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace partigiano_windows_app_wpf.Views
{
    /// <summary>
    /// Interaction logic for CallControl.xaml
    /// </summary>
    public partial class CallControl : UserControl
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user requests a call")]
        public event EventHandler<object> SendCallRequest;

        public CallControl()
        {
            InitializeComponent();
        }

        private void BtnCall_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnVideoCall_Click(object sender, RoutedEventArgs e)
        {
            popVideoCall.IsOpen = !popVideoCall.IsOpen;
        }

        private void BtnVideoUltraLow_Click(object sender, RoutedEventArgs e)
        {
            RequestVideoCall(5, 128, 96);
        }

        private void BtnVideoLow_Click(object sender, RoutedEventArgs e)
        {
            RequestVideoCall(5, 320, 240);
        }
        private void BtnVideoMedium_Click(object sender, RoutedEventArgs e)
        {
            RequestVideoCall(10, 480, 360);
        }
        private void BtnVideoHigh_Click(object sender, RoutedEventArgs e)
        {
            RequestVideoCall(10, 640, 480);
        }
        private void BtnVideoUltraHigh_Click(object sender, RoutedEventArgs e)
        {
            RequestVideoCall(15, 960, 720);
        }

        private void RequestVideoCall(int FPS, int Height, int Width)
        {
            popVideoCall.IsOpen = false;

            //create room name
            string strRoomName;
            if( Globals.CurrentGroupID > 0 )
            {
                strRoomName = String.Format("g-{0}", Globals.CurrentGroupID);
            } else
            {
                Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
                if( _server.AccountID > Globals.CurrentAccountID )
                {
                    strRoomName = String.Format("a-{0}-{1}", Globals.CurrentAccountID, _server.AccountID);
                } else
                {
                    strRoomName = String.Format("a-{0}-{1}", _server.AccountID, Globals.CurrentAccountID);
                }
            }

            CallHeaderModel ch = new CallHeaderModel()
            {
                RoomName = strRoomName,
                FPS = FPS,
                Height = Height,
                Width = Width,
                AudioEnabled = 1,
                VideoEnabled = 1
            };

            EventHandler<object> handler = SendCallRequest;
            handler?.Invoke(null, ch);
        }

    }
}
