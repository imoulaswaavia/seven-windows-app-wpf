﻿using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace partigiano_windows_app_wpf.Classes
{
    static class DatabaseCommon
    {

        public static Server GetServer(int ServerID=0, int AccountID=0, int CompanyID=0)
        {
            if(ServerID==0 && AccountID==0 && CompanyID==0)
            {
                return null;
            }
            SQLiteCommand command;
            string strSQL;
            string strClause="";

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from server where ";
            if(AccountID > 0)
            {
                strSQL += strClause + " accountid = @accountid ";
                command.Parameters.AddWithValue("@accountid", AccountID);

                strClause = " and ";
            }
            if (CompanyID > 0)
            {
                strSQL += strClause + " companyid = @companyid ";
                command.Parameters.AddWithValue("@companyid", CompanyID);

                strClause = " and ";
            }
            if (ServerID > 0)
            {
                strSQL += strClause + " serverid = @serverid ";
                command.Parameters.AddWithValue("@serverid", ServerID);

                strClause = " and ";
            }

            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    Server Servermodel = new Server
                    {
                        ServerID = (int)(long)reader["serverid"],
                        AccountID = (int)reader["accountid"],
                        DeviceID = (int)reader["deviceid"],
                        CompanyID = (int)reader["companyid"],
                        ServerDescription = reader["serverdescr"].ToString(),
                        Username = reader["username"].ToString(),
                        Password = reader["password"].ToString(),
                        SiteRoleID = (int)reader["siteroleid"],
                        Firstname = reader["firstname"].ToString(),
                        Lastname = reader["lastname"].ToString(),
                        Email = reader["email"].ToString(),
                        SiteLevel = (int)reader["sitelevel"],
                        NameLocked = (int)reader["namelocked"],
                        FileSegment = (int)reader["filesegment"],
                        Language = reader["lang"].ToString()
                    };
                    return Servermodel;
                }
            }

            return null;
        }

        public static List<int> GetServerAccountIDs()
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select accountid from server";

            command.CommandText = strSQL;

            List<int> _ids = new List<int>();

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    _ids.Add((int)reader["accountid"]);
                }
            }

            return _ids;
        }

        public static Group GetGroup(int GroupID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from accountgroup where accountgroupid=@groupid ";
            command.Parameters.AddWithValue("@groupid", GroupID);
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    Group model = new Group
                    {
                        CompanyID = Int16.Parse(reader["companyid"].ToString()),
                        AccountGroupID = Int16.Parse(reader["accountgroupid"].ToString()),
                        SiteID = Int16.Parse(reader["siteid"].ToString()),
                        Description = reader["description"].ToString(),
                        Color = reader["color"].ToString(),
                        Priority = Int16.Parse(reader["priority"].ToString()),
                        Email = reader["email"].ToString()
                    };
                    return model;
                }
            }

            return null;
        }

        public static Account GetAccount(int AccountID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from account where accountid=@accountid ";
            command.Parameters.AddWithValue("@accountid", AccountID);
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    Account model = new Account
                    {
                        CompanyID = Int32.Parse(reader["companyid"].ToString()),
                        AccountID = Int32.Parse(reader["accountid"].ToString()),
                        Firstname = reader["firstname"].ToString(),
                        Lastname = reader["lastname"].ToString(),
                        Active = Int16.Parse(reader["active"].ToString()),
                        SiteLevel = Int16.Parse(reader["sitelevel"].ToString()),
                        Lang = reader["lang"].ToString()
                    };
                    return model;
                }
            }

            return null;
        }

        public static Site GetSite(long SiteID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from site where siteid=@siteid ";
            command.Parameters.AddWithValue("@siteid", SiteID);
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    Site model = new Site
                    {
                        ServerID = Int16.Parse(reader["companyid"].ToString()),
                        CompanyID = Int16.Parse(reader["companyid"].ToString()),
                        SiteID = Int16.Parse(reader["siteid"].ToString()),
                        Description = reader["description"].ToString(),
                        CompanyLevel = Int16.Parse(reader["companylevel"].ToString())
                    };
                    return model;
                }
            }

            return null;
        }

        public static void DeleteServer(long ServerID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"delete from site where serverid=@serverid;
                       delete from account where serverid=@serverid;
                       delete from accountgroup where serverid=@serverid;
                       delete from msg where serverid=@serverid;
                       delete from msgform where serverid=@serverid;
                       delete from msgq where serverid=@serverid;
                       delete from server where serverid=@serverid;
                       delete from site where serverid=@serverid;
                       delete from siterole where serverid=@serverid";

            command.Parameters.AddWithValue("@serverid", ServerID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            string storageFolder = string.Format(@"{0}\storage\{1}", Globals.DataPath, ServerID);
            if (System.IO.Directory.Exists(storageFolder))
            {
                System.IO.Directory.Delete(storageFolder, true);
            }
        }


    }
}
