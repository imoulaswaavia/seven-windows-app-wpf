﻿using partigiano_windows_app_wpf.Classes;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Management;
using System.Net;
using System.Text;
using System.Windows;

namespace partigiano_windows_app_wpf
{
    static class Helpers
    {
        /*
         * usage 
        if (Helpers.IsWindowOpen<Window>("MyWindowName"))
        {
           // MyWindowName is open
        }

        if (Helpers.IsWindowOpen<MyCustomWindowType>())
        {
            // There is a MyCustomWindowType window open
        }

        if (Helpers.IsWindowOpen<MyCustomWindowType>("CustomWindowName"))
        {
            // There is a MyCustomWindowType window named CustomWindowName open
        }
         */

        public static bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }

        public static Window GetWindow(string name = "")
        {

            foreach(Window w in Application.Current.Windows)
            {
                if(w.Name==name)
                {
                    return w;
                }
            }
            return null;
        }

        public static void CreateStartupLink() {
            String desktopFolder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            String startupFolder = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            String strFilename = "Waavia7.appref-ms";
            if(!System.IO.File.Exists(startupFolder + @"\" + strFilename)) 
            {
                if( System.IO.File.Exists(desktopFolder + @"\" + strFilename)) 
                {
                    System.IO.File.Copy(desktopFolder + @"\" + strFilename, startupFolder + @"\" + strFilename);
                }
            }
        }

        public static void DeleteStartupLink()
        {
            String startupFolder = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            String strFilename = "Waavia7.appref-ms";
            if (System.IO.File.Exists(startupFolder + @"\" + strFilename))
            {
                System.IO.File.Delete(startupFolder + @"\" + strFilename);
            }
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}
