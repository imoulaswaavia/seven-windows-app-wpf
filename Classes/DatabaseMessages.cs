﻿using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using static partigiano_windows_app_wpf.Model.Msg;

namespace partigiano_windows_app_wpf.Classes
{
    static class DatabaseMessages
    {

        public static Msg GetMsg(string MsgUUID="", long ClientMsgID=0)
        {
            SQLiteCommand command;
            string strSQL;
            string strClause = "";
            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from msg " +
                     " where ";

            command.Parameters.Clear();

            if ( ClientMsgID > 0 )
            {
                strSQL += strClause + " clientmsgid=@clientmsgid";
                strClause = "and";
                command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);
            }
            if (MsgUUID != "")
            {
                strSQL += strClause + " msguuid=@msguuid";
                command.Parameters.AddWithValue("@msguuid", MsgUUID);
            }

            command.CommandText = strSQL;
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                { // Don't assume we have any rows.
                    DateTime? s = null;
                    s = (reader["ScheduledDate"].ToString() == "") ? null : (DateTime?)reader["ScheduledDate"];

                    int intClientMsgID = (int)(long)reader["clientmsgid"];

                    string strQuoteMsgBody = "";
                    if (reader["quotemsguuid"].ToString() != "")
                    {
                        Msg _qmsg = DatabaseMessages.GetMsg(MsgUUID: reader["quotemsguuid"].ToString());
                        if (_qmsg != null)
                        {
                            strQuoteMsgBody = _qmsg.Body;
                        }
                    }

                    Msg _msg = new Msg
                    {
                        ServerID = (int)reader["serverid"],
                        ClientMsgID = intClientMsgID,
                        CompanyID = (int)reader["companyid"],
                        MsgUUID = (string)reader["msguuid"],
                        Body = StringCipher.Decrypt((string)reader["body"], Globals.EncryptionPassword),
                        Alert = (int)reader["alert"],
                        MsgType = (int)reader["msgtype"],
                        Destroy = (int)reader["destroy"],
                        MsgDate = (DateTime)reader["msgdate"],
                        ScheduledDate = s,
                        QuoteMsgUUID = (string)reader["quotemsguuid"],
                        Header=DatabaseMessages.GetMsgHeader(intClientMsgID),
                        QuoteMsgBody=strQuoteMsgBody
                    };
                    return _msg;
                }
            }
            return null;
        }

        public static List<Msgq> GetMsgqs(int ClientMsgID)
        {
            SQLiteCommand command;
            string strSQL;
            DataTable dt = new DataTable();
            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select msgq.*," +
                     "   fromaccount.firstname as from_firstname,toaccount.firstname as to_firstname," +
                     "   fromaccount.lastname as from_lastname,toaccount.lastname as to_lastname" +
                     " from msgq" +
                     " left join account as fromaccount on msgq.fromaccountid=fromaccount.accountid" +
                     " left join account as toaccount   on msgq.toaccountid=toaccount.accountid" +
                     " where " +
                     " clientmsgid=@clientmsgid";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);

            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            List<Msgq> _msgqlist = new List<Msgq>();

            foreach (DataRow row in dt.Rows)
            {
                DateTime? d = null;
                d = (row["delivereddate"].ToString() == "") ? null : (DateTime?)row["delivereddate"];
                DateTime? r = null;
                r = (row["readdate"].ToString() == "") ? null : (DateTime?)row["readdate"];

                string FromName= row["from_firstname"].ToString() + " " + row["from_lastname"].ToString();
                //string ToName = row["to_firstname"].ToString() + " " + row["to_lastname"].ToString();
                
                Msgq _msgq = new Msgq
                {
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    Firstname = row["from_firstname"].ToString(),
                    Lastname = row["from_lastname"].ToString(),
                    ClientMsgID = Int32.Parse(row["clientmsgid"].ToString()),
                    MsgqID = Int32.Parse(row["msgqid"].ToString()),
                    FromAccountID = (int)row["fromaccountid"],
                    FromDevID = (int)row["fromdevid"],
                    ToAccountID = (int)row["toaccountid"],
                    ToDevID = (int)row["todevid"],
                    AccountGroupID = (int)row["accountgroupid"],
                    DeliveredDate = d,
                    ReadDate = r,
                    Status = (int)row["status"],
                    IsHeader = (int)row["isheader"]

                };

                _msgqlist.Add(_msgq);

            }
            return _msgqlist;
        }

        public static Msgq GetMsgHeader(int ClientMsgID)
        {
            SQLiteCommand command;
            string strSQL;
            DataTable dt = new DataTable();
            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select msgq.*," +
                     "   fromaccount.firstname as from_firstname,toaccount.firstname as to_firstname," +
                     "   fromaccount.lastname as from_lastname,toaccount.lastname as to_lastname" +
                     " from msgq" +
                     " left join account as fromaccount on msgq.fromaccountid=fromaccount.accountid" +
                     " left join account as toaccount   on msgq.toaccountid=toaccount.accountid" +
                     " where isheader=1 and" +
                     " clientmsgid=@clientmsgid";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);

            command.CommandText = strSQL;
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                { // Don't assume we have any rows.

                    DateTime? d = null;
                    d = (reader["delivereddate"].ToString() == "") ? null : (DateTime?)reader["delivereddate"];
                    DateTime? r = null;
                    r = (reader["readdate"].ToString() == "") ? null : (DateTime?)reader["readdate"];

                    string FromName = reader["from_firstname"].ToString() + " " + reader["from_lastname"].ToString();
                    //string ToName = row["to_firstname"].ToString() + " " + row["to_lastname"].ToString();

                    Msgq _msgq = new Msgq
                    {
                        ServerID = Int32.Parse(reader["serverid"].ToString()),
                        Firstname = reader["from_firstname"].ToString(),
                        Lastname = reader["from_lastname"].ToString(),
                        ClientMsgID = Int32.Parse(reader["clientmsgid"].ToString()),
                        MsgqID = Int32.Parse(reader["msgqid"].ToString()),
                        FromAccountID = (int)reader["fromaccountid"],
                        FromDevID = (int)reader["fromdevid"],
                        ToAccountID = (int)reader["toaccountid"],
                        ToDevID = (int)reader["todevid"],
                        AccountGroupID = (int)reader["accountgroupid"],
                        DeliveredDate = d,
                        ReadDate = r,
                        Status = (int)reader["status"],
                        IsHeader = (int)reader["isheader"]

                    };
                    return _msgq;

                }
            }
            return null;
        }


        public static Msgq GetMsgq(long ClientMsgID, int FromAccountID, int FromDevID, int ToAccountID, int ToDevID)
        {
            SQLiteCommand command;
            string strSQL;
            DataTable dt = new DataTable();
            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select msgq.*," +
                     "   fromaccount.firstname as from_firstname,toaccount.firstname as to_firstname," +
                     "   fromaccount.lastname as from_lastname,toaccount.lastname as to_lastname" +
                     " from msgq" +
                     " left join account as fromaccount on msgq.fromaccountid=fromaccount.accountid" +
                     " left join account as toaccount   on msgq.toaccountid=toaccount.accountid" +
                     " where isheader=0 " +
                     " and clientmsgid=@clientmsgid" +
                     " and fromaccountid=@fromaccountid" +
                     " and fromdevid=@fromdevid" +
                     " and toaccountid=@toaccountid" +
                     " and todevid=@todevid";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);
            command.Parameters.AddWithValue("@fromaccountid", FromAccountID);
            command.Parameters.AddWithValue("@fromdevid", FromDevID);
            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.Parameters.AddWithValue("@todevid", ToDevID);

            command.CommandText = strSQL;
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read())
                { // Don't assume we have any rows.

                    DateTime? d = null;
                    d = (reader["delivereddate"].ToString() == "") ? null : (DateTime?)reader["delivereddate"];
                    DateTime? r = null;
                    r = (reader["readdate"].ToString() == "") ? null : (DateTime?)reader["readdate"];

                    string FromName = reader["from_firstname"].ToString() + " " + reader["from_lastname"].ToString();
                    //string ToName = row["to_firstname"].ToString() + " " + row["to_lastname"].ToString();

                    Msgq _msgq = new Msgq
                    {
                        ServerID = Int32.Parse(reader["serverid"].ToString()),
                        Firstname = reader["from_firstname"].ToString(),
                        Lastname = reader["from_lastname"].ToString(),
                        ClientMsgID = Int32.Parse(reader["clientmsgid"].ToString()),
                        MsgqID = Int32.Parse(reader["msgqid"].ToString()),
                        FromAccountID = (int)reader["fromaccountid"],
                        FromDevID = (int)reader["fromdevid"],
                        ToAccountID = (int)reader["toaccountid"],
                        ToDevID = (int)reader["todevid"],
                        AccountGroupID = (int)reader["accountgroupid"],
                        DeliveredDate = d,
                        ReadDate = r,
                        Status = (int)reader["status"],
                        IsHeader = (int)reader["isheader"]

                    };
                    return _msgq;

                }
            }
            return null;
        }

        public static long SaveMsg(int ServerID,
                                   string MsgUUID,
                                   int CompanyID,
                                   int FromAccountID,
                                   int FromDeviceID,
                                   int ToAccountID, 
                                   int ToDeviceID,
                                   int AccountGroupID,
                                   string Message,
                                   int Alert, int DestroyMinutes, 
                                   DateTime? Scheduleddate, 
                                   DateTime MsgDate,
                                   DateTime CreatedDate,
                                   int MsgType,
                                   string QuoteMsgUUID,
                                   int Status)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "insert into msg (  " +
                            " serverid," +
                            " companyid," +
                            " msguuid," +
                            " body," +
                            " alert," +
                            " msgtype," +
                            " destroy," +
                            " msgdate," +
                            " scheduleddate," +
                            " createddate," +
                            " quotemsguuid," +
                            " qstatus," +
                            " filestatus" +
                        " ) values ( " +
                            " @serverid," +
                            " @companyid," +
                            " @msguuid," +
                            " @body," +
                            " @alert," +
                            " @msgtype," +
                            " @destroy," +
                            " @msgdate," +
                            " @scheduleddate," +
                            " @createddate," +
                            " @quotemsguuid," +
                            " @qstatus," +
                            " @filestatus" +
                        " )";
            strSQL += ";select last_insert_rowid()";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@serverid", ServerID);
            command.Parameters.AddWithValue("@companyid", CompanyID);
            command.Parameters.AddWithValue("@msguuid", MsgUUID);
            command.Parameters.AddWithValue("@body", StringCipher.Encrypt(Message,Globals.EncryptionPassword));
            command.Parameters.AddWithValue("@alert", Alert);
            command.Parameters.AddWithValue("@msgtype", MsgType);
            command.Parameters.AddWithValue("@destroy", DestroyMinutes);
            command.Parameters.AddWithValue("@msgdate", String.Format("{0:yyyy-MM-dd HH:mm:ss}", MsgDate));
            command.Parameters.AddWithValue("@scheduleddate", Scheduleddate);
            command.Parameters.AddWithValue("@createddate", String.Format("{0:yyyy-MM-dd HH:mm:ss}", CreatedDate));
            command.Parameters.AddWithValue("@quotemsguuid", QuoteMsgUUID);
            command.Parameters.AddWithValue("@qstatus", 0); //noone read the message yet
            command.Parameters.AddWithValue("@filestatus", 0); //no download started

            command.CommandText = strSQL;
            long clientmessageid = 0;
            try
            {
                clientmessageid = (long)command.ExecuteScalar();
            } catch(System.Data.SQLite.SQLiteException err)
            {   //if error --->message exists
                Console.WriteLine(err.Data);
                //System.Windows.MessageBox.Show(err.Data.ToString(),"Database Error",System.Windows.MessageBoxButton.OK,System.Windows.MessageBoxImage.Error);
                return 0;
            }

            //strSQL = "insert into msgq (  " +
            //               " serverid," +
            //               " companyid," +
            //               " clientmsgid," +
            //               " fromaccountid," +
            //               " fromdevid," +
            //               " toaccountid," +
            //               " todevid," +
            //               " accountgroupid," +
            //               " delivereddate," +
            //               " readdate," +
            //               " status," +
            //               " isheader" +
            //           " ) values ( " +
            //               " @serverid," +
            //               " @companyid," +
            //               " @clientmsgid," +
            //               " @fromaccountid," +
            //               " @fromdevid," +
            //               " @toaccountid," +
            //               " @todevid," +
            //               " @accountgroupid," +
            //               " @delivereddate," +
            //               " @readdate," +
            //               " @status," +
            //               " @isheader" +
            //           " )";
            //command.Parameters.Clear();
            //command.Parameters.AddWithValue("@serverid", ServerID);
            //command.Parameters.AddWithValue("@companyid", CompanyID);
            //command.Parameters.AddWithValue("@clientmsgid", clientmessageid);
            //command.Parameters.AddWithValue("@fromaccountid", FromAccountID);
            //command.Parameters.AddWithValue("@fromdevid", FromDeviceID);
            //command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            //command.Parameters.AddWithValue("@todevid", ToDeviceID);
            //command.Parameters.AddWithValue("@accountgroupid", AccountGroupID);
            //command.Parameters.AddWithValue("@delivereddate", DBNull.Value);
            //command.Parameters.AddWithValue("@readdate", DBNull.Value);
            //command.Parameters.AddWithValue("@status", Status);
            //command.Parameters.AddWithValue("@isheader", 1);

            //command.CommandText = strSQL;
            //command.ExecuteNonQuery();

            long lngMsgQID = SaveMsgQ(clientmessageid, ServerID, CompanyID, 
                                      FromAccountID, FromDeviceID, 
                                      ToAccountID, ToDeviceID, 
                                      AccountGroupID,
                                      DateTime.UtcNow,
                                      null,
                                      Status,1);

            return clientmessageid;
        }

        public static long SaveMsgQ(long ClientMsgID,
                                   int ServerID,
                                   int CompanyID,
                                   int FromAccountID,
                                   int FromDeviceID,
                                   int ToAccountID,
                                   int ToDeviceID,
                                   int AccountGroupID,
                                   DateTime? DeliveredDate,
                                   DateTime? ReadDate,
                                   int Status,
                                   int Header)
        {
            long clientmessageQid;
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "insert into msgq (  " +
                         " serverid," +
                         " companyid," +
                         " clientmsgid," +
                         " fromaccountid," +
                         " fromdevid," +
                         " toaccountid," +
                         " todevid," +
                         " accountgroupid," +
                         " delivereddate," +
                         " readdate," +
                         " status," +
                         " isheader" +
                     " ) values ( " +
                         " @serverid," +
                         " @companyid," +
                         " @clientmsgid," +
                         " @fromaccountid," +
                         " @fromdevid," +
                         " @toaccountid," +
                         " @todevid," +
                         " @accountgroupid," +
                         " @delivereddate," +
                         " @readdate," +
                         " @status," +
                         " @isheader" +
                     " )";
            strSQL += ";select last_insert_rowid()";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@serverid", ServerID);
            command.Parameters.AddWithValue("@companyid", CompanyID);
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);
            command.Parameters.AddWithValue("@fromaccountid", FromAccountID);
            command.Parameters.AddWithValue("@fromdevid", FromDeviceID);
            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.Parameters.AddWithValue("@todevid", ToDeviceID);
            command.Parameters.AddWithValue("@accountgroupid", AccountGroupID);
            command.Parameters.AddWithValue("@delivereddate",DeliveredDate);
            command.Parameters.AddWithValue("@readdate", ReadDate);
            command.Parameters.AddWithValue("@status", Status);
            command.Parameters.AddWithValue("@isheader", Header);

            command.CommandText = strSQL;

            clientmessageQid = (long)command.ExecuteScalar();

            return clientmessageQid;

        }

        public static List<int> GetUnsentMsgs()
        {
            SQLiteCommand command;
            string strSQL;
            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select msg.clientmsgid 
                     from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     where isheader=1 and status=1";

            List<int> _ids = new List<int>();

            command.Parameters.Clear();
            command.CommandText = strSQL;
            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                { // Don't assume we have any rows.

                    _ids.Add( Int32.Parse(reader["clientmsgid"].ToString()) );
                }
            }
            return _ids;
        }

        public static int GetUnreadMsgsFromAccount(int FromAccountID,int ToAccountID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select count(*) from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     where fromaccountid=@fromaccountid 
                     and toaccountid=@toaccountid and fromaccountid<>@toaccountid 
                     and accountgroupid=0 
                     and qstatus=0 
                     and (msgtype=0 or msgtype=1)
                     and isheader=1";

            command.Parameters.AddWithValue("@fromaccountid", FromAccountID);
            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.CommandText = strSQL;

            int unread = (int)(long)command.ExecuteScalar();

            return unread;
        }

        public static int GetUnreadMsgsFromAllAccounts(int ToAccountID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select count(*) from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     where toaccountid=@toaccountid and fromaccountid<>@toaccountid
                     and accountgroupid=0 
                     and qstatus=0 
                     and (msgtype=0 or msgtype=1)
                     and isheader=1";

            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.CommandText = strSQL;

            int unread = (int)(long)command.ExecuteScalar();

            return unread;
        }

        public static int GetUnreadMsgsFromAllSites(int ToAccountID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select count(*) from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     where toaccountid=@toaccountid and fromaccountid<>@toaccountid
                     and accountgroupid>0 
                     and qstatus=0 
                     and (msgtype=0 or msgtype=1)
                     and isheader=1";

            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.CommandText = strSQL;

            int unread = (int)(long)command.ExecuteScalar();

            return unread;
        }

        public static int GetUnreadMsgsFromSite(int ToAccountID, int SiteID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select count(*) from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     inner join accountgroup on msgq.accountgroupid=accountgroup.accountgroupid 
                     where toaccountid=@toaccountid and fromaccountid<>@toaccountid
                     and siteid=@siteid
                     and msgq.accountgroupid>0 
                     and qstatus=0 
                     and isheader=1";

            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.Parameters.AddWithValue("@siteid", SiteID);
            command.CommandText = strSQL;

            int unread = (int)(long)command.ExecuteScalar();

            return unread;
        }

        public static int GetUnreadMsgsFromGroup(int ToAccountID, int AccountGroupID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"select count(*) from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid 
                     inner join accountgroup on msgq.accountgroupid=accountgroup.accountgroupid 
                     where msgq.accountgroupid=@accountgroupid 
                     and toaccountid=@toaccountid and fromaccountid<>@toaccountid
                     and qstatus=0 
                     and (msgtype=0 or msgtype=1)
                     and isheader=1";

            command.Parameters.AddWithValue("@accountgroupid", AccountGroupID);
            command.Parameters.AddWithValue("@toaccountid", ToAccountID);
            command.CommandText = strSQL;

            int unread = (int)(long)command.ExecuteScalar();

            return unread;
        }

        public static void DeleteMsg(long ClientMsgID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"delete from msg where clientmsgid=@clientmsgid;
                       delete from msgq where clientmsgid=@clientmsgid";

            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);
            command.CommandText = strSQL;

            command.ExecuteNonQuery();

        }

    }

}
