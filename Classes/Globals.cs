﻿using partigiano_windows_app_wpf.Classes;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Management;
using System.Net;

namespace partigiano_windows_app_wpf
{
    static class Globals
    {
        public static SQLiteConnection Connection;
        public static String DataPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\Waavia7";
        public static String ClientGuid;

        public static String EncryptionPassword = "daDFhu3849!329uG";
        public static int CurrentCompanyID = 0;
        public static int CurrentSiteID = 0;
        public static int CurrentGroupID = 0;
        public static int CurrentAccountID = 0;
        public static int CurrentServerID = 0;

        public static bool IsOnline = false;

        public static int PendingMessagesToReceive = 0;

        public static List<int> ServerAccountIDs;  //own account IDs

        public static AWSSocket AwsSocketClient = new AWSSocket();

        public static void CheckFolders()
        {
            if (!System.IO.Directory.Exists(Globals.DataPath))
            {
                System.IO.Directory.CreateDirectory(Globals.DataPath);
            }
        }

        public static void CheckGuid()
        {
            String strGuidFile = Globals.DataPath + "//g.dat";

            if (System.IO.File.Exists(strGuidFile))
            {
                Globals.ClientGuid = Encryption.TextDecrypt(System.IO.File.ReadAllText(strGuidFile));
            }
            else
            {
                Guid g = Guid.NewGuid();

                System.IO.File.WriteAllText(strGuidFile, Encryption.TextEncrypt(g.ToString()));
                Globals.ClientGuid = g.ToString();
            }

        }

        public static string getCpuID()
        {
            String cpuInfo = "";
            ManagementClass managClass = new ManagementClass("win32_processor");
            ManagementObjectCollection managCollec = managClass.GetInstances();

            foreach (ManagementObject managObj in managCollec)
            {
                if (managObj.Properties["processorID"].Value != null)
                {
                    cpuInfo = managObj.Properties["processorID"].Value.ToString();
                }
                break;
            }
            return cpuInfo;
        }

        public static string getBoardSerialNumber()
        {
            string SerialNumber = "";
            string query = "SELECT * FROM Win32_BaseBoard";
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher(query);
            foreach (ManagementObject info in searcher.Get())
            {
                SerialNumber += info.GetPropertyValue("SerialNumber").ToString();
            }

            return SerialNumber.Trim();

        }

        public static string getOSInfo()
        {
            //Get Operating system information.
            OperatingSystem os = Environment.OSVersion;
            //Get version information about the os.
            Version vs = os.Version;

            //Variable to hold our return value
            string operatingSystem = "";

            if (os.Platform == PlatformID.Win32Windows)
            {
                //This is a pre-NT version of Windows
                switch (vs.Minor)
                {
                    case 0:
                        operatingSystem = "95";
                        break;
                    case 10:
                        if (vs.Revision.ToString() == "2222A")
                            operatingSystem = "98SE";
                        else
                            operatingSystem = "98";
                        break;
                    case 90:
                        operatingSystem = "Me";
                        break;
                    default:
                        break;
                }
            }
            else if (os.Platform == PlatformID.Win32NT)
            {
                switch (vs.Major)
                {
                    case 3:
                        operatingSystem = "NT 3.51";
                        break;
                    case 4:
                        operatingSystem = "NT 4.0";
                        break;
                    case 5:
                        if (vs.Minor == 0)
                            operatingSystem = "2000";
                        else
                            operatingSystem = "XP";
                        break;
                    case 6:
                        if (vs.Minor == 0)
                            operatingSystem = "Vista";
                        else if (vs.Minor == 1)
                            operatingSystem = "7";
                        else if (vs.Minor == 2)
                            operatingSystem = "8";
                        else
                            operatingSystem = "8.1";
                        break;
                    case 10:
                        operatingSystem = "10";
                        break;
                    default:
                        break;
                }
            }

            return String.Format("Windows {0}",operatingSystem);
        }

        public static void ApplicationUpgrade()
        {
            string appFileName = "Waavia7_Launcher.application";
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string destFile = string.Format(@"{0}\{1}", docFolder, appFileName);
            try {
                if (System.IO.File.Exists(destFile)) {
                    System.IO.File.Delete(destFile);
                }

                using (var client = new WebClient())
                {
                    client.DownloadFile(string.Format("http://downloads.waavia7.com/windows/{0}",appFileName), destFile);
                }

                if (System.IO.File.Exists(destFile)) {
                    System.Diagnostics.Process.Start(destFile);
                    System.Windows.Application.Current.Shutdown();
                }
            }
            catch {

            }

        }

    }
}
