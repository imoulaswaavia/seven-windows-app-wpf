﻿using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.Classes
{
    static class Api
    {

        public static List<HE_ClientModel> GetHelloModels()
        {
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select accountid from Server order by accountid";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            List<HE_ClientModel> Hellolist = new List<HE_ClientModel>();

            foreach (DataRow row in dt.Rows)
            {
                HE_ClientModel c = GetHello(Int32.Parse(row["accountid"].ToString()));

                Hellolist.Add(c);

            }

            return Hellolist;
        }

        public static int HandleHello(Object helloobject)
        {
            HE_ServerModel hello = JsonSerializer.Deserialize<HE_ServerModel>(helloobject.ToString());

            Globals.PendingMessagesToReceive += hello.PendingMessages;

            //Console.WriteLine(hello.CompanyDescription);
            Server _server = DatabaseCommon.GetServer(AccountID: hello.AccountID);
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "update Server set " +
                     " serverdescr=@serverdescr," +
                     " siteroleid=@siteroleid," +
                     " firstname=@firstname," +
                     " lastname=@lastname," +
                     " email=@email," +
                     " sitelevel=@sitelevel," +
                     " namelocked=@namelocked," +
                     " filesegment=@filesegment," +
                     " lang=@lang" +
                     " WHERE serverid=@serverid and accountid=@accountid and companyid=@companyid";

            command.Parameters.Add("@serverdescr", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@siteroleid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@firstname", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@lastname", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@email", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@sitelevel", (DbType)SqlDbType.Int);
            command.Parameters.Add("@namelocked", (DbType)SqlDbType.Int);
            command.Parameters.Add("@filesegment", (DbType)SqlDbType.Int);
            command.Parameters.Add("@lang", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@accountid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);

            command.Parameters["@serverdescr"].Value = hello.CompanyDescription;
            command.Parameters["@siteroleid"].Value = hello.SiteRoleID;
            command.Parameters["@firstname"].Value = hello.FirstName;
            command.Parameters["@lastname"].Value = hello.Lastname;
            command.Parameters["@email"].Value = hello.Email;
            command.Parameters["@sitelevel"].Value = hello.SiteLevel;
            command.Parameters["@namelocked"].Value = hello.NameLocked;
            command.Parameters["@filesegment"].Value = hello.FileSegment;
            command.Parameters["@lang"].Value = hello.Language;
            command.Parameters["@accountid"].Value = hello.AccountID;
            command.Parameters["@companyid"].Value = hello.CompanyID;
            command.Parameters["@serverid"].Value = _server.ServerID;
            command.CommandText = strSQL;

            command.ExecuteNonQuery();

            return hello.AccountID;
        }
        private static HE_ClientModel GetHello(int AccountID)
        {

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from Server where accountid=@accountid ";
            command.Parameters.Add("@accountid", (DbType)SqlDbType.Int);
            command.Parameters["@accountid"].Value = AccountID;
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {

                    HE_ClientModel ach = new HE_ClientModel
                    {
                        Action = "he",
                        Hello = new he_Model
                        {
                            AccountID = (int)reader["accountid"],
                            DeviceID = (int)reader["deviceid"],
                            CompanyID = (int)reader["companyid"],
                            Cguid = Globals.ClientGuid,
                            Os = Globals.getOSInfo(),
                            Hw = Globals.getCpuID(),
                            Serial = Globals.getBoardSerialNumber(),
                            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString()
                        }
                    };

                    return ach;
                }
            }

            return null;
        }


        public static FR_ClientModel GetFriends(int AccountID)
        {
            Server _server = DatabaseCommon.GetServer(AccountID: AccountID);
            if (_server == null)
            {
                return null;
            }

            FR_ClientModel acfr = new FR_ClientModel
            {
                Action = "fr",
                Fr = new fr_model
                {
                    AccountID = _server.AccountID,
                    DeviceID = _server.DeviceID,
                    CompanyID = _server.CompanyID
                }
            };

            return acfr;
        }

        public static void HandleFriends(Object friendsobject)
        {
            FR_ServerModel friends = JsonSerializer.Deserialize<FR_ServerModel>(friendsobject.ToString());
            //Console.WriteLine(hello.CompanyDescription);
            Server _server = DatabaseCommon.GetServer(AccountID: friends.AccountID);
            DataTable dt = new DataTable();
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            List<int> friendsList = new List<int>();
            foreach(frl_model fr in friends.Friends)
            {
                friendsList.Add(fr.AccountID);
                strSQL = "update account set " +
                         " siteroleid=@siteroleid," +
                         " firstname=@firstname," +
                         " lastname=@lastname," +
                         " email=@email," +
                         " active=@active," +
                         " sitelevel=@sitelevel," +
                         " lang=@lang" +
                         " WHERE serverid=@serverid and accountid=@accountid and companyid=@companyid";

                command.Parameters.Clear();
                command.Parameters.Add("@siteroleid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@firstname", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@lastname", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@email", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@active", (DbType)SqlDbType.Int);
                command.Parameters.Add("@sitelevel", (DbType)SqlDbType.Int);
                command.Parameters.Add("@lang", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@accountid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);

                command.Parameters["@siteroleid"].Value = fr.SiteRoleID;
                command.Parameters["@firstname"].Value = fr.Firstname;
                command.Parameters["@lastname"].Value = fr.Lastname;
                command.Parameters["@email"].Value = fr.Email;
                command.Parameters["@active"].Value = fr.Active;
                command.Parameters["@sitelevel"].Value = fr.SiteLevel;
                command.Parameters["@lang"].Value = fr.Language;
                command.Parameters["@serverid"].Value = _server.ServerID;
                command.Parameters["@accountid"].Value = fr.AccountID;
                command.Parameters["@companyid"].Value = friends.CompanyID;
                command.CommandText = strSQL;

                int rowsupdated = command.ExecuteNonQuery();

                if( rowsupdated==0) {
                    strSQL = "insert into account (  " +
                                 " serverid," +
                                 " siteroleid," +
                                 " firstname," +
                                 " lastname," +
                                 " email," +
                                 " active," +
                                 " sitelevel," +
                                 " lang," +
                                 " accountid," +
                                 " companyid" +
                             " ) values ( " +
                                 " @serverid," +
                                 " @siteroleid," +
                                 " @firstname," +
                                 " @lastname," +
                                 " @email," +
                                 " @active," +
                                 " @sitelevel," +
                                 " @lang," +
                                 " @accountid," +
                                 " @companyid" +
                             " )";
                    strSQL += ";select last_insert_rowid()";
                    command.CommandText = strSQL;
                    long last_inserted_id = (long)command.ExecuteScalar();
                }
            } //foreach
            //string.Join(",", friendsList)

            //cannot set as parameters
            //check https://stackoverflow.com/questions/2377506/pass-array-parameter-in-sqlcommand
            strSQL = "update account set active=-2 where accountid NOT in ( "+ string.Join(",", friendsList) + " )";
            command.Parameters.Clear();
            command.CommandText = strSQL;
            long totalaccounts = (long)command.ExecuteNonQuery();

            return;
        }

        public static void HandleSearchFriends(Object friendsobject)
        {
            FS_ServerModel friends = JsonSerializer.Deserialize<FS_ServerModel>(friendsobject.ToString());
            //Console.WriteLine(hello.CompanyDescription);
            Server _server = DatabaseCommon.GetServer(AccountID: friends.AccountID);

            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            foreach (fsl_model fs in friends.Friends)
            {
                strSQL = "update account set " +
                         " siteroleid=@siteroleid," +
                         " firstname=@firstname," +
                         " lastname=@lastname," +
                         " email=@email," +
                         " sitelevel=@sitelevel," +
                         " lang=@lang" +
                         " WHERE serverid=@serverid and accountid=@accountid and companyid=@companyid";

                command.Parameters.Clear();
                command.Parameters.Add("@siteroleid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@firstname", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@lastname", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@email", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@sitelevel", (DbType)SqlDbType.Int);
                command.Parameters.Add("@lang", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@accountid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);

                command.Parameters["@siteroleid"].Value = fs.SiteRoleID;
                command.Parameters["@firstname"].Value = fs.Firstname;
                command.Parameters["@lastname"].Value = fs.Lastname;
                command.Parameters["@email"].Value = fs.Email;
                command.Parameters["@sitelevel"].Value = fs.SiteLevel;
                command.Parameters["@lang"].Value = fs.Language;
                command.Parameters["@serverid"].Value = _server.ServerID;
                command.Parameters["@accountid"].Value = fs.AccountID;
                command.Parameters["@companyid"].Value = friends.CompanyID;
                command.CommandText = strSQL;

                int rowsupdated = command.ExecuteNonQuery();

                if (rowsupdated == 0)
                {   //save non friend to database
                    strSQL = "insert into account (  " +
                                 " serverid," +
                                 " siteroleid," +
                                 " firstname," +
                                 " lastname," +
                                 " email," +
                                 " active," +
                                 " sitelevel," +
                                 " lang," +
                                 " accountid," +
                                 " companyid" +
                             " ) values ( " +
                                 " @serverid," +
                                 " @siteroleid," +
                                 " @firstname," +
                                 " @lastname," +
                                 " @email," +
                                 " @active," +
                                 " @sitelevel," +
                                 " @lang," +
                                 " @accountid," +
                                 " @companyid" +
                             " )";
                    strSQL += ";select last_insert_rowid()";
                    command.Parameters.AddWithValue("@active", -2);
                    command.CommandText = strSQL;
                    long last_inserted_id = (long)command.ExecuteScalar();
                }
            } //foreach
            return;
        }

        public static SG_ClientModel GetSitesAndGroups(int AccountID)
        {
            Server _server = DatabaseCommon.GetServer(AccountID:AccountID);
            if (_server == null)
            {
                return null;
            }

            SG_ClientModel acsg = new SG_ClientModel
            {
                Action = "sg",
                Sg = new sg_Model
                {
                    AccountID = _server.AccountID,
                    DeviceID = _server.DeviceID,
                    CompanyID = _server.CompanyID
                }
            };

            return acsg;
        }

        public static SR_ClientModel GetSitesRoles(int AccountID)
        {
            Server _server = DatabaseCommon.GetServer(AccountID:AccountID);
            if (_server == null)
            {
                return null;
            }

            SR_ClientModel acsr = new SR_ClientModel
            {
                Action = "sr",
                Sr = new sr_Model
                {
                    AccountID = _server.AccountID,
                    DeviceID = _server.DeviceID,
                    CompanyID = _server.CompanyID
                }
            };

            return acsr;
        }

        public static void HandleSitesAndGroups(Object sgobject)
        {
            SG_ServerModel sg = JsonSerializer.Deserialize<SG_ServerModel>(sgobject.ToString());
            //Console.WriteLine(hello.CompanyDescription);
            Server _server = DatabaseCommon.GetServer(AccountID: sg.AccountID);
            if( _server==null)
            {
                return;
            }
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            foreach (sg_site_model s in sg.Sites)
            {
                strSQL = "update site set " +
                         " description=@description," +
                         " companylevel=@companylevel" +
                         " WHERE serverid=@serverid and siteid=@siteid and companyid=@companyid";

                command.Parameters.Clear();
                command.Parameters.Add("@description", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@companylevel", (DbType)SqlDbType.Int);
                command.Parameters.Add("@siteid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);

                command.Parameters["@description"].Value = s.Description;
                command.Parameters["@companylevel"].Value = s.CompanyLevel;
                command.Parameters["@siteid"].Value = s.SiteID;
                command.Parameters["@serverid"].Value = _server.ServerID;
                command.Parameters["@companyid"].Value = sg.CompanyID;
                command.CommandText = strSQL;

                int rowsupdated = command.ExecuteNonQuery();

                if (rowsupdated == 0)
                {
                    strSQL = "insert into site (  " +
                                 " serverid," +
                                 " siteid," +
                                 " companylevel," +
                                 " description," +
                                 " companyid" +
                             " ) values ( " +
                                 " @serverid," +
                                 " @siteid," +
                                 " @companylevel," +
                                 " @description," +
                                 " @companyid" +
                             " )";
                    strSQL += ";select last_insert_rowid()";
                    command.CommandText = strSQL;
                    long last_inserted_id = (long)command.ExecuteScalar();
                }
            } //foreach


           

            foreach (sg_group_model s in sg.Groups)
            {
                strSQL = "update accountgroup set " +
                         " description=@description," +
                         " priority=@priority," +
                         " color=@color," +
                         " email=@email" +
                         " WHERE serverid=@serverid and accountgroupid=@accountgroupid and siteid=@siteid and companyid=@companyid";

                command.Parameters.Clear();
                command.Parameters.Add("@description", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@priority", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@color", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@email", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@accountgroupid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@siteid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);

                command.Parameters["@description"].Value = s.Description;
                command.Parameters["@priority"].Value = s.Priority;
                command.Parameters["@color"].Value = s.Color;
                command.Parameters["@email"].Value = s.Email;
                command.Parameters["@accountgroupid"].Value = s.AccountGroupID;
                command.Parameters["@siteid"].Value = s.SiteID;
                command.Parameters["@companyid"].Value = sg.CompanyID;
                command.Parameters["@serverid"].Value = _server.ServerID;
                command.CommandText = strSQL;

                int rowsupdated = command.ExecuteNonQuery();

                if (rowsupdated == 0)
                {
                    strSQL = "insert into accountgroup (  " +
                                 " serverid," +
                                 " accountgroupid," +
                                 " companyid," +
                                 " siteid," +
                                 " description," +
                                 " priority," +
                                 " color," +
                                 " email" +
                             " ) values ( " +
                                 " @serverid," +
                                 " @accountgroupid," +
                                 " @companyid," +
                                 " @siteid," +
                                 " @description," +
                                 " @priority," +
                                 " @color," +
                                 " @email" +
                             " )";
                    strSQL += ";select last_insert_rowid()";
                    command.CommandText = strSQL;
                    long last_inserted_id = (long)command.ExecuteScalar();
                }
            } //foreach

            strSQL = @"select siteid from site WHERE serverid=@serverid";
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@serverid", _server.ServerID);
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    int intSiteID = Int16.Parse(reader["siteid"].ToString());
                    bool found = false;
                    foreach (sg_site_model s in sg.Sites)
                    {
                        if (intSiteID == s.SiteID)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        strSQL = @"delete from site WHERE serverid=@serverid and siteid=@siteid;
                               delete from accountgroup WHERE serverid=@serverid and siteid=@siteid";
                        SQLiteCommand command2;
                        command2 = new SQLiteCommand
                        {
                            Connection = Globals.Connection
                        };
                        command2.Parameters.Clear();
                        command2.Parameters.AddWithValue("@siteid", intSiteID);
                        command2.Parameters.AddWithValue("@serverid", _server.ServerID);
                        command2.CommandText = strSQL;
                        command2.ExecuteNonQuery();
                    }
                }
            }



            return;
        }

        public static void HandleSitesRoles(Object srobject)
        {
            SR_ServerModel sr = JsonSerializer.Deserialize<SR_ServerModel>(srobject.ToString());
            Server _server = DatabaseCommon.GetServer(AccountID: sr.AccountID);
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            foreach (sr_model s in sr.SiteRoles)
            {
                strSQL = "update siterole set " +
                         " companyid=@companyid," +
                         " description=@description" +
                         " WHERE serverid=@serverid and siteroleid=@siteroleid";

                command.Parameters.Clear();
                command.Parameters.Add("@description", (DbType)SqlDbType.VarChar);
                command.Parameters.Add("@siteroleid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@serverid", (DbType)SqlDbType.Int);
                command.Parameters.Add("@companyid", (DbType)SqlDbType.Int);

                command.Parameters["@description"].Value = s.Description;
                command.Parameters["@siteroleid"].Value = s.SiteRoleID;
                command.Parameters["@serverid"].Value = _server.ServerID;
                command.Parameters["@companyid"].Value = sr.CompanyID;
                command.CommandText = strSQL;

                int rowsupdated = command.ExecuteNonQuery();

                if (rowsupdated == 0)
                {
                    strSQL = "insert into siterole (  " +
                                 " serverid," +
                                 " siteroleid," +
                                 " companyid," +
                                 " description" +
                             " ) values ( " +
                                 " @serverid," +
                                 " @siteroleid," +
                                 " @companyid," +
                                 " @description" +
                             " )";
                    strSQL += ";select last_insert_rowid()";
                 
                    command.CommandText = strSQL;
                    long last_inserted_id = (long)command.ExecuteScalar();
                }
            } //foreach

            return;
        }



        public static SM_ClientModel GetSendMessageModel(long ClientMsgID)
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select * from msg inner join msgq on msg.clientmsgid=msgq.clientmsgid" +
                          " where msg.clientmsgid=@clientmsgid";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {
                    String _scheduleddate = "";
                    if (  reader["scheduleddate"].Equals(System.DBNull.Value) )
                    {
                        _scheduleddate = "";
                    } else
                    {
                        _scheduleddate = ((System.DateTime)reader["scheduleddate"]).ToString("yyyy-MM-dd HH:mm:ss");
                    }

                    SM_ClientModel acsm = new SM_ClientModel
                    {
                        Action = "sm",
                        Sm = new sm_Model
                        {
                            AccountID = (int)reader["fromaccountid"],
                            DeviceID = (int)reader["fromdevid"],
                            CompanyID = (int)reader["companyid"],
                            MsgUUID = (string)reader["msguuid"],
                            Body = StringCipher.Decrypt((string)reader["body"],Globals.EncryptionPassword),
                            ToAccountID = (int)reader["toaccountid"],
                            ToAccountGroupID = (int)reader["accountgroupid"],
                            ScheduledDate = _scheduleddate,
                            Alert = (int)reader["alert"],
                            Destroy = (int)reader["destroy"],
                            MessageType= (int)reader["msgtype"],
                            QuoteMsgUUID = (string)reader["quotemsguuid"]
                        }
                    };

                    return acsm;
                }
            }

            return null;
        }

        //handle the response of an new message sent by client
        public static SM_ServerModel HandleSendMessage(Object smsobject)
        {
            SM_ServerModel sm = JsonSerializer.Deserialize<SM_ServerModel>(smsobject.ToString());
            //Console.WriteLine(hello.CompanyDescription);

            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"update msg set 
                       msgdate=@msgdate
                       WHERE msguuid=@msguuid";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@msgdate", sm.MessageDate);
            command.Parameters.AddWithValue("@msguuid", sm.MsgUUID);
            command.CommandText = strSQL;
            int rowsupdated = command.ExecuteNonQuery();

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: sm.MsgUUID);

            strSQL = @"update msgq set 
                       status=@status, delivereddate=@msgdate
                       WHERE clientmsgid=@clientmsgid and
                       fromaccountid=@fromaccountid and 
                       fromdevid=@fromdevid";

            command.Parameters.Clear();
            command.Parameters.AddWithValue("@msgdate", sm.MessageDate);
            command.Parameters.AddWithValue("@fromaccountid", sm.AccountID);
            command.Parameters.AddWithValue("@fromdevid", sm.DeviceID);
            command.Parameters.AddWithValue("@status", 3); //3=server get the msg
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.CommandText = strSQL;

            int rowsupdatedq = command.ExecuteNonQuery();

            return sm;
        }

        //handle a new message from SERVER to CLIENT
        public static RU_ClientModel HandleReceiveMessage(Object rmsobject)
        {
            RM_ServerModel rm = JsonSerializer.Deserialize<RM_ServerModel>(rmsobject.ToString());
            Server _server = DatabaseCommon.GetServer(AccountID:  rm.ToAccountID);
            if( _server==null)
            {
                return null;
            }
            DateTime ReceivedDate = DateTime.UtcNow;

            DateTime? datScheduleddate = null;

            if (rm.ScheduledDate != "") {
                try
                {
                    datScheduleddate = DateTime.ParseExact(rm.ScheduledDate, "yyyy-MM-dd HH:mm:ss", null);
                } catch
                {

                }
            }

            long lngClientMsgID = DatabaseMessages.SaveMsg(
                               _server.ServerID, 
                               rm.MsgUUID,
                               rm.CompanyID, 
                               rm.FromAccountID, rm.FromDeviceID,
                               rm.ToAccountID, rm.ToDeviceID, 
                               rm.AccountGroupID,
                               rm.Body,
                               rm.Alert, rm.Destroy,
                               datScheduleddate,
                               DateTime.ParseExact(rm.MsgDate, "yyyy-MM-dd HH:mm:ss", null),
                               ReceivedDate,
                               rm.MessageType,
                               rm.QuoteMsgUUID, 5);



            RU_ClientModel acru = new RU_ClientModel
            {
                Action = "ru",
                ClientMsgID = lngClientMsgID,
                Ru = new ru_model
                {
                    AccountID = rm.ToAccountID,
                    DeviceID = rm.ToDeviceID,
                    CompanyID = rm.CompanyID,
                    MsgUUID = rm.MsgUUID,
                    ReceivedDate = ReceivedDate.ToString("yyyy-MM-dd HH:mm:ss")
                }
            };

            if (lngClientMsgID == 0)
            {
                acru.Action = "ru-duplicate"; //hack to send specific event to UI
                return acru;
            }


            if (rm.MessageType==1) //files
            {
                try
                {
                    FileHeaderModel fh = JsonSerializer.Deserialize<FileHeaderModel>(rm.Body);
                    string strExtension = System.IO.Path.GetExtension(fh.Filename);
                    string storageFolder = string.Format("{0}\\storage\\{1}", Globals.DataPath, _server.ServerID);
                    string previewFileName = fh.Hash + ".p" + strExtension;
                    string compressedFilename = fh.Hash + strExtension;
                    string previewFullFilename = string.Format("{0}\\{1}", storageFolder, previewFileName);
                    string compressedFullFilename = string.Format("{0}\\{1}", storageFolder, compressedFilename);

                    if (!System.IO.File.Exists(previewFullFilename) || !System.IO.File.Exists(compressedFullFilename))
                    {
                        //start downloading parts of the file
                        //set lastsegment to zero to track downloaded files
                        //and mark message as pending filestatus=0
                        fh.LastSegment = 0;

                        SQLiteCommand command;
                        string strSQL;

                        command = new SQLiteCommand
                        {
                            Connection = Globals.Connection
                        };

                        strSQL = @"update msg set filestatus=0,body=@body 
                                   where clientmsgid=@clientmsgid";

                        command.Parameters.AddWithValue("@body", StringCipher.Encrypt(JsonSerializer.Serialize(fh),Globals.EncryptionPassword));
                        command.Parameters.AddWithValue("@clientmsgid", lngClientMsgID);
                        command.CommandText = strSQL;
                        int intRowsUpdated = command.ExecuteNonQuery();

                        IG_ClientModel acig = new IG_ClientModel
                        {
                            Action = "ig",
                            Ig = new ig_model
                            {
                                AccountID = rm.ToAccountID,
                                DeviceID = rm.ToDeviceID,
                                CompanyID = rm.CompanyID,
                                MsgUUID = rm.MsgUUID,
                                Hash = fh.Hash,
                                TotalSegments = fh.TotalSegments,
                                Segment = 1,
                            }
                        };
                        Globals.AwsSocketClient.SendMessage(acig);
                    }
                }
                catch
                {

                }

            }

            if (rm.MessageType == 3) //friend actions
            {
                FR_ClientModel acfr = Api.GetFriends(_server.AccountID);
                Globals.AwsSocketClient.SendMessage(acfr);

                acru.Action = "ru-friends"; //hack to send specific event to UI
            }

            Account _account = DatabaseCommon.GetAccount(rm.FromAccountID);
            if(_account==null)
            { 
                FS_ClientModel acfs = new FS_ClientModel
                {
                    Action = "fs",
                    Fs = new fs_model
                    {
                        AccountID = _server.AccountID,
                        DeviceID = _server.DeviceID,
                        CompanyID = _server.CompanyID,
                        SearchName = "",
                        SearchAccountID = rm.FromAccountID
                    }
                };

                Globals.AwsSocketClient.SendMessage(acfs);
            }

            return acru;
        }

        private static HE_ClientModel GetReceivedUpdate(string MsgUUID)
        {

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = "select receiveddate from msg where msguuid=@msguuid ";
            command.Parameters.AddWithValue("@msguuid", MsgUUID);
            command.CommandText = strSQL;

            using (SQLiteDataReader reader = command.ExecuteReader())
            {
                if (reader.Read()) // Don't assume we have any rows.
                {

                    HE_ClientModel ach = new HE_ClientModel
                    {
                        Action = "he",
                        Hello = new he_Model
                        {
                            AccountID = (int)reader["accountid"],
                            DeviceID = (int)reader["deviceid"],
                            CompanyID = (int)reader["companyid"],
                            Cguid = Globals.ClientGuid,
                            Os = Globals.getOSInfo(),
                            Hw = Globals.getCpuID(),
                            Serial = Globals.getBoardSerialNumber(),
                            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString()
                        }
                    };

                    return ach;
                }
            }

            return null;
        }

        public static SM_ServerModel Handle_Server_RU(Object ruobject)
        {   //set status = 7
            //Client received the answer from server[RU] 
            //that server is being informed about that client get the message.

            RU_ServerModel ru = JsonSerializer.Deserialize<RU_ServerModel>(ruobject.ToString());

            //update status
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: ru.MsgUUID);
            if (_msg == null)
            {
                return null;
            }

            SM_ServerModel smru = null;

            //if message received is from the same user on other device
            //hack to display message as send from this device
            if (_msg.Header.FromAccountID == _msg.Header.ToAccountID)
            {
                strSQL = "update msgq set status=3 where " +
                                      " clientmsgid=@clientmsgid and " +
                                      " isheader=1 ";
                smru = new SM_ServerModel
                {
                    AccountID = ru.AccountID,
                    DeviceID = ru.DeviceID,
                    CompanyID = ru.CompanyID,
                    MsgUUID = ru.MsgUUID,
                    MessageDate = ""
                };
            }
            else
            {
                strSQL = "update msgq set status=7 where " +
                                          " clientmsgid=@clientmsgid and " +
                                          " isheader=1 ";
            }

            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.CommandText = strSQL;

            int intRowsUpdated = command.ExecuteNonQuery();

            return smru;
        }

        public static DU_ClientModel Handle_Server_UU(Object uuobject)
        {
            //set status = 35
            //A read update [UU] just received from server.

            UU_ServerModel uu = JsonSerializer.Deserialize<UU_ServerModel>(uuobject.ToString());
            //update status
            Server _server = DatabaseCommon.GetServer(AccountID: uu.ToAccountID);
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: uu.MsgUUID);
            if(_msg == null)
            {   //message not found BUT you have to inform server to stop sending UU msgs
                return new DU_ClientModel
                {
                    Action = "du",
                    Du = new du_model
                    {
                        AccountID = uu.ToAccountID,
                        DeviceID = uu.ToDeviceID,
                        CompanyID = uu.CompanyID,
                        MsgUUID = uu.MsgUUID,
                        FromAccountID = uu.FromAccountID,
                        FromDeviceID = uu.FromDeviceID
                    }
                };
              
            }

            strSQL = "update msg set qstatus=1 where " +
                     " clientmsgid=@clientmsgid";
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.CommandText = strSQL;
            int intRowsUpdated = command.ExecuteNonQuery();

            //check if already received the read update----------------------
            strSQL = "update msgq set readdate=@readdate, status=35 where " +
                                      " clientmsgid=@clientmsgid and " +
                                      " fromaccountid=@fromaccountid and " +
                                      " fromdevid=@fromdevid and " +
                                      " isheader=0 ";

            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.Parameters.AddWithValue("@readdate", uu.ReadDate);
            command.Parameters.AddWithValue("@fromaccountid", uu.FromAccountID);
            command.Parameters.AddWithValue("@fromdevid", uu.FromDeviceID);
            command.CommandText = strSQL;

            int intRowsUpdatedq = command.ExecuteNonQuery();
            if( intRowsUpdatedq==0)
            {
                strSQL = "insert into msgq (  " +
                          " serverid," +
                          " companyid," +
                          " clientmsgid," +
                          " fromaccountid," +
                          " fromdevid," +
                          " toaccountid," +
                          " todevid," +
                          " accountgroupid," +
                          " delivereddate," +
                          " readdate," +
                          " status," +
                          " isheader" +
                      " ) values ( " +
                          " @serverid," +
                          " @companyid," +
                          " @clientmsgid," +
                          " @fromaccountid," +
                          " @fromdevid," +
                          " @toaccountid," +
                          " @todevid," +
                          " @accountgroupid," +
                          " @delivereddate," +
                          " @readdate," +
                          " @status," +
                          " @isheader" +
                      " )";
                strSQL += ";select last_insert_rowid()";
                command.CommandText = strSQL;
                
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@serverid", _server.ServerID);
                command.Parameters.AddWithValue("@companyid", _msg.CompanyID);
                command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
                command.Parameters.AddWithValue("@fromaccountid", uu.FromAccountID);
                command.Parameters.AddWithValue("@fromdevid", uu.FromDeviceID);
                command.Parameters.AddWithValue("@toaccountid", uu.ToAccountID);
                command.Parameters.AddWithValue("@todevid", uu.ToDeviceID);
                command.Parameters.AddWithValue("@accountgroupid", uu.AccountGroupID);
                command.Parameters.AddWithValue("@delivereddate", String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.UtcNow));
                command.Parameters.AddWithValue("@readdate", uu.ReadDate);
                command.Parameters.AddWithValue("@status", 35);
                command.Parameters.AddWithValue("@isheader", 0);
                //int intRowsInserted = command.ExecuteNonQuery();
                long last_inserted_id = (long)command.ExecuteScalar();
            }

            DU_ClientModel du = new DU_ClientModel
            {
                Action = "du",
                Du = new du_model
                {
                    AccountID = uu.ToAccountID,
                    DeviceID = uu.ToDeviceID,
                    CompanyID = uu.CompanyID,
                    MsgUUID = uu.MsgUUID,
                    FromAccountID = uu.FromAccountID,
                    FromDeviceID = uu.FromDeviceID
                }
            };

            return du;
        }

        public static SU_ServerModel Handle_Server_SU(Object suobject)
        {   //set status = 23
            //Client received the answer from server[SU] 
            //that server get the read update.

            SU_ServerModel ru = JsonSerializer.Deserialize<SU_ServerModel>(suobject.ToString());
            //update status

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: ru.MsgUUID);

            strSQL = "update msgq set status = 23" +
                     " where isheader = 0 " +
                     " and clientmsgid = @clientmsgid" +
                     " and toaccountid = @toaccountid" +
                     " and todevid = @todevid";
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.Parameters.AddWithValue("@toaccountid", _msg.Header.ToAccountID);
            command.Parameters.AddWithValue("@todevid", _msg.Header.ToDevID);
            command.CommandText = strSQL;
            int rowsUpdated = command.ExecuteNonQuery();

            if (rowsUpdated == 0)
            {
                return null;
            }

            return ru;
        }

        public static void Handle_Server_DU(Object duobject)
        {   //set status = 37
            //Client received the answer from server[DU] 
            //that server acknowledge that get the read update.

            DU_ServerModel du = JsonSerializer.Deserialize<DU_ServerModel>(duobject.ToString());
            //update status

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: du.MsgUUID);
            if(_msg==null)
            {
                return;
            }

            strSQL = "update msgq set status = 37" +
                     " where clientmsgid = @clientmsgid and " +
                     " toaccountid=@toaccountid and todevid=@todevid";
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.Parameters.AddWithValue("@toaccountid", du.AccountID);
            command.Parameters.AddWithValue("@todevid", du.DeviceID);
            command.CommandText = strSQL;
            int rowsUpdated = command.ExecuteNonQuery();

            return;
        }

        public static void Handle_Server_AU(Object auobject)
        {  
            AU_ServerModel aum = JsonSerializer.Deserialize<AU_ServerModel>(auobject.ToString());

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"update server set 
                           firstname=@firstname,
                           lastname=@lastname,
                           password=@password,
                           email=@email
                        where accountid = @accountid";

            command.Parameters.AddWithValue("@firstname", aum.Firstname);
            command.Parameters.AddWithValue("@lastname", aum.Lastname);
            command.Parameters.AddWithValue("@password", aum.Password);
            command.Parameters.AddWithValue("@email", aum.Email);
            command.Parameters.AddWithValue("@accountid", aum.AccountID);
            command.CommandText = strSQL;
            int rowsUpdated = command.ExecuteNonQuery();

            return;
        }


        public static SU_ClientModel ReadUpdateMessage(Msg msg)
        {
            Msgq _msgq = DatabaseMessages.GetMsgq(msg.ClientMsgID,
                                                  msg.Header.ToAccountID,
                                                  msg.Header.ToDevID,
                                                  msg.Header.FromAccountID,
                                                  msg.Header.FromDevID);
            if(_msgq!=null)
            {
                return null;
            }

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };


            //update msg status
            strSQL = "update msg set qstatus=1 where clientmsgid=@clientmsgid";
            command.Parameters.AddWithValue("@clientmsgid", msg.ClientMsgID);
            command.CommandText = strSQL;
            int intRowsUpdated = command.ExecuteNonQuery();


            DateTime readdate = DateTime.UtcNow;
          
            strSQL = "insert into msgq (  " +
                    " serverid," +
                    " companyid," +
                    " clientmsgid," +
                    " fromaccountid," +
                    " fromdevid," +
                    " toaccountid," +
                    " todevid," +
                    " accountgroupid," +
                    " delivereddate," +
                    " readdate," +
                    " status," +
                    " isheader" +
                " ) values ( " +
                    " @serverid," +
                    " @companyid," +
                    " @clientmsgid," +
                    " @fromaccountid," +
                    " @fromdevid," +
                    " @toaccountid," +
                    " @todevid," +
                    " @accountgroupid," +
                    " @delivereddate," +
                    " @readdate," +
                    " @status," +
                    " @isheader" +
                " )";
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@serverid", msg.ServerID);
            command.Parameters.AddWithValue("@companyid", msg.CompanyID);
            command.Parameters.AddWithValue("@clientmsgid", msg.ClientMsgID);
            command.Parameters.AddWithValue("@fromaccountid", msg.Header.ToAccountID);
            command.Parameters.AddWithValue("@fromdevid", msg.Header.ToDevID);
            command.Parameters.AddWithValue("@toaccountid", msg.Header.FromAccountID);
            command.Parameters.AddWithValue("@todevid", msg.Header.FromDevID);
            command.Parameters.AddWithValue("@accountgroupid", msg.Header.AccountGroupID);
            command.Parameters.AddWithValue("@delivereddate", DBNull.Value);
            command.Parameters.AddWithValue("@readdate", String.Format("{0:yyyy-MM-dd HH:mm:ss}", readdate));
            command.Parameters.AddWithValue("@status", 5);
            command.Parameters.AddWithValue("@isheader", 0);

            command.CommandText = strSQL;
            command.ExecuteNonQuery();
            
            Server ru = DatabaseCommon.GetServer(CompanyID: Globals.CurrentCompanyID);
            int intAccountID = ru.AccountID;
            int intDeviceID = ru.DeviceID;
            int intCompanyID = ru.CompanyID;

            SU_ClientModel su = new SU_ClientModel
            {
                Action = "su",
                Su = new su_model
                {
                    AccountID = intAccountID,
                    DeviceID = intDeviceID,
                    CompanyID = intCompanyID,
                    MsgUUID = msg.MsgUUID,
                    ReadDate = readdate.ToString("yyyy-MM-dd HH:mm:ss")
                }
            };

            return su;
        }


        public static IS_ClientModel Handle_Server_IC(Object icobject)
        {
            //Client received the answer from server[IC] 
            //that server has/or not the file.

            IC_ServerModel ic = JsonSerializer.Deserialize<IC_ServerModel>(icobject.ToString());
            
            if(ic.TotalSegments==0) //file not exists - start upload
            {
                Server _server = DatabaseCommon.GetServer(CompanyID: Globals.CurrentCompanyID);
                string strExtension = System.IO.Path.GetExtension(ic.Filename);
                string storageFolder = string.Format("{0}\\storage\\{1}", Globals.DataPath, _server.ServerID);
                string _fullfilename = string.Format("{0}\\{1}", storageFolder, ic.Hash + strExtension);
                if (!System.IO.File.Exists(_fullfilename))
                {
                    //do something
                }

                FileInfo inf = new FileInfo(_fullfilename);

                
                int t = (int)Math.Ceiling((double)inf.Length / 1024 / _server.FileSegment);

                IS_ServerModel iss = new IS_ServerModel
                {
                    AccountID = ic.AccountID,
                    DeviceID = ic.DeviceID,
                    CompanyID = ic.CompanyID,
                    MsgUUID = ic.MsgUUID,
                    ToAccountID = ic.ToAccountID,
                    ToAccountGroupID = ic.ToAccountGroupID,
                    TotalSegments = t,
                    Segment = 0,
                    Hash = ic.Hash,
                    Filename = ic.Filename,
                    FileSize = ic.FileSize,
                    FileDate = ic.FileDate

                };

                IS_ClientModel ism = SendPartOfFile(iss, _fullfilename);
                return ism;

            }

            return null;
            
        }

        public static IS_ClientModel Handle_Server_IS(Object isobject)
        {
            //Client received the answer from server[IS] 
            //that server get the file part

            IS_ServerModel ic = JsonSerializer.Deserialize<IS_ServerModel>(isobject.ToString());

            //update completed last segment value
            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: ic.MsgUUID);
            FileHeaderModel fh = JsonSerializer.Deserialize<FileHeaderModel>(_msg.Body);
            fh.LastSegment = ic.Segment;

            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"update msg set body=@body where msguuid=@msguuid";
            //command.CommandType = CommandType.Text;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@body", StringCipher.Encrypt(JsonSerializer.Serialize(fh),Globals.EncryptionPassword) );
            command.Parameters.AddWithValue("@msguuid", _msg.MsgUUID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();



            string strExtension = System.IO.Path.GetExtension(ic.Filename);
            string storageFolder = string.Format("{0}\\storage\\{1}", Globals.DataPath, _msg.ServerID);
            string _fullfilename = string.Format("{0}\\{1}", storageFolder, ic.Hash + strExtension);
            FileInfo inf = new FileInfo(_fullfilename);

            //Server _server = DatabaseCommon.GetServer(CompanyID: Globals.CurrentCompanyID);

            //int t = (int)(inf.Length / 1024 / _server.FileSegment);
       
            if (ic.Segment < ic.TotalSegments)
            {
                if (!System.IO.File.Exists(_fullfilename))
                {
                    //do something
                }

                IS_ServerModel iss = new IS_ServerModel
                {
                    AccountID = ic.AccountID,
                    DeviceID = ic.DeviceID,
                    CompanyID = ic.CompanyID,
                    MsgUUID = ic.MsgUUID,
                    ToAccountID = ic.ToAccountID,
                    ToAccountGroupID = ic.ToAccountGroupID,
                    TotalSegments = ic.TotalSegments,
                    Segment = ic.Segment,
                    Hash = ic.Hash,
                    Filename = ic.Filename,
                    FileSize = ic.FileSize,
                    FileDate = ic.FileDate

                };

                IS_ClientModel ism = SendPartOfFile(iss, _fullfilename);
                return ism;

            }

            return null;

        }


        public static IG_ClientModel Handle_Server_IG(Object igobject)
        {
            //Client received a part of a file from server[IG] 
            //that server get the file part
            IG_ServerModel ig = JsonSerializer.Deserialize<IG_ServerModel>(igobject.ToString());

            //update completed last segment value
            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: ig.MsgUUID);

            FileHeaderModel fh = JsonSerializer.Deserialize<FileHeaderModel>(_msg.Body);
            fh.LastSegment = ig.Segment;

            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"update msg set body=@body where msguuid=@msguuid";
            //command.CommandType = CommandType.Text;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@body", StringCipher.Encrypt(JsonSerializer.Serialize(fh),Globals.EncryptionPassword));
            command.Parameters.AddWithValue("@msguuid", _msg.MsgUUID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();
            
            String fileExtension = Path.GetExtension(fh.Filename); //includes dot
            string storageFolder = string.Format("{0}\\storage\\{1}", Globals.DataPath, _msg.ServerID);
            string partfilename = string.Format("{0}.{1}", ig.Hash,ig.Segment);
            string partfullfilename = string.Format("{0}\\{1}", storageFolder, partfilename);

            string fullfilename = string.Format("{0}\\{1}{2}", storageFolder, ig.Hash, fileExtension);

            if (!System.IO.Directory.Exists(storageFolder))
            {
                System.IO.Directory.CreateDirectory(storageFolder);
            }

            //System.IO.File.AppendAllText(fullfilename + "." + ig.Segment,ig.PartOfFile);

            //append part to file
            byte[] part = System.Convert.FromBase64String(ig.PartOfFile);
            using (var stream = new FileStream(fullfilename, FileMode.Append))
            {
                stream.Write(part, 0, part.Length);
            }

            if(ig.PartOfFile=="")
            {
                Console.WriteLine("part not found sleep 3 seconds");

                //sleep 3 seconds
                Thread.Sleep(3000);
                Console.WriteLine("try to get part " + ig.Segment.ToString());
                //try again to get the file
                IG_ClientModel igc = new IG_ClientModel
                {
                    Action = "ig",
                    Ig = new ig_model
                    {
                        AccountID = ig.AccountID,
                        DeviceID = ig.DeviceID,
                        CompanyID = ig.CompanyID,
                        MsgUUID = ig.MsgUUID,
                        TotalSegments = ig.TotalSegments,
                        Segment = ig.Segment,
                        Hash = ig.Hash
                    }
                };
                return igc;
            }

            if ( ig.Segment < ig.TotalSegments ) //more parts needed
            {
                IG_ClientModel igc = new IG_ClientModel
                {
                    Action = "ig",
                    Ig = new ig_model
                    {
                        AccountID = ig.AccountID,
                        DeviceID = ig.DeviceID,
                        CompanyID = ig.CompanyID,
                        MsgUUID = ig.MsgUUID,
                        TotalSegments = ig.TotalSegments,
                        Segment = ig.Segment+1,
                        Hash = ig.Hash
                    }
                };
                return igc;

            } else //download finished
            {
                //update msg that download is completed
                strSQL = @"update msg set filestatus=1 where msguuid=@msguuid";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@msguuid", _msg.MsgUUID);
                command.CommandText = strSQL;
                command.ExecuteNonQuery();

                if ( fileExtension.ToLower() == ".jpg" || fileExtension.ToLower() == ".jpeg" || fileExtension.ToLower() == ".png")
                {
                    string previewFileName = ig.Hash + ".p" + fileExtension.ToLower();
                    string previewFullFilename = string.Format("{0}\\{1}", storageFolder, previewFileName);
                    //create a preview image file
                    ImageHandlers.CreatePreviewImage(fullfilename, previewFullFilename);
                }

                //hack to inform UI that download completed
                IG_ClientModel igc = new IG_ClientModel
                {
                    Action = "ig-done",
                    Ig = new ig_model
                    {
                        AccountID = ig.AccountID,
                        DeviceID = ig.DeviceID,
                        CompanyID = ig.CompanyID,
                        MsgUUID = ig.MsgUUID,
                        TotalSegments = ig.TotalSegments,
                        Segment = ig.Segment,
                        Hash = ig.Hash
                    }
                };
                return igc;
            }
            
        }


        private static IS_ClientModel SendPartOfFile(IS_ServerModel iss, string FullFilename)
        {
            Server _server = DatabaseCommon.GetServer(CompanyID: iss.CompanyID);
            int bufferSize = _server.FileSegment * 1024;
            int start = iss.Segment * bufferSize ;

            string strTempFileName = System.IO.Path.GetTempFileName();
            System.IO.File.Copy(FullFilename, strTempFileName, true);
            FileInfo filtemp = new System.IO.FileInfo(strTempFileName);

            if(filtemp.Length < start+bufferSize)
            {
                bufferSize = Convert.ToInt32(filtemp.Length) - start;
            }
            byte[] part = new byte[bufferSize];

            

            using (BinaryReader reader = new BinaryReader(new FileStream(strTempFileName, FileMode.Open)))
            {
                reader.BaseStream.Seek(start, SeekOrigin.Begin);
                reader.Read(part, 0, bufferSize);
            }
            System.IO.File.Delete(strTempFileName);

            int _segment = iss.Segment + 1;

            IS_ClientModel ism = new IS_ClientModel
            {
                Action = "is",
                Ism = new is_model
                {
                    AccountID = iss.AccountID,
                    DeviceID = iss.DeviceID,
                    CompanyID = iss.CompanyID,
                    MsgUUID = iss.MsgUUID,
                    ToAccountID = iss.ToAccountID,
                    ToAccountGroupID = iss.ToAccountGroupID,
                    TotalSegments = iss.TotalSegments,
                    Segment = _segment,
                    Hash = iss.Hash,
                    Filename = iss.Filename,
                    FileSize = iss.FileSize,
                    FileDate = iss.FileDate,
                    PartOfFile = Convert.ToBase64String(part)
                }
            };

            
            return ism;
        }

    }
}