﻿using partigiano_windows_app_wpf.Model;
using partigiano_windows_app_wpf.SocketModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Security;
using System.Windows;
using Websocket.Client;

namespace partigiano_windows_app_wpf.Classes
{
   
    class AWSSocket
    {
        public event EventHandler Connected;
        public event EventHandler Disconnected;
        public event EventHandler MessageReceived;
        public event EventHandler MessageReceivedDuplicate;
        public event EventHandler ReadUpdateReceived;
        public event EventHandler ImageReceived;
        public event EventHandler MessageDelivered;
        public event EventHandler FriendsReceived;
        public event EventHandler FriendsSearchReceived;
        public event EventHandler FriendshipReceived;
        public event EventHandler GroupsReceived;

        WebsocketClient client;
        public void Start()
        {
            var exitEvent = new ManualResetEvent(false);
            //var url = new Uri(string.Format("wss://m0rbnty9eg.execute-api.eu-central-1.amazonaws.com/production?c={0}", Get_OnConnect_Params()));
            var url = new Uri(string.Format("wss://app.waavia7.com/?c={0}", Get_OnConnect_Params()));

            //using (var client = new WebsocketClient(url))
            using (client = new WebsocketClient(url))
            {
                
                
                client.ReconnectTimeout = TimeSpan.FromSeconds(2880); //2 hours default
                client.DisconnectionHappened.Subscribe(type =>
                {
#if DEBUG
                    System.IO.File.AppendAllText(Globals.DataPath + @"\\log.txt", DateTime.UtcNow.ToString() + " DisconnectionHappened" + Environment.NewLine);
#endif
                    EventHandler handlerMessageReceived = Disconnected;
                    handlerMessageReceived?.Invoke(null, EventArgs.Empty);
                    Globals.IsOnline = false;
                    Console.WriteLine($"Disconnected happened, type: {type}, url: {client.Url}");
                    client.ReconnectTimeout = TimeSpan.FromSeconds(60);
                });

                client.ReconnectionHappened.Subscribe(type =>
                {
#if DEBUG
                    App.Current.Dispatcher.Invoke(() =>
                    {
                        System.IO.File.AppendAllText(Globals.DataPath + @"\\log.txt", DateTime.UtcNow.ToString() + " ReconnectionHappened" + Environment.NewLine);
                    });
#endif
                    EventHandler handlerMessageReceived = Connected;
                    handlerMessageReceived?.Invoke(null, EventArgs.Empty);

                    Console.WriteLine($"Reconnection happened, type: {type}, url: {client.Url}");
                    SendHelloModels();
                    Globals.IsOnline = true;
                    client.ReconnectTimeout = TimeSpan.FromSeconds(2880); //2 hours default

                });

                client.MessageReceived.Subscribe(msg =>
                {
                    Globals.IsOnline = true;
                    Console.WriteLine($"Message received: {msg}");
#if DEBUG
                    App.Current.Dispatcher.Invoke(() =>
                    {
                        System.IO.File.AppendAllText(Globals.DataPath + @"\\log.txt", DateTime.UtcNow.ToString() + " " + msg.ToString() + Environment.NewLine);
                    });
#endif
                    //MessageBox.Show(msg.ToString());

                    // messages from SERVER to CLIENT
                    var data = JsonSerializer.Deserialize<ResponseModel>(msg.ToString());
                    switch( data.Action )
                    {
                        case "he":
                            int AccountID = Api.HandleHello(data.Response);
                            SendMessage(Api.GetFriends(AccountID));
                            SendMessage(Api.GetSitesAndGroups(AccountID));
                            SendMessage(Api.GetSitesRoles(AccountID));
                            break;

                        case "fr": //receive friends list from server
                            Api.HandleFriends(data.Response);
                            EventHandler handlerFriendsReceived = FriendsReceived;
                            handlerFriendsReceived?.Invoke(null, EventArgs.Empty);
                            break;

                        case "fs": //receive friends list from server after a search request
                            Api.HandleSearchFriends(data.Response);
                            EventHandler handlerSearchFriendsReceived = FriendsSearchReceived;
                            handlerSearchFriendsReceived?.Invoke(null, EventArgs.Empty);
                            break;

                        case "sg": //receive sites and groups from server
                            Api.HandleSitesAndGroups(data.Response);
                            EventHandler handlerGroupsReceived = GroupsReceived;
                            handlerGroupsReceived?.Invoke(null, EventArgs.Empty);
                            break;

                        case "sr": //receive siterols from server
                            Api.HandleSitesRoles(data.Response);
                            break;

                        case "au": //receive auth data
                            Api.Handle_Server_AU(data.Response);
                            break;

                        case "sm": //receive a response for a send message from server
                            SM_ServerModel smserver = Api.HandleSendMessage(data.Response);

                            EventHandler handlerMessageDelivered = MessageDelivered;
                            handlerMessageDelivered?.Invoke(smserver, EventArgs.Empty);
                            break;

                        case "rm": //receive a new message from server
                            RU_ClientModel ruclient = Api.HandleReceiveMessage(data.Response);
                            if (ruclient.Action == "ru")
                            {
                                EventHandler handlerMessageReceived = MessageReceived;
                                handlerMessageReceived?.Invoke(ruclient, EventArgs.Empty);

                                SendMessage(ruclient);
                            } else if(ruclient.Action == "ru-duplicate")
                            {
                                EventHandler handlerMessageReceivedDuplicate = MessageReceivedDuplicate;
                                handlerMessageReceivedDuplicate?.Invoke(ruclient, EventArgs.Empty);
                                ruclient.Action = "ru"; // fix action
                                SendMessage(ruclient);
                            } else if (ruclient.Action == "ru-friends")
                            {
                                EventHandler handlerFriendshipReceived = FriendshipReceived;
                                handlerFriendshipReceived?.Invoke(ruclient, EventArgs.Empty);
                                ruclient.Action = "ru"; // fix action
                                SendMessage(ruclient);
                            }
                            break;

                        case "ru": //receive the ack that server informed the delivery of a message
                            SM_ServerModel smru = Api.Handle_Server_RU(data.Response);

                            //hack to refresh ui
                            if(smru != null)
                            {
                                EventHandler handlerru = MessageDelivered;
                                handlerru?.Invoke(smru, EventArgs.Empty);
                            }
                            
                            break;

                        case "uu": //receive a read update from server
                            DU_ClientModel duclient = Api.Handle_Server_UU(data.Response);
                            if (duclient != null)
                                SendMessage(duclient);

                            UU_ServerModel uuserver = JsonSerializer.Deserialize<UU_ServerModel>(data.Response.ToString());
                            EventHandler handlerReadUpdateRcvd = ReadUpdateReceived;
                            handlerReadUpdateRcvd?.Invoke(uuserver, EventArgs.Empty);
                            break;

                        case "du": //receive a read update ACK from server
                            Api.Handle_Server_DU(data.Response);
                            break;

                        case "su": //receive the ack that server get the read update
                            SU_ServerModel suserver = Api.Handle_Server_SU(data.Response);
                            break;

                        case "ic": //receive the response of a file check
                            //hack. file exists, so send the header
                            IC_ServerModel ic = JsonSerializer.Deserialize<IC_ServerModel>(data.Response.ToString());
                            if( ic.TotalSegments > 0 ) 
                            {
                                SendPendingHeaderMessage(ic.MsgUUID);
                            }

                            IS_ClientModel isclientc = Api.Handle_Server_IC(data.Response);
                            if (isclientc != null)
                                SendMessage(isclientc);
                            break;

                        case "is": //receive the response that a part of a file is sent
                            //hack. if all parts send, then send the header
                            IS_ServerModel isserver = JsonSerializer.Deserialize<IS_ServerModel>(data.Response.ToString());
                            if (isserver.Segment == isserver.TotalSegments)
                            {
                                //update msg that download is completed
                                SendPendingHeaderMessage(isserver.MsgUUID);
                            }

                            IS_ClientModel isclient = Api.Handle_Server_IS(data.Response);
                            if (isclient != null)
                                SendMessage(isclient);
                            break;

                        case "ig": //receive the part of a file 
                            IG_ClientModel igclient = Api.Handle_Server_IG(data.Response);
                            if (igclient != null)
                            {
                                if (igclient.Action == "ig-done")
                                {
                                    EventHandler handlerImageReceived = ImageReceived;
                                    handlerImageReceived?.Invoke(igclient, EventArgs.Empty);
                                } else {
                                    SendMessage(igclient);
                                }
                            }
                                
                            break;
                        default:
                            break;
                    }

                  
                });

                client.Start();

                //SendHelloModels();

              
                //Task.Run(() => client.Send("{\"ac\": \"fr\", \"fr\": {\"a\": 44, \"d\": 99, \"r\": 1}}"));
                //Task.Run(() => client.Send("{\"ac\":\"he\",\"he\":{\"a\":44,\"d\":6,\"r\":1,\"c\":\"....\",\"o\":\"....\",\"h\":\"....\",\"s\":\"....\",\"b\":\"....\"}}"));

                exitEvent.WaitOne();
            }
        }

        private void SendHelloModels()
        {
            List<HE_ClientModel> Hellolist = Api.GetHelloModels();
            foreach (var model in Hellolist)
            {
                SendMessage(model);
            }

        }
        private String Get_OnConnect_Params()
        {
            SQLiteCommand  command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand 
            {
                Connection = Globals.Connection
            };

            strSQL = "select username,password,deviceid from Server order by username";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            List<Credentials> Credlist = new List<Credentials>();

            foreach (DataRow row in dt.Rows)
            {
                Credentials c = new Credentials
                {
                    Username = row["username"].ToString(),
                    Password = row["password"].ToString(),
                    DeviceID = Int32.Parse(row["deviceid"].ToString()),
                };

                Credlist.Add(c);

            }

            return JsonSerializer.Serialize(Credlist);
        }

        class Credentials
        {
            [JsonPropertyName("u")]
            public string Username { get; set; }

            [JsonPropertyName("p")]
            public string Password { get; set; }

            [JsonPropertyName("d")]
            public long DeviceID { get; set; }
        }

        public void test()
        {
            Console.WriteLine("test");
            Task.Run(() => client.Send("{\"ac\": \"fr\", \"fr\": {\"a\": 44, \"d\": 99, \"r\": 1}}"));

        }

        public void SendMessage(Object jsonmodel)
        {
#if DEBUG
            System.IO.File.AppendAllText(Globals.DataPath + @"\\log.txt", DateTime.UtcNow.ToString() + " " +JsonSerializer.Serialize(jsonmodel) + Environment.NewLine);
#endif
            Console.WriteLine(JsonSerializer.Serialize(jsonmodel));

            //if (Globals.IsOnline)
            //{
                try
                {
                    Task.Run(() => client.Send(JsonSerializer.Serialize(jsonmodel)));
                }
                catch
                {
                    Console.WriteLine("Socket Send Error");
                    Globals.IsOnline = false;
                }
            //}
            
        }

        public void SendPendingHeaderMessage(string MsgUUID)
        {
            Msg _msg = DatabaseMessages.GetMsg(MsgUUID: MsgUUID);

            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            //update msg that upload has been completed
            strSQL = @"update msg set filestatus=1 where clientmsgid=@clientmsgid";
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //unblock status to send the message
            strSQL = @"update msgq set status=1 where clientmsgid=@clientmsgid and isheader=1 and status=-2";
            //command.CommandType = CommandType.Text;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@clientmsgid", _msg.ClientMsgID);
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //get send message model from api
            SM_ClientModel sm = Api.GetSendMessageModel(_msg.ClientMsgID);

            Globals.AwsSocketClient.SendMessage(sm);

        }
    }
}
