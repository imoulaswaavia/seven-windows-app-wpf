﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace partigiano_windows_app_wpf.Classes
{
    static class Database
    {
        static String strDB = Globals.DataPath + "\\seven.dat";

        public static void Check()
        {
            Globals.Connection = new SQLiteConnection("Data Source=" + strDB);
            
            if (!System.IO.File.Exists(strDB))
            {
                SQLiteConnection.CreateFile(strDB);
                CreateSchema();
            }
            
            Globals.Connection.Open();
        
            UpgradeDatabase();
            
        }

        private static void CreateSchema()
        {
            SQLiteCommand  command;
            string strSQL;
            Globals.Connection.Open();

            command = new SQLiteCommand ();
            command.Connection = Globals.Connection;

            //DB_VERSION----------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE db_version";
            strSQL += "  (dbid int)";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            strSQL = "insert into db_version (dbid) values (2)";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //SITE---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE site";
            strSQL += "  (";
            strSQL += " serverid      int,";
            strSQL += " companyid     int,";
            strSQL += " siteid        int primary key,";
            strSQL += " description   varchar(50),";
            strSQL += " companylevel  int";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //siterole---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE siterole";
            strSQL += "  (";
            strSQL += " serverid      int,";
            strSQL += " companyid     int,";
            strSQL += " siteroleid    int primary key,";
            strSQL += " description   varchar(50)";
            strSQL +=  " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //account---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE account";
            strSQL += "  (";
            strSQL += " serverid     int,";
            strSQL += " companyid    int,";
            strSQL += " accountid    int primary key,";
            strSQL += " siteroleid   int,";
            strSQL += " firstname    varchar(50),";
            strSQL += " lastname     varchar(50),";
            strSQL += " email        varchar(50),";
            strSQL += " active       int,";
            strSQL += " sitelevel    int,"; 
            strSQL += " lang         varchar(2)";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //accountgroup---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE accountgroup";
            strSQL += "  (";
            strSQL += " serverid        int,";
            strSQL += " companyid       int,";
            strSQL += " accountgroupid  int primary key,";
            strSQL += " siteid          int,";
            strSQL += " description     varchar(50),";
            strSQL += " priority        int,";
            strSQL += " color           varchar(50),";
            strSQL += " email           varchar(50)";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            ////accountgroups---------------------------------------------
            //strSQL = "";
            //strSQL += " CREATE TABLE accountgroups";
            //strSQL += "  (";
            //strSQL += " companyid        int,";
            //strSQL += " accountgroupsid  int,";
            //strSQL += " accountgroupid   int primary key,";
            //strSQL += " accountid        int";
            //strSQL += " )";
            //command.CommandText = strSQL;
            //command.ExecuteNonQuery();

            //msg---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE msg";
            strSQL += " (";
            strSQL += " serverid        int,";
            strSQL += " companyid       int,";
            strSQL += " clientmsgid     INTEGER primary key AUTOINCREMENT ,";
            strSQL += " msguuid         varchar(36),";
            strSQL += " body            text,";
            strSQL += " alert           int,";
            strSQL += " msgtype         int,";
            strSQL += " destroy         int,";
            strSQL += " msgdate         datetime,";
            strSQL += " createddate     datetime,"; //this is the receiveddate
            strSQL += " scheduleddate   datetime,";
            strSQL += " quotemsguuid    varchar(36),";
            strSQL += " qstatus         int,";
            strSQL += " filestatus      int";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            strSQL = "CREATE UNIQUE INDEX msguuid_idx ON msg(msguuid);";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //msgq---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE msgq";
            strSQL += " (";
            strSQL += " serverid          int,";
            strSQL += " companyid         int,";
            strSQL += " msgqid            INTEGER primary key AUTOINCREMENT ,";
            strSQL += " clientmsgid       int,";
            strSQL += " fromaccountid     int,";
            strSQL += " fromdevid         int,";
            strSQL += " toaccountid       int,";
            strSQL += " todevid           int,";
            strSQL += " accountgroupid    int,";
            strSQL += " delivereddate     datetime,";
            strSQL += " readdate          datetime,";
            strSQL += " status            int,";
            strSQL += " isheader          int";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //dev---------------------------------------------
            /*strSQL = "";
            strSQL += " CREATE TABLE dev";
            strSQL += "  (";
            strSQL += " Serverid           int,";
            strSQL += " devid             int,";
            strSQL += " accountid         int,";
            strSQL += " lastconnecteddate datetime,";
            strSQL += " status            int,";
            strSQL += " userstatus        int,";
            strSQL += " cguid             string,";
            strSQL += " os                string,";
            strSQL += " mailclient        string";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();*/

            //msgform---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE msgform";
            strSQL += "  (";
            strSQL += " serverid          int,";
            strSQL += " companyid         int,";
            strSQL += " msgformid         int primary key,";
            strSQL += " description       varchar(50),";
            strSQL += " formbody          text,";
            strSQL += " siteroleid        int,";
            strSQL += " recipientemail    varchar(50),";
            strSQL += " active            int";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();

            //Server---------------------------------------------
            strSQL = "";
            strSQL += " CREATE TABLE server";
            strSQL += "  (";
            strSQL += " serverid      INTEGER primary key AUTOINCREMENT ,";
            strSQL += " companyid     int,";
            strSQL += " serverdescr   varchar(50),";
            strSQL += " username      varchar(50),";
            strSQL += " password      varchar(50),";
            strSQL += " accountid     int,";
            strSQL += " deviceid      int,";
            strSQL += " siteroleid    int,";
            strSQL += " firstname     varchar(50),";
            strSQL += " lastname      varchar(50),";
            strSQL += " email         varchar(50),";
            strSQL += " sitelevel     int,";
            strSQL += " namelocked    int,";
            strSQL += " filesegment   int,";
            strSQL += " lang          varchar(2),";
            strSQL += " accountstatus int";
            strSQL += " )";
            command.CommandText = strSQL;
            command.ExecuteNonQuery();
           
            Globals.Connection.Close();
        }

        private static void UpgradeDatabase()
        {
            SQLiteCommand command;
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            }
            ;
            strSQL = "select dbid from db_version";
            command.CommandText = strSQL;

            int intVersion = Convert.ToInt32(command.ExecuteScalar());
            if(intVersion == 1) //upgrade to encryption
            {
                Console.WriteLine("upgrade db");
                strSQL = @"ALTER TABLE account 
                             RENAME COLUMN lang to lang_TBD";
                command.CommandText = strSQL;
                command.ExecuteNonQuery();

                strSQL = @"ALTER TABLE account 
                             ADD COLUMN lang varchar(2)";
                command.CommandText = strSQL;
                command.ExecuteNonQuery();

                strSQL = @"update account SET
                             lang='en'";
                command.CommandText = strSQL;
                command.ExecuteNonQuery();

                strSQL = @"update db_version SET
                             dbid=2";
                command.CommandText = strSQL;
                command.ExecuteNonQuery();
                
                //encrypt old db
                string e = StringCipher.Encrypt("plaintext", "password");
                strSQL = @"select clientmsgid,body from msg";

                command = new SQLiteCommand
                {
                    Connection = Globals.Connection
                };

                SQLiteCommand commandu;
                commandu = new SQLiteCommand
                {
                    Connection = Globals.Connection
                };

                command.CommandText = strSQL;

                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    while(reader.Read()) // Don't assume we have any rows.
                    {
                        strSQL = @"update msg SET body=@body where clientmsgid=@clientmsgid";
                        commandu.CommandText = strSQL;
                        commandu.Parameters.Clear();
                        commandu.Parameters.AddWithValue("@body", StringCipher.Encrypt(reader["body"].ToString(), Globals.EncryptionPassword));
                        commandu.Parameters.AddWithValue("@clientmsgid", (int)(long)reader["clientmsgid"]);
                        commandu.ExecuteNonQuery();
                    }
                }
                intVersion = 1;
            }




        }

        public static bool IsConfigured()
        {
            SQLiteCommand  command;
            string strSQL;

            command = new SQLiteCommand ();
            command.Connection = Globals.Connection;

            strSQL = "select count(*) from Server";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
           
            if ( Convert.ToInt32(command.ExecuteScalar()) > 0)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
