﻿using partigiano_windows_app_wpf.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace partigiano_windows_app_wpf
{
    /// <summary>
    /// Interaction logic for NotificationWindow.xaml
    /// </summary>
    public partial class NotificationWindow : Window
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user clicks a notification")]
        public event EventHandler<object> NotificationClick;


        private readonly Msg _msg;
       
        public NotificationWindow(Msg Message)
        {
            InitializeComponent();

            _msg = Message;

            Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                string strNotif = "";
                switch (_msg.MsgType)
                {
                    case 0:
                        if (_msg.Body.Length > 100)
                        {
                            strNotif = _msg.Body.Substring(0, 100) + "...";
                        }
                        else
                        {
                            strNotif = _msg.Body;
                        }
                        break;
                    case 1:
                        strNotif = "You received a file";
                        break;

                    case 2:
                        strNotif = "A friend reacted";
                        break;
                }

                var workingArea = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
                var transform = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice;
                var corner = transform.Transform(new Point(workingArea.Right, workingArea.Bottom));

                this.Left = corner.X - this.ActualWidth - 150;
                this.Top = corner.Y - this.ActualHeight - 20;
                this.txtHeader.Text = _msg.MsgDate.ToString("yyyy-M-d HH:mm") + Environment.NewLine + _msg.FromNiceAccountName;
                this.txtNotificationMessage.Text = strNotif;

                //autoclose
                System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer.Tick += ProcessTimer_Tick;
                dispatcherTimer.Interval = new TimeSpan(0, 0, 12);
                dispatcherTimer.Start();

            }));


        }
        private void ProcessTimer_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnNotification_Click(object sender, EventArgs e)
        {
            EventHandler<object> handler = NotificationClick;
            handler?.Invoke(null, _msg);
        }
    }
}
