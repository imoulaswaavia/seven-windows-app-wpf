﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using static partigiano_windows_app_wpf.Model.Msg;
using partigiano_windows_app_wpf.Classes;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class MsgqViewModel
    {
    
        public ObservableCollection<Msgq> Msgqs
        {
            get;
            set;
        }

    
        public void LoadMsgqs(long ClientMsgID)
        {
            ObservableCollection<Msgq> _msgqs = new ObservableCollection<Msgq>();
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;

            strSQL = @"select DeliveredDate,ReadDate,msg.clientmsgid,
                       msg.companyid,accountgroupid,
                       fromaccountid,fromdevid,toaccountid,todevid,
                       status,firstname,lastname,isheader,msgdate
                       from msgq 
                       inner join account on msgq.fromaccountid=account.accountid
                       inner join msg on msg.clientmsgid=msgq.clientmsgid
                       Where msgq.clientmsgid=@clientmsgid";
            command.Parameters.AddWithValue("@clientmsgid", ClientMsgID);
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                DateTime? datDeliveredDate = null;
                datDeliveredDate = (row["DeliveredDate"].ToString() == "") ? null : (DateTime?)row["DeliveredDate"];
                DateTime? datReadDate = null;
                datReadDate = (row["ReadDate"].ToString() == "") ? null : (DateTime?)row["ReadDate"];

                //Console.WriteLine("q " + q.FromAccountID);
                _msgqs.Add(new Msgq
                {
                    ClientMsgID = Int32.Parse(row["clientmsgid"].ToString()),
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    AccountGroupID = Int32.Parse(row["accountgroupid"].ToString()),
                    FromAccountID = Int32.Parse(row["fromaccountid"].ToString()),
                    FromDevID = Int32.Parse(row["fromdevid"].ToString()),
                    ToAccountID = Int32.Parse(row["toaccountid"].ToString()),
                    ToDevID = Int32.Parse(row["todevid"].ToString()),
                    DeliveredDate = datDeliveredDate,
                    ReadDate = datReadDate,
                    Status = Int32.Parse(row["status"].ToString()),
                    Firstname = row["firstname"].ToString(),
                    Lastname = row["lastname"].ToString(),
                    IsHeader = Int32.Parse(row["IsHeader"].ToString()),
                    MsgDate = (DateTime)row["MsgDate"]
                });
            }
        
            Msgqs = _msgqs;


        }


    

    }
}