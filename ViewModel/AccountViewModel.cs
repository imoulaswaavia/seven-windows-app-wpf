﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using partigiano_windows_app_wpf.Classes;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class AccountViewModel
    {

        public ObservableCollection<Account> Accounts
        {
            get;
            set;
        }

        public void LoadAccounts(bool WithFriends, String SearchText)
        {
            ObservableCollection<Account> _accounts = new ObservableCollection<Account>();

            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);
            if (_server == null) return;
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;

            strSQL = "select * from account ";
            if (!WithFriends)
            {
                strSQL += " where (active=0 Or active=-1 Or active=-3)";
            } else
            {
                strSQL += " where (active=0 Or active=-1 Or active=-3 Or active=-2)";
            }

            if(SearchText!="")
            {
                strSQL += string.Format(" and ( lastname like '%{0}%' or firstname like '%{0}%' )", SearchText);
            }
            strSQL += " order by lastname, firstname";

            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                _accounts.Add(new Account
                {
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    AccountID = Int32.Parse(row["accountid"].ToString()),
                    Firstname = row["firstname"].ToString(),
                    Lastname = row["lastname"].ToString(),
                    Active = Int16.Parse(row["active"].ToString()),
                    SiteLevel = Int16.Parse(row["sitelevel"].ToString()),
                    Lang = row["lang"].ToString(),
                    UnreadMessages = DatabaseMessages.GetUnreadMsgsFromAccount(Int32.Parse(row["accountid"].ToString()),_server.AccountID)
                }); 
            }


            Accounts = _accounts;
        }


    

    }
}