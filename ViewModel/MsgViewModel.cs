﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using static partigiano_windows_app_wpf.Model.Msg;
using partigiano_windows_app_wpf.Classes;
using System.Web;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class MsgViewModel
    {

        public ObservableCollection<Msg> Msgs
        {
            get;
            set;
        }

        public void LoadMessages(int MessageLimit=30,long ClientMsgID=0,string SearchText="")
        {
            ObservableCollection<Msg> _msgs = new ObservableCollection<Msg>();

            if ( Globals.CurrentGroupID == 0 &&  Globals.CurrentAccountID == 0 ) {
                return;
            }
            
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL="";

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;
            command.Parameters.Clear();
            if ( Globals.CurrentGroupID>0)
            {
                //strSQL = "select msg.companyid, msg.serverid, msg.clientmsgid,msguuid,body,alert,msgtype,destroy,msgdate,scheduleddate, quotemsguuid, account.firstname, account.lastname, msgqid, msgq.fromaccountid, msgq.fromdevid " +
                strSQL = @"SELECT  msgdate,msgq.accountgroupid,ScheduledDate,body,
                        msg.clientmsgid,msg.companyid,msg.serverid,msguuid,alert,
                        msgtype,destroy,quotemsguuid,qstatus,filestatus,
                        firstname,lastname,msgqid,
                        fromaccountid,fromdevid,toaccountid,todevid,status
                         FROM (msg INNER JOIN msgq ON msg.clientmsgid = msgq.clientmsgid) 
                         LEFT JOIN account ON msgq.fromaccountid = account.accountid 
                         WHERE isheader=1 and msgq.accountgroupid=@accountgroupid and  
                         (msgtype=0 or msgtype=2 or msgtype=11 or msgtype=1) ";
                command.Parameters.AddWithValue("@accountgroupid", Globals.CurrentGroupID);
            }
            if (Globals.CurrentAccountID > 0)
            {
                //strSQL = "select msg.companyid, msg.serverid, msg.clientmsgid,msguuid,body,alert,msgtype,destroy,msgdate,scheduleddate, quotemsguuid, account.firstname, account.lastname,msgqid, msgq.fromaccountid, msgq.fromdevid  " +
                strSQL = @"SELECT  msgdate,msgq.accountgroupid,ScheduledDate,body,
                        msg.clientmsgid,msg.companyid,msg.serverid,msguuid,alert,
                        msgtype,destroy,quotemsguuid,qstatus,filestatus,
                        firstname,lastname,msgqid,
                        fromaccountid,fromdevid,toaccountid,todevid,status
                         FROM (msg INNER JOIN msgq ON msg.clientmsgid = msgq.clientmsgid) 
                         LEFT JOIN account ON msgq.fromaccountid = account.accountid 
                         WHERE isheader=1 and msgq.accountgroupid=0 and 
                         (msgq.toaccountid=@accountid or msgq.fromaccountid=@accountid) And 
                         (msgtype=0 or msgtype=2 or msgtype=11 Or msgtype=1)";
                //command.Parameters.AddWithValue("@accountgroupid", Globals.CurrentGroupID);
                command.Parameters.AddWithValue("@accountid", Globals.CurrentAccountID);
            }

            if(ClientMsgID!=0)
            {
                //cannot use clientmsgid for paging because you might receive messages to a previous date

                Msg _msg0 = DatabaseMessages.GetMsg(ClientMsgID: ClientMsgID);
              
                //strSQL += " AND msg.ClientMsgID < @clientmsgdD ";
                //command.Parameters.AddWithValue("@clientmsgdD", ClientMsgID);
                  
                strSQL += " AND msg.msgdate <= @msgdate  ";
                command.Parameters.AddWithValue("@msgdate", _msg0.MsgDate);

                strSQL += " AND msg.ClientMsgID <> @clientmsgdD ";
                command.Parameters.AddWithValue("@clientmsgdD", ClientMsgID);
            }
            strSQL += " order by msgdate desc, msg.clientmsgid desc limit @limit ";
            command.Parameters.AddWithValue("@limit", MessageLimit);

            command.CommandText = strSQL;
            //command.CommandType = CommandType.Text;
            
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);
            if (dt.Rows.Count > 1 )
            {
                if (DateTime.Compare((DateTime)dt.Rows[0]["MsgDate"], (DateTime)dt.Rows[dt.Rows.Count - 1]["MsgDate"]) == 0)
                {
                    LoadMessages(MessageLimit+=30, ClientMsgID, SearchText);
                    return;
                }
            }
            
            DateTime datPreviousDate=DateTime.UtcNow;

            foreach (DataRow row in dt.Rows)
            {
                String strBody = StringCipher.Decrypt(row["body"].ToString(), Globals.EncryptionPassword).ToLower();
                if( SearchText != "" )
                {
                    if( !strBody.Contains(SearchText.ToLower()))
                    {
                        continue;
                    }
                }

                bool blnShowDate = false;
                string strQuoteMsgBody = "";
                Msg _qmsg = null;

                string strQuoteMsgID = row["quotemsguuid"].ToString();

                if (strQuoteMsgID != "" )
                {
                    _qmsg = DatabaseMessages.GetMsg(MsgUUID: strQuoteMsgID);
                    if(_qmsg != null)
                    {
                        strQuoteMsgBody = _qmsg.Body;
                    } else
                    {
                        strQuoteMsgID = ""; //quoted messsage not found
                    }
                }

                DateTime datMsgDate;
                DateTime? datScheduledDate = null;

                datMsgDate = (DateTime)row["MsgDate"];
                datScheduledDate = (row["ScheduledDate"].ToString()=="")?null:(DateTime?)row["ScheduledDate"];
                

                if ( datPreviousDate.Date == datMsgDate.Date )
                {
                    blnShowDate = false;
                } else
                {
                    blnShowDate = true;
                }

               
                int intClientMSgID = Int32.Parse(row["clientmsgid"].ToString());
                _msgs.Insert(0,new Msg
                {
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    ClientMsgID = intClientMSgID,
                    MsgUUID = row["msguuid"].ToString(),
                    Body = strBody,
                    Alert = Int16.Parse(row["alert"].ToString()),
                    MsgType = Int16.Parse(row["msgtype"].ToString()),
                    Destroy = Int16.Parse(row["destroy"].ToString()),
                    MsgDate = datMsgDate,
                    ScheduledDate = datScheduledDate,
                    QuoteMsgUUID = strQuoteMsgID,
                    QStatus = Int16.Parse(row["qstatus"].ToString()),
                    FileStatus = Int16.Parse(row["filestatus"].ToString()),
                    QuoteMsgBody = strQuoteMsgBody,
                    QuoteMsg = _qmsg,
                    ShowDate = blnShowDate,
                    ShownDate = datPreviousDate,

                    Header = new Msgq()
                    {
                        ServerID = Int32.Parse(row["serverid"].ToString()),
                        Firstname = row["firstname"].ToString(),
                        Lastname = row["lastname"].ToString(),
                        ClientMsgID = Int32.Parse(row["clientmsgid"].ToString()),
                        MsgqID = Int32.Parse(row["msgqid"].ToString()),
                        FromAccountID = (int)row["fromaccountid"],
                        FromDevID = (int)row["fromdevid"],
                        ToAccountID = (int)row["toaccountid"],
                        ToDevID = (int)row["todevid"],
                        AccountGroupID = (int)row["accountgroupid"],
                        DeliveredDate = null,
                        ReadDate = null,
                        Status = (int)row["status"],
                        IsHeader = 1
                    }
                    //MessageQueue = DatabaseMessages.GetMsgq(intClientMSgID)
                });

                datPreviousDate = datMsgDate;
            }

            Msgs = _msgs;
        }
    }
}