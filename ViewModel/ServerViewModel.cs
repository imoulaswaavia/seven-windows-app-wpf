﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class ServerViewModel
    {

        public ObservableCollection<Server> Servers
        {
            get;
            set;
        }

        public void LoadServers()
        {
            ObservableCollection<Server> _Servers = new ObservableCollection<Server>();

            SQLiteCommand  command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand ();
            command.Connection = Globals.Connection;

            strSQL = "select * from Server order by username";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            foreach(DataRow row in dt.Rows)
            {
                _Servers.Add(new Server {
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    Firstname = row["Firstname"].ToString(),
                    Lastname = row["Lastname"].ToString(),
                    ServerDescription= row["serverdescr"].ToString(),
                });
            }

            //_accounts.Add(new Account { ServerID = 2, Firstname = "Allain" });

            Servers = _Servers;
        }
    }
}