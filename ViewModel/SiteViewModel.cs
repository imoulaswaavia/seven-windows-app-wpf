﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using partigiano_windows_app_wpf.Classes;

namespace partigiano_windows_app_wpf.ViewModel
{
    
    public class SiteViewModel
    {


        //public ObservableCollection<Site> Sites
        //{
        //    get;
        //    set;
        //}

        public ObservableCollection<Site> Sites { get; set; } = new ObservableCollection<Site>();

        //private ObservableCollection<Site> _sites = new ObservableCollection<Site>();

        //public ObservableCollection<Site> Sites 
        //{
        //    get { return _sites; }
        //    set { _sites = value; }
        //}

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



        public void LoadSites()
        {
            //ObservableCollection<Site> _sites = new ObservableCollection<Site>();
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;

            strSQL = "select * from site " +
                     " order by companylevel,description";
            //command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Sites.Add(new Site
                {
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    SiteID = Int32.Parse(row["siteid"].ToString()),
                    Description = row["description"].ToString(),
                    CompanyLevel = Int16.Parse(row["companylevel"].ToString()),
                    UnreadMessages = DatabaseMessages.GetUnreadMsgsFromSite(_server.AccountID, Int32.Parse(row["siteid"].ToString()))
                });
            }

            //Sites = _sites;
        }
    }
}