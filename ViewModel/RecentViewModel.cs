﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using static partigiano_windows_app_wpf.Model.Msg;
using partigiano_windows_app_wpf.Classes;
using System.Web;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class RecentViewModel
    {

        public ObservableCollection<Msg> Msgs
        {
            get;
            set;
        }

        public void LoadMessages(int MessageLimit=15)
        {
            ObservableCollection<Msg> _msgs = new ObservableCollection<Msg>();
            
            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;
            command.Parameters.Clear();

            //get all last messages FROM GROUPS ONLY
            strSQL = @"SELECT msgdate,msgq.accountgroupid,ScheduledDate,body,
                        msg.clientmsgid,msg.companyid,msg.serverid,msguuid,alert,
                        msgtype,destroy,quotemsguuid,qstatus,filestatus,
                        firstname,lastname,msgqid,
                        fromaccountid,fromdevid,toaccountid,todevid,status,
                        accountgroup.description as accountgroupname, site.description as sitename       
                        FROM msg INNER JOIN msgq ON msg.clientmsgid = msgq.clientmsgid 
                        LEFT JOIN account ON msgq.fromaccountid = account.accountid 
                        LEFT JOIN accountgroup ON msgq.accountgroupid = accountgroup.accountgroupid 
                        LEFT JOIN site ON accountgroup.siteid = site.siteid 
                        WHERE isheader=1 
                        and msgtype=0
                        and msgq.accountgroupid > 0
                        group by msgq.accountgroupid
                        having max(msgdate) 

                        union

                        SELECT msgdate,msgq.accountgroupid,ScheduledDate,body,
                        msg.clientmsgid,msg.companyid,msg.serverid,msguuid,alert,
                        msgtype,destroy,quotemsguuid,qstatus,filestatus,
                        firstname,lastname,msgqid,
                        fromaccountid,fromdevid,toaccountid,todevid,status,
                        accountgroup.description as accountgroupname, site.description as sitename       
                        FROM msg INNER JOIN msgq ON msg.clientmsgid = msgq.clientmsgid 
                        LEFT JOIN account ON msgq.fromaccountid = account.accountid 
                        LEFT JOIN accountgroup ON msgq.accountgroupid = accountgroup.accountgroupid 
                        LEFT JOIN site ON accountgroup.siteid = site.siteid 
                        WHERE isheader=1 
                        and msgtype=0
                        and msgq.accountgroupid = 0
                        group by msgq.fromaccountid,msgq.toaccountid
                        having max(msgdate) 

                        order by msgdate asc ";


            //command.Parameters.AddWithValue("@limit", MessageLimit);
            

            command.CommandText = strSQL;
            //command.CommandType = CommandType.Text;
            
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            DateTime datPreviousDate=DateTime.UtcNow;

            foreach (DataRow row in dt.Rows)
            {
                if( Int32.Parse(row["accountgroupid"].ToString()) == 0 )
                {
                    if ( Globals.ServerAccountIDs.Contains(Int32.Parse(row["accountgroupid"].ToString())) )
                    {

                    }

                }
                bool blnShowDate = false;
               
                DateTime datMsgDate;
                DateTime? datScheduledDate = null;

                datMsgDate = (DateTime)row["MsgDate"];
                datScheduledDate = (row["ScheduledDate"].ToString()=="")?null:(DateTime?)row["ScheduledDate"];

                if ( datPreviousDate.Date == datMsgDate.Date )
                {
                    blnShowDate = false;
                } else
                {
                    blnShowDate = true;
                }
                string strBody = StringCipher.Decrypt( row["body"].ToString(), Globals.EncryptionPassword );
                if(strBody.Length >= 30)
                {
                    strBody = strBody.Substring(0, 30) + " ...";
                }
                int intClientMSgID = Int32.Parse(row["clientmsgid"].ToString());
                _msgs.Insert(0,new Msg
                {
                    CompanyID = Int32.Parse(row["companyid"].ToString()),
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    ClientMsgID = intClientMSgID,
                    MsgUUID = row["msguuid"].ToString(),
                    Body = strBody,
                    Alert = Int16.Parse(row["alert"].ToString()),
                    MsgType = Int16.Parse(row["msgtype"].ToString()),
                    Destroy = Int16.Parse(row["destroy"].ToString()),
                    MsgDate = datMsgDate,
                    ScheduledDate = datScheduledDate,
                    QuoteMsgUUID = row["quotemsguuid"].ToString(),
                    QStatus = Int16.Parse(row["qstatus"].ToString()),
                    FileStatus = Int16.Parse(row["filestatus"].ToString()),
                    QuoteMsgBody = "",
                    ShowDate = blnShowDate,
                    ShownDate = datPreviousDate,

                    Header = new Msgq()
                    {
                        ServerID = Int32.Parse(row["serverid"].ToString()),
                        Firstname = row["firstname"].ToString(),
                        Lastname = row["lastname"].ToString(),
                        AccountGroupName = row["accountgroupname"].ToString(),
                        SiteName = row["sitename"].ToString(),
                        ClientMsgID = Int32.Parse(row["clientmsgid"].ToString()),
                        MsgqID = Int32.Parse(row["msgqid"].ToString()),
                        FromAccountID = (int)row["fromaccountid"],
                        FromDevID = (int)row["fromdevid"],
                        ToAccountID = (int)row["toaccountid"],
                        ToDevID = (int)row["todevid"],
                        AccountGroupID = (int)row["accountgroupid"],
                        DeliveredDate = null,
                        ReadDate = null,
                        Status = (int)row["status"],
                        IsHeader = 1
                    }
                    //MessageQueue = DatabaseMessages.GetMsgq(intClientMSgID)
                });

                datPreviousDate = datMsgDate;
            }

            Msgs = _msgs;
        }
    }
}