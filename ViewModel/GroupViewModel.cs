﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using partigiano_windows_app_wpf.Model;
using System.Data.SQLite;
using System.Data;
using partigiano_windows_app_wpf.Classes;

namespace partigiano_windows_app_wpf.ViewModel
{
    public class GroupViewModel
    {
       

        public ObservableCollection<Group> Groups
        {
            get;
            set;
        }

        public void LoadGroups()
        {
            ObservableCollection<Group> _groups = new ObservableCollection<Group>();

            Server _server = DatabaseCommon.GetServer(ServerID: Globals.CurrentServerID);

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand();
            command.Connection = Globals.Connection;

            strSQL = "select * from accountgroup " +
                     " where siteid=@siteid" +
                     " order by priority,description";
            //command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@siteid", Globals.CurrentSiteID);
            command.CommandText = strSQL;
            SQLiteDataAdapter da = new SQLiteDataAdapter(command);
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                _groups.Add(new Group
                {
                    ServerID = Int32.Parse(row["serverid"].ToString()),
                    CompanyID = Int16.Parse(row["companyid"].ToString()),
                    AccountGroupID = Int16.Parse(row["accountgroupid"].ToString()),
                    SiteID = Int16.Parse(row["siteid"].ToString()),
                    Description = row["description"].ToString(),
                    Color = row["color"].ToString(),
                    Priority = Int16.Parse(row["priority"].ToString()),
                    Email = row["email"].ToString(),
                    UnreadMessages = DatabaseMessages.GetUnreadMsgsFromGroup(_server.AccountID, Int32.Parse(row["accountgroupid"].ToString()))

                });
            }


            Groups = _groups;
        }


    

    }
}