﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Text.Json;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using partigiano_windows_app_wpf.SocketModel;
using System.Reflection;
using System.Data.SQLite;
using MahApps.Metro.Controls;
using System.Text.RegularExpressions;
using System.ComponentModel;
using partigiano_windows_app_wpf.Classes;
using partigiano_windows_app_wpf.Model;

namespace partigiano_windows_app_wpf
{
    /// <summary>
    /// Interaction logic for ServerrsWindow.xaml
    /// </summary>
    public partial class ServersWindow : MetroWindow
    {
        static HttpClient client = new HttpClient();

        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when user removed an account")]
        public event EventHandler ForceRefreshUI;


        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked when window needs to send a message to socket")]
        public event EventHandler<object> SendMessage;

        public bool ShowPinRegisterPanel { get;  set; }

        private int mintServerID = 0;

        public ServersWindow()
        {
            InitializeComponent();

            Loaded += MyWindow_Loaded;

        }

        private void MyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            brdRegister.Visibility = Visibility.Collapsed;
            brdUpdateServer.Visibility = Visibility.Collapsed;

            txtPin.Text = "";

            ServersViewControl.ServerRemoved += new EventHandler(ServerRemoved);
            ServersViewControl.ServerUpdate += new EventHandler<int>(ServerUpdate);

            if (ShowPinRegisterPanel)
            {
                txtPin.Text = "";
                brdRegister.Visibility = Visibility.Visible;
                txtPin.Focus();
            }
        }

        private void ServerRemoved(object sender, EventArgs e)
        {
            EventHandler handler = ForceRefreshUI;
            handler?.Invoke(null, null);
        }

        private void ServerUpdate(object sender, int ServerID)
        {
            mintServerID = ServerID;
            brdUpdateServer.Visibility = Visibility.Visible;
            ClearUpdateForm();
            Server _server = DatabaseCommon.GetServer(ServerID:ServerID);

            txtFirstName.Text = _server.Firstname;
            txtLastName.Text = _server.Lastname;
            txtEmail.Text = _server.Email;
        }



        private void ServersViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadServers();
        }

        private void LoadServers()
        {
            partigiano_windows_app_wpf.ViewModel.ServerViewModel ServersViewModelObject =
              new partigiano_windows_app_wpf.ViewModel.ServerViewModel();
            ServersViewModelObject.LoadServers();

            ServersViewControl.DataContext = ServersViewModelObject;
        }

        private void OnKeyDownHandler_txtPin(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter )
            {
                BtnRegisterPin_Click(sender, e);
            }
        }


        private async void BtnRegisterPin_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(txtPin.Text);

            if( txtPin.Text.ToString().Trim()=="" )
            {
                return;
            }

            this.Cursor = Cursors.Wait;
            if ( await RegistertAsync(txtPin.Text)){
                this.Cursor = Cursors.Arrow;
                this.Close();
            }
            this.Cursor = Cursors.Arrow;

        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            txtPin.Text = "";
            brdRegister.Visibility = Visibility.Visible;
            txtPin.Focus();

        }

        private void BtnRegisterClose_Click(object sender, RoutedEventArgs e)
        {
            brdRegister.Visibility = Visibility.Collapsed;
            txtPin.Text = "";
        }

        private void BtnUpdateAccountClose_Click(object sender, RoutedEventArgs e)
        {
            brdUpdateServer.Visibility = Visibility.Collapsed;
            ClearUpdateForm();

        }
        
        private void ClearUpdateForm()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPassword.Password = "";
            txtRepeatPassword.Password = "";
        }

        private void BtnUpdateAccountSave_Click(object sender, RoutedEventArgs e)
        {
            if(txtPassword.Password.Trim()!=txtRepeatPassword.Password.Trim())
            {
                MessageBox.Show("Passwords do not match.", "Invalid Password", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtPassword.Password.Trim().Length<8)
            {
                MessageBox.Show("Passwords length must be at least 8 characters.", "Invalid Password", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtFirstName.Text.Trim().Length < 2)
            {
                MessageBox.Show("First name length must be at least 2 characters.", "Invalid", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (txtLastName.Text.Trim().Length < 2)
            {
                MessageBox.Show("Last name length must be at least 2 characters.", "Invalid", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!Helpers.IsValidEmail(txtEmail.Text.Trim()))
            {
                MessageBox.Show("Invalid email.", "Invalid", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            brdUpdateServer.Visibility = Visibility.Collapsed;

            Server _server = DatabaseCommon.GetServer(ServerID: mintServerID);

            string strPassword = Helpers.CreateMD5(txtPassword.Password.Trim());

            AU_ClientModel aum = new AU_ClientModel
            {
                Action = "au",
                Au = new au_model
                {
                    AccountID = _server.AccountID,
                    DeviceID = _server.DeviceID,
                    CompanyID = _server.CompanyID,
                    Firstname = txtFirstName.Text.Trim(),
                    Lastname = txtLastName.Text.Trim(),
                    Email = txtEmail.Text.Trim(),
                    Password = strPassword

                }
            };

            EventHandler<object> handler = SendMessage;
            handler?.Invoke(null, aum);


            //save data. 
            //server will send the au message if data changed successfully on server
            //you need to save data, because password might be changed on server side

            SQLiteCommand command;
            DataTable dt = new DataTable();
            string strSQL;

            command = new SQLiteCommand
            {
                Connection = Globals.Connection
            };

            strSQL = @"update server set 
                           firstname=@firstname,
                           lastname=@lastname,
                           password=@password,
                           email=@email
                        where accountid = @accountid";

            command.Parameters.AddWithValue("@firstname", txtFirstName.Text.Trim());
            command.Parameters.AddWithValue("@lastname", txtLastName.Text.Trim());
            command.Parameters.AddWithValue("@password", strPassword);
            command.Parameters.AddWithValue("@email", txtEmail.Text.Trim());
            command.Parameters.AddWithValue("@accountid", _server.AccountID);
            command.CommandText = strSQL;
            int rowsUpdated = command.ExecuteNonQuery();

            LoadServers();

        }

        static async Task<REST_ResponseModel> GetPinAsync(string path)
        {
            REST_ResponseModel responsemodel = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                responsemodel = await response.Content.ReadAsAsync<REST_ResponseModel>();
            }
            return responsemodel;
        }

        static async Task<bool> RegistertAsync(string pin)
        {
            PinModel acp = new PinModel
            {
                Auth = new auth_Model
                {
                    Password= " p a r t i g i a n o "
                },
                Pin = new pn_Model
                {
                    Pin = pin,
                    Cguid = Globals.ClientGuid,
                    Os = Globals.getOSInfo(),
                    Hw = Globals.getCpuID(),
                    Serial = Globals.getBoardSerialNumber(),
                    Version = Assembly.GetExecutingAssembly().GetName().Version.ToString()
                }


            };
            string json = JsonSerializer.Serialize(acp);

            var payload = new StringContent(json, Encoding.UTF8, "application/json");

            //String awsurl = "https://xrqzxskuq3.execute-api.eu-central-1.amazonaws.com/beta/register";
            String awsurl = "https://pin.waavia7.com/register";
            HttpResponseMessage response = await client.PostAsync(awsurl, payload);

            response.EnsureSuccessStatusCode();

            // Deserialize the updated product from the response body.
            REST_ResponseModel responsemodel;

            responsemodel = await response.Content.ReadAsAsync<REST_ResponseModel>();

            if(responsemodel.statusCode==200)
            {
                var pindata = JsonSerializer.Deserialize<PinResponseModel>(responsemodel.body);
                if ( CreateServer(pindata) )
                {
                    return true;
                }

            } else
            {
                var errordata = JsonSerializer.Deserialize<ErrorModel>(responsemodel.body);
                MessageBox.Show(string.Format("#{0} {1}", errordata.error.code, errordata.error.message),"Error",MessageBoxButton.OK,MessageBoxImage.Error);
            }

            
            
            return false;
            //return responsemodel;
        }

        private static bool CreateServer(PinResponseModel pin)
        {
            SQLiteCommand  command;
            string strSQL;

            command = new SQLiteCommand ();
            command.Connection = Globals.Connection;

            strSQL = "select count(*) from Server where username=@username and companyid=@companyid ";
            command.Parameters.Add("@username", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@companyid", (DbType)SqlDbType.VarChar);
            command.Parameters["@username"].Value = pin.Username;
            command.Parameters["@companyid"].Value = pin.CompanyID;
            command.CommandText = strSQL;

            if (Convert.ToInt32(command.ExecuteScalar()) > 0)
            {
                MessageBox.Show("Cannot add account. Account exists.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            strSQL = "insert into Server (username,password,companyid,serverdescr,accountid,deviceid,siteroleid,sitelevel,namelocked,filesegment,lang,accountstatus)";
            strSQL += " values (@username,@password,@companyid,@serverdescr,@accountid,@deviceid,@siteroleid,@sitelevel,@namelocked,@filesegment,@lang,@accountstatus);";
            //strSQL += " SELECT CAST(scope_identity() AS int)";
            strSQL += "select last_insert_rowid()";
            command.Parameters.Add("@username", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@password", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@companyid", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@serverdescr", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@accountid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@deviceid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@siteroleid", (DbType)SqlDbType.Int);
            command.Parameters.Add("@sitelevel", (DbType)SqlDbType.Int);
            command.Parameters.Add("@namelocked", (DbType)SqlDbType.Int);
            command.Parameters.Add("@filesegment", (DbType)SqlDbType.Int);
            command.Parameters.Add("@lang", (DbType)SqlDbType.VarChar);
            command.Parameters.Add("@accountstatus", (DbType)SqlDbType.Int);
            command.Parameters["@username"].Value = pin.Username;
            command.Parameters["@password"].Value = pin.Password;
            command.Parameters["@companyid"].Value = pin.CompanyID;
            command.Parameters["@serverdescr"].Value = pin.CompanyDescription;
            command.Parameters["@accountid"].Value = pin.AccountID;
            command.Parameters["@deviceid"].Value = pin.DeviceID;
            command.Parameters["@siteroleid"].Value = pin.SiteRoleID;
            command.Parameters["@sitelevel"].Value = pin.SiteLevel;
            command.Parameters["@namelocked"].Value = pin.NameLocked;
            command.Parameters["@filesegment"].Value = pin.FileSegment;
            command.Parameters["@lang"].Value = pin.Language;
            command.Parameters["@accountstatus"].Value = 0;
            command.CommandText = strSQL;

            long newServerID = (long)command.ExecuteScalar();

            return true;
        }

       
        public class ErrorModel
        {
            public ErrorDetailsModel error { get; set; }
           
        }

        public class ErrorDetailsModel
        {
            public string name { get; set; }
            public string message { get; set; }
            public int code { get; set; }
            public int status { get; set; }


        }

    
    }
}
