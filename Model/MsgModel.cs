﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Configuration;
using System.Drawing;
using System.Text.Json;
using partigiano_windows_app_wpf.SocketModel;
using partigiano_windows_app_wpf.Classes;
using System.Windows;

namespace partigiano_windows_app_wpf.Model
{
    public class MsgModel { }

    public class Msg : INotifyPropertyChanged
    {
        //private string _MsgUUID = string.Empty;
        //private bool _IsMyMsg;

        public int ServerID { get; set; }
        public long ClientMsgID { get; set; }
        public int CompanyID { get; set; }
        public string MsgUUID { get; set; }
        public bool IsMyMsg
        {
            get
            {
                if (Globals.ServerAccountIDs.Contains(Header.FromAccountID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
        
        public bool HasQuotedMessage
        {
            get
            {
                return QuoteMsgUUID != "";
            }

        }

        public Msg QuoteMsg { get; set; }

        public string FromNiceAccountName
        {
            get
            {
                return Header.Firstname + " " + Header.Lastname;
            }
        }

    
        string _Body;
        public string Body
        {
            get { return this._Body; }
            set
            {
                this._Body = value;
                NotifyPropertyChanged();
            }
        }

        public int Alert { get; set; }
        public int MsgType { get; set; }
        public int Destroy { get; set; }
        public DateTime MsgDate { get; set; }
        public DateTime? ScheduledDate { get; set; }
        public string QuoteMsgUUID { get; set; }
        public string QuoteMsgBody { get; set; }

        public int FileStatus { get; set; }

        int _qstatus;
        public int QStatus
        {
            get
            {
                return _qstatus;
            }
            set
            {
                _qstatus = value;
                NotifyPropertyChanged();
            }
        }

        public Msgq Header { get; set; }

        
        public string NiceImagePreviewFilename
        {
            get {
                if (MsgType != 1) return "../images/image-150.png";
                try
                {

                    var data = JsonSerializer.Deserialize<FileHeaderModel>(Body);
                    string strExtension = System.IO.Path.GetExtension(data.Filename).ToLower();
                    if (strExtension == ".jpg" || strExtension == ".jpeg" || strExtension == ".png")
                    {
                        string filename = string.Format("{0}\\storage\\{1}\\{2}.p{3}", Globals.DataPath, ServerID, data.Hash, strExtension);
                        if (System.IO.File.Exists(filename))
                        {
                            return filename;
                        }
                        else
                        {
                            return "../images/image-150.png";
                        }
                    } else if (strExtension == ".mp3" || strExtension ==".wav")
                    {
                        return "../images/play-150-white-dark.png";
                    } else
                    {
                        return "../images/play-150-white-dark.png";
                    }
                }
                catch
                {
                    return "../images/play-150-white-dark.png";
                }

            }
        }


        public string NiceActualFilename
        {
            get
            {
                if (MsgType != 1) return "";
                try
                {
                    var data = JsonSerializer.Deserialize<FileHeaderModel>(Body);
                    return data.Filename;
                }
                catch
                {
                    return "";
                }
            }
        }


        public string NiceImageFilename
        {
            get
            {
                if (MsgType != 1) return "";

                try
                {
                    var data = JsonSerializer.Deserialize<FileHeaderModel>(Body);
       
                    string strExtension = System.IO.Path.GetExtension(data.Filename);
                    string filename = string.Format("{0}\\storage\\{1}\\{2}{3}", Globals.DataPath, ServerID, data.Hash, strExtension);
                    if (System.IO.File.Exists(filename))
                    {
                        return filename;
                    }
                    else
                    {
                        return "";
                    }
                }
                catch
                {
                    return "";
                }
            }
        }

        public int NiceImageWidth
        {
            get
            {
                if (MsgType != 1) return 0;
                try
                {
                    var data = JsonSerializer.Deserialize<FileHeaderModel>(Body);
                    string filename = string.Format("{0}\\storage\\{1}\\{2}.p.jpg", Globals.DataPath, ServerID, data.Hash);
                    if (System.IO.File.Exists(filename))
                    {
                        Bitmap image1 = (Bitmap)System.Drawing.Image.FromFile(filename, true);
                        return image1.Width;
                    }
                    else
                    {
                        return 80;
                    }

                }
                catch
                {
                    return 80;
                }

            }

        }

        public int NiceImageHeight
        {
            get
            {
                if (MsgType != 1) return 0;
                try
                {
                    var data = JsonSerializer.Deserialize<FileHeaderModel>(Body);
                    string filename = string.Format("{0}\\storage\\{1}\\{2}.p.jpg", Globals.DataPath, ServerID, data.Hash);
                    if (System.IO.File.Exists(filename))
                    {
                        Bitmap image1 = (Bitmap)System.Drawing.Image.FromFile(filename, true);
                        return image1.Height;
                    }
                    else
                    {
                        return 80;
                    }
                }
                catch
                {
                    return 80;
                }
            }
        }

        //if showdate=true then display the current date to UI
        public bool ShowDate { get; set; }
        public DateTime ShownDate { get; set; }

        public void RefreshModel()
        {
            NotifyPropertyChanged("NiceImageFilename");
            NotifyPropertyChanged("NiceActualFilename");
            NotifyPropertyChanged("NiceImageHeight");
            NotifyPropertyChanged("NiceImageWidth");

        }
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        
    }
}
