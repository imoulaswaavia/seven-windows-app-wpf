﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace partigiano_windows_app_wpf.Model
{
    public class AccountModel { }

    public class Account : INotifyPropertyChanged
    {
        int _AccountID;
        bool _IsCurrent;
        public int AccountID
        {
            get { return _AccountID; }

            set
            {
                _AccountID = value;
                NotifyPropertyChanged();
            }
        }

        public int ServerID { get; set; }
        public int CompanyID { get; set; }
        public int SiteRoleID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int SiteLevel { get; set; }
        public int Active { get; set; }
        public string Lang { get; set; }

        public bool IsCurrent
        {
            get
            {
                return _IsCurrent;
            }

            set
            {
                _IsCurrent = value;
                NotifyPropertyChanged();
            }
        }

        int _UnreadMessages;
        public int UnreadMessages
        {
            get
            {
                return _UnreadMessages;
            }

            set
            {
                _UnreadMessages = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HasUnreadMessages");
                NotifyPropertyChanged("NiceUnreadMessages");
            }
        }

        public string NiceUnreadMessages
        {
            get
            {
                if(_UnreadMessages==0)
                {
                    return "";
                } else
                {
                    return _UnreadMessages.ToString();
                }
                
            }
        }


        public bool HasUnreadMessages
        {
            get
            {
                return _UnreadMessages > 0;
            }
        }


        public string NiceName
        {
            get { 
                return Firstname + " " + Lastname; 
            }        
        }

        public string NiceInitials
        {
            get
            {
                string initials = "";
                if(Firstname.Length>1)
                {
                    initials = Firstname.Substring(0, 1);
                }
                if (Lastname.Length > 1)
                {
                    initials +=Lastname.Substring(0, 1);
                }
                return initials.ToUpper();
            }
        }

        public string NiceActiveDescription
        {
            get
            {
                switch(Active)
                {
                    case 0: //friends
                        return "You are friends";
                    case -1: //you requested
                        return "You have requested a friendship";
                    case -2: //non friend
                        return "You are not friends";
                    case -3: //friend requested
                        return "A friend is requesting your friendship";
                    case -19: //you denied
                        return "You have denied friendship";
                }
                return "";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
