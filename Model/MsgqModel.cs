﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace partigiano_windows_app_wpf.Model
{
    public class MsgqModel { }

    public class Msgq : INotifyPropertyChanged
    {
        public int ServerID { get; set; }
        public int MsgqID { get; set; }
        public int ClientMsgID { get; set; }
        public int CompanyID { get; set; }
        public int FromAccountID { get; set; }
        public int FromDevID { get; set; }
        public int ToAccountID { get; set; }
        public int ToDevID { get; set; }
        public int AccountGroupID { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public DateTime? ReadDate { get; set; }
        public DateTime MsgDate { get; set; }
        public int Queued { get; set; }

        public int IsHeader { get; set; }

        int _Status;
        public int Status
        {
            get { return this._Status; }
            set
            {
                this._Status = value;
                NotifyPropertyChanged();
            }
        }


        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string AccountGroupName { get; set; }
        public string SiteName { get; set; }

        public string NiceInitials
        {
            get
            {
                string initials = "";
                if (Firstname.Length > 1)
                {
                    initials = Firstname.Substring(0, 1);
                }
                if (Lastname.Length > 1)
                {
                    initials += Lastname.Substring(0, 1);
                }
                return initials.ToUpper();
            }
        }

        public string NiceName
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
