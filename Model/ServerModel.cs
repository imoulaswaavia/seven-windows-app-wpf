﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace partigiano_windows_app_wpf.Model
{
    public class ServerModel { }

    public class Server : INotifyPropertyChanged
    {
        public int ServerID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int AccountID { get; set; }
        public int DeviceID { get; set; }
        public int CompanyID { get; set; }
        public int FileSegment { get; set; }
        public string ServerDescription { get; set; }
        public int SiteRoleID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int SiteLevel { get; set; }
        public string Email { get; set; }
        public int NameLocked { get; set; }
        public string Language { get; set; }
        public int AccountStatus { get; set; }
        public string NiceName
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }
        public string NiceInitials
        {
            get
            {
                string initials = "";
                if (Firstname.Length > 1)
                {
                    initials = Firstname.Substring(0, 1);
                }
                if (Lastname.Length > 1)
                {
                    initials += Lastname.Substring(0, 1);
                }
                return initials.ToUpper();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }

   
}
