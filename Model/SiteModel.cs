﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace partigiano_windows_app_wpf.Model
{
    public class SiteModel { }

    public class Site : INotifyPropertyChanged
    {
        public int ServerID { get; set; }
        public int SiteID { get; set; }
        public int CompanyID { get; set; }
        public string Description { get; set; }
        public int CompanyLevel { get; set; }

        bool _IsCurrent;
        public bool IsCurrent
        {
            get { 
                return _IsCurrent; 
            }

            set
            {
                _IsCurrent = value;
                NotifyPropertyChanged();
            }
        }

        int _UnreadMessages;
        public int UnreadMessages
        {
            get
            {
                return _UnreadMessages;
            }

            set
            {
                _UnreadMessages = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("HasUnreadMessages");
                NotifyPropertyChanged("NiceUnreadMessages");
            }
        }

        public string NiceUnreadMessages
        {
            get
            {
                if (_UnreadMessages == 0)
                {
                    return "";
                }
                else
                {
                    return _UnreadMessages.ToString();
                }

            }
        }

        public bool HasUnreadMessages
        {
            get
            {
                return _UnreadMessages > 0;
            }
        }

        public string NiceInitials
        {
            get
            {
                string initials = "";
                if (Description.Length > 1)
                {
                    initials = Description.Substring(0, 1);
                }
                return initials.ToUpper();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
